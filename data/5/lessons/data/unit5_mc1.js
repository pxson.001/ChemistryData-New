/** Simple JavaScript Quiz
 * version 0.1.0
 * http://journalism.berkeley.edu
 * created by: Jeremy Rue @jrue
 *
 * Copyright (c) 2013 The Regents of the University of California
 * Released under the GPL Version 2 license
 * http://www.opensource.org/licenses/gpl-2.0.php
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 */

var quiztitle = "Unit 5 MC1";

/**
* Set the information about your questions here. The correct answer string needs to match
* the correct choice exactly, as it does string matching. (case sensitive)
*
*/
var quiz = [
    {
        "question"      :   "<center> 2 CO<i>(g)</i> + 2 NO<i>(g)</i> &#8652; 2 CO<sub>2</sub><i>(g)</i> + N<sub>2</sub><i>(g)</i> &nbsp;&nbsp; ∆H<sub>rxn</sub> = -746 kJ</center> <br/><br/> The reaction above is at equilibrium and a chemist decides to apply a stress to the system in order to increase the concentration of NO(g). Which of the following stresses would increase the concentration of NO(g) after equilibrium is re-established?",
        "image"         :   "",
        "choices"       :   [
								"Removing N<sub>2</sub>(g) from the system.",
								"Adding CO(g) to the system. ",
								"Increasing the temperature of the system.",
								"Decreasing the temperature of the system. "
                            ],
        "correct"       :   [2],
        "explanation"   :   "The shifts that occur with changing temperature can be thought of in the same way as changes in concentration. The forward reaction here is exothermic, as ∆H<sub>rxn</sub> < 0, so we can include heat as one of the products of the forward reaction. If we add heat, the equilibrium shifts to use up that heat, so the equilibrium would shift to the left to produce more reactants in this case. <br/><br/> To think about it more scientifically, adding heat will cause an increase in the rate of the endothermic reaction, which is the reverse reaction in this case. Heat is required to drive an endothermic reaction. If more heat is added, we are effectively adding the energy required to make the endothermic reaction happen more often. This extra energy ends up being stored as potential energy in the bonds of the products of the endothermic reaction, which are CO(g) and NO(g). <br/><br/> See Equilibrium II for more information.",
    },
	{
        "question"      :   "<center> 2 H<sub>2</sub>S<i>(g)</i> &#8652; 2 H<sub>2</sub><i>(g)</i> + S<sub>2</sub><i>(g)</i> &nbsp;&nbsp; K<sub>c</sub> = 1.0 x 10<sup>-6</sup> at 1000K</center> <br/><br/> If a chemist adds 1.0 mol of each of the three species involved in the above equilibrium to a rigid, previously evacuated, vessel at 1000 K, which of the following statements would be true when equilibrium is established?",
        "image"         :   "",
        "choices"       :   [
								"S<sub>2</sub><i>(g)</i> will have the lowest concentration of the three gases.",
								"H<sub>2</sub><i>(g)</i> will have the lowest concentration of the three gases.",
								"H<sub>2</sub>S<i>(g)</i> will have the lowest concentration of the three gases.",
								"The concentrations of three gases will be equal. "
                            ],
        "correct"       :   [1],
        "explanation"   :   "The reaction will proceed to the left until equilibrium is established, as the value for the equilibrium constant, K<sub>c</sub>, is very small. This means that the concentrations of H<sub>2</sub> and S<sub>2</sub> will decrease and the concentration of H<sub>2</sub>S will increase. Because the ratio of H<sub>2</sub> to S<sub>2</sub> in the balanced equation is 2:1, the concentration of H<sub>2</sub> will decrease by twice as much as that of S<sub>2</sub>. <br/><br/> See Equilibrium I and II for more information.",
    },
	{
        "question"      :   "<center> H<sub>2</sub><i>(g)</i> + I<sub>2</sub><i>(g)</i> &#8652; 2 HI<i>(g)</i> &nbsp;&nbsp; K<sub>c</sub> = 160 at 1000K</center> <br/><br/> A rigid 1.0 L vessel kept at 500 K contains 1.0 mol H<sub>2</sub>, 0.050 mol I<sub>2</sub>, and 2.0 mol HI. Which of the following statements about the system is correct?",
        "image"         :   "",
        "choices"       :   [
								"The rate of the forward reaction will exceed the rate of the reverse reaction until the system establishes equilibrium.",
								"The rate of the reverse reaction will exceed the rate of the forward reaction until the system establishes equilibrium.",
								"The system has established equilibrium, so chemical reactions are no longer taking place.",
								"The system has established equilibrium, so the rate of the forward reaction equals the rate of the reverse reaction. "
                            ],
        "correct"       :   [0],
        "explanation"   :   "<br/><i>Q<sub>c</sub></i> =<div class=\"frac\"> <span>[HI]<sup>2</sup></span> <span class=\"symbol\">/</span> <span class=\"bottom\">[H<sub>2</sub>][I<sub>2</sub>]</span></div> <br/> <i>Q<sub>c</sub></i> =<div class=\"frac\"> <span>[2.0]<sup>2</sup></span> <span class=\"symbol\">/</span> <span class=\"bottom\">[1.0][0.050]</span></div> <br/> <i>Q<sub>c</sub></i> = 80 <br/><br/> Since Q < K<sub>c</sub> the denominator in the equilibrium expression is too large and the numerator is too small. This means that the concentration of reactants is too large and the concentration of products is too small. The rate of the forward reaction will be greater than that of the reverse reaction until equilibrium is established.<br/><br/> See Equilibrium II for more information.",
    },
	{
        "question"      :   "<center> CaCO<sub>3</sub><i>(s)</i> &#8652; Ca<sup>2+</sup><i>(aq)</i> + CO<sub>3</sub><sup>2-</sup><i>(aq)</i></center> <br/><br/> The equilibrium above takes place in a sealed 2.0 L flask that is kept at 25<sup>o</sup>C. Which of the following would increase the number of moles of Ca<sup>2+</sup>(aq) in the above equilibrium system?",
        "image"         :   "",
        "choices"       :   [
								"Add additional CaCO<sub>3</sub>(s) to the solution.",
								"Add a 750 mL sample of distilled water to the solution. ",
								"Increase the pressure on the surface of the solution.",
								"Decrease the pressure on the surface of the solution."
                            ],
        "correct"       :   [1],
        "explanation"   :   "This is similar to decreasing the concentration in any solution or increasing the volume of a gas. If additional solvent is added, the volume increases and the concentration decreases. This causes the reaction to shift to the left in order to produce more moles of Ca<sup>2+</sup><i>(aq)</i> and CO<sub>3</sub><sup>2-</sup><i>(aq)</i>. When equilibrium is re-established, the number of moles of Ca<sup>2+</sup><i>(aq)</i> and CO<sub>3</sub><sup>2-</sup><i>(aq)</i> will be greater, but the concentration of Ca<sup>2+</sup><i>(aq)</i> and CO<sub>3</sub><sup>2-</sup><i>(aq)</i> will remain the same. The number of moles increased and the volume increased, but the number of moles per liter will be the same when the system re-establishes equilibrium. <br/><br/> Adding solid CaCO<sub>3</sub> will not cause the equilibrium to shift. Solids are left out of the equilibrium expression as they do not have a concentration in the solution. <br/><br/> Changes in pressure only affect the equilibrium of gaseous systems. <br/><br/> See Equilibrium II for more information.",
    },
	{
        "question"      :   "<center> 2 SO<sub>3</sub><i>(g)</i> &#8652; 2 SO<sub>2</sub><i>(g)</i> + O<sub>2</sub><i>(g)</i> &nbsp;&nbsp; ∆H<sup>o</sup><sub>298</sub> = 197.8 kJ/mol</center> <br/><br/> The equilibrium constant, K<sub>c</sub>, for the reaction above is 2.3 x 10<sup>-7</sup> at 298 K. The above equilibrium takes place in rigid sealed vessel. If the temperature of this system increases from 298 K to 550 K, what will happen to the value of the equilibrium constant?",
        "image"         :   "",
        "choices"       :   [
								"K<sub>c</sub> will increase, because the reaction will proceed to the right in order to produce more SO<sub>2</sub>(g) and O<sub>2</sub>(g). ",
								"K<sub>c</sub> will increase, because there are three moles of gas on the products side of the chemical equation and only two moles of gas on the reactants side of the chemical equation.",
								"K<sub>c</sub> will decrease, because the forward reaction is endothermic. ",
								"A change in temperature will cause the equilibrium position to shift, but the K<sub>c</sub> value will not change."
                            ],
        "correct"       :   [0],
        "explanation"   :   "The shifts that occur with changing temperature can be thought of in the same way as changes in concentration. The forward reaction here is endothermic, as ∆H<sup>o</sup> > 0, so we can include heat as one of the reactants in the forward reaction. If we add heat, the equilibrium shifts to consume the additional heat, so the equilibrium would shift to the right to produce more SO<sub>2</sub>(g) and O<sub>2</sub>(g). This will increase the concentrations of SO<sub>2</sub>(g) and O<sub>2</sub>(g), and decrease the concentration of SO<sub>3</sub>(g) as the system moves toward its new equilibrium position. Because the numerator has increased and the denominator has decreased in the equilibrium expression, the value of the equilibrium constant will increase. A temperature change is the only stress that changes the value of K<sub>eq</sub>. <br/><br/> See Equilibrium II for more information.",
    },
	{
        "question"      :   "<center> 2 SO<sub>3</sub><i>(g)</i> &#8652; 2 SO<sub>2</sub><i>(g)</i> + O<sub>2</sub><i>(g)</i> &nbsp;&nbsp; ∆H<sup>o</sup><sub>298</sub> = 197.8 kJ/mol</center> <br/><br/> A 1.0 mole sample of SO<sub>2</sub>(g) and a 1.0 mole sample of O<sub>2</sub>(g) are added to a rigid, previously evacuated, 1.5 L vessel at 298 K. The equilibrium constant, K<sub>c</sub>, for the reaction above is 2.3 x 10<sup>-7</sup> at 298 K. Which of the following is true when the system establishes equilibrium?",
        "image"         :   "",
        "choices"       :   [
								"[SO<sub>2</sub>] = 2[O<sub>2</sub>]",
								"[SO<sub>2</sub>] < [O<sub>2</sub>] < [SO<sub>3</sub>]",
								"[SO<sub>3</sub>] = [SO<sub>2</sub>]",
								"[SO<sub>3</sub>] = [SO<sub>2</sub>] > [O<sub>2</sub>]"
                            ],
        "correct"       :   [1],
        "explanation"   :   "Since K<sub>c</sub> << 0.1, we know that the system contains mostly reactants at equilibrium. The numerator must be much smaller than the denominator in the equilibrium expression to give a value of 2.3 x 10<sup>-7</sup> for K<sub>c</sub>. The denominator is associated with the reactants, and this is the larger number. Thus, at equilibrium, the system contains mostly reactants – SO<sub>3</sub> in this case. The system initially had 1.0 mole of SO<sub>3</sub> and 1.0 mole of O<sub>2</sub>, and two SO<sub>2</sub> molecules were consumed for every one O<sub>2</sub> molecule as the reaction proceeded toward equilibrium. For this reason, SO<sub>2</sub> ended up with the lowest concentration. <br/><br/> See Equilibrium I for more information",
    },
	{
        "question"      :   "<center> A<sub>2</sub><i>(g)</i> + B<sub>2</sub><i>(g)</i> &#8652; 2 AB<i>(g)</i></center> <br/><br/> A 3.0 mol sample of A<sub>2</sub>(g) and 3.0 mol sample of B(g) are placed in an 1.0 L evacuated vessel that is kept at 350 K. When equilibrium is established in accordance with the chemical equation above, the vessel contains 4.0 mol of AB(g). What is the value of the equilibrium constant, K<sub>c</sub>, at 350 K?",
        "image"         :   "",
        "choices"       :   [
								"0.25",
								"1.8",
								"4",
								"16"
                            ],
        "correct"       :   [3],
        "explanation"   :   "Since it is a 1.0 L vessel, 3.0 mol/1.0 L = 3.0 M. <br/><br/><center><table><tr><td></td><td>A2</td><td>B2</td><td>2 AB</td></tr><tr><td>I</td><td>3.0 M</td><td>3.0 M</td><td>0 M</td></tr><tr><td>C</td><td>-2.0 M</td><td>-2.0 M</td><td>+2(2.0 M)</td></tr><tr><td>E</td><td>1.0 M</td><td>1.0 M</td><td>4.0 M</td></tr></table></center> <br/><br/> The numbers for Change, C, and Equilibrium, E, in the above ICE chart are the only possible values that would give 4.0 mol of AB(g) at equilibrium. <br/><br/><center> K<sub>c</sub> =<div class=\"frac\"> <span>[AB]<sup>2</sup></span> <span class=\"symbol\">/</span> <span class=\"bottom\">[A<sub>2</sub>][B<sub>2</sub>]</span></div> <br/> K<sub>c</sub> =<div class=\"frac\"> <span>[4.0]<sup>2</sup></span> <span class=\"symbol\">/</span> <span class=\"bottom\">[1.0][1.0]</span></div> =16</center> <br/><br/> See Equilibrium I for more information.",
    },
	{
        "question"      :   "<center> AB<sub>4</sub><i>(g)</i> &#8652; AB<sub>2</sub><i>(g)</i> + B<sub>2</sub><i>(g)</i></center> <br/><br/> A chemist adds a pure sample of AB<sub>4</sub>(g) to a rigid reaction vessel until the pressure reaches 1.5 atm. The 2.5 L vessel is kept at 298 K. If the reaction went to completion, what would be the total pressure in the vessel?",
        "image"         :   "",
        "choices"       :   [
								"1.5 atm",
								"2.5 atm",
								"3.0 atm",
								"5.0 atm"
                            ],
        "correct"       :   [2],
        "explanation"   :   "If the reaction went to completion, the total number of moles in the vessel would double, as one mole of gas (AB<sub>4</sub>) decomposes to produce two moles of gas (AB<sub>2</sub> + B<sub>2</sub>). Doubling the number of moles of gas in the vessel will double the total pressure in the vessel, if the volume and temperature remain the same. See Equilibrium I for more information.",
    },
	{
        "question"      :   "For a certain process, ΔH<sup>o</sup> = -35.0 kJ/mol and ΔS<sup>o</sup> = +100 J/molK. Which of the following statements provides the best description of the system at equilibrium?",
        "image"         :   "",
        "choices"       :   [
								"The system contains mostly products.",
								"The equilibrium position is close to the middle.",
								"The system contains mostly reactants.",
								"The equilibrium position cannot be determined with the given information. "
                            ],
        "correct"       :   [0],
        "explanation"   :   "<br/>∆G<sup>o</sup> = ∆H<sup>o</sup> - T∆S<sup>o</sup> <br/> ∆G<sup>o</sup> &asymp; -35000 J - (300 K)(-100 j/K) <br/> ∆G<sup>o</sup> &asymp; - 5000 J <br/><br/> If ΔG<sup>o</sup> is negative, the system will contain mostly products when it reaches equilibrium. This will also mean that it will have a relatively large value for K<sub>eq</sub>, as K<sub>eq</sub> = products over reactants. The more negative the value for ΔG<sup>o</sup>, the further the equilibrium will lie to the right.<br/><br/> See Equilibrium III for more information.",
    },
	{
        "question"      :   "An increase in pressure would cause the equilibrium position to shift toward the reactants for which one of the following reactions?",
        "image"         :   "",
        "choices"       :   [
								"N<sub>2</sub><i>(g)</i> + 3 H<sub>2</sub><i>(g)</i> &#8652; 2 NH<sub>4</sub><i>(g)</i>",
								"CO<i>(g)</i> + H<sub>2</sub>O<i>(g)</i> &#8652; CO<sub>2</sub><i>(g)</i> + H<sub>2</sub><i>(g)</i>",
								"2 HI<i>(g)</i> &#8652; H<sub>2</sub> <i>(g)</i> + I<sub>2</sub><i>(g)</i>",
								"SO<sub>2</sub>Cl<sub>2</sub><i>(g)</i> &#8652; SO<sub>2</sub><i>(g)</i> + Cl<sub>2</sub><i>(g)</i>"
                            ],
        "correct"       :   [3],
        "explanation"   :   "If the pressure increases, the equilibrium will shift in the direction where there are fewer moles of gas in order to reduce the pressure. In this case, there is 1 mole of gas on the reactants side and 2 moles of gas on the products side of the balanced chemical equation. <br/><br/> If the volume is reduced by a factor of 2, the pressure doubles and the molar concentrations of all species double. The collision rate between particles increases when the volume decreases. This increases the rate of both the forward and the reverse reactions. The rate of the reaction that will bring the system to a lower pressure will be faster. In this case, the rate of the reverse reaction will be faster, as that will reduce the pressure. <br/><br/> See Equilibrium II for more information.",
    }
];
