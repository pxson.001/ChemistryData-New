/** Simple JavaScript Quiz
 * version 0.1.0
 * http://journalism.berkeley.edu
 * created by: Jeremy Rue @jrue
 *
 * Copyright (c) 2013 The Regents of the University of California
 * Released under the GPL Version 2 license
 * http://www.opensource.org/licenses/gpl-2.0.php
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 */

var quiztitle = "Unit 5 MC2";

/**
* Set the information about your questions here. The correct answer string needs to match
* the correct choice exactly, as it does string matching. (case sensitive)
*
*/
var quiz = [
    {
        "question"      :   "<center>COCl<sub>2</sub><i>(g)</i> &#8651; CO<i>(g)</i> + Cl<sub>2</sub><i>(g)</i> &nbsp;&nbsp; K<sub>p</sub>=167 at 850<sup>o</sup>C</center><br/><br/>A 2.0 mol sample of COCl<sub>2</sub>(g) is placed in a rigid, previously evacuated, container at 850<sup>o</sup>C. The container is sealed and the temperature is kept constant. Which if the following is true whenequilibrium is established?",
        "image"         :   "",
        "choices"       :   [
								"[COCl<sub>2</sub>] = [CO] = [Cl<sub>2</sub>]",
								"[COCl<sub>2</sub>] > [CO] = [Cl<sub>2</sub>]",
								"[COCl<sub>2</sub>] < [Cl<sub>2</sub>] =[CO]",
								"[CO] > [Cl<sub>2</sub>]"
                            ],
        "correct"       :   [2],
        "explanation"   :   "Since K<sub>p</sub> > 10, we know that the system contains mostly products at equilibrium. The numerator mustbe greater than the denominator in the equilibrium expression to give a value of 167 for the equilibriumconstant, K<sub>p</sub>. The numerator is associated with the products, and this is the larger number. Thus, theequilibrium system contains mostly products. [Cl<sub>2</sub>] and [CO] are equal, because every COCl<sub>2</sub> moleculedecomposes to produce one Cl<sub>2</sub> and one CO.<br/><br/>See Equilibrium I for more information",
    },
	{
        "question"      :   "<center>N<sub>2</sub>O<sub>4</sub><i>(g)</i> &#8651; 2 NO<sub>2</sub><i>(g)</i> </center><br/><br/>A chemist adds 1.0 mol of N<sub>2</sub>O<sub>4</sub><i>(g)</i> to a rigid, previously evacuated, vessel. Which of the followingstatements about the reverse reaction is true?",
        "image"         :   "",
        "choices"       :   [
								"The rate of the reverse reaction decreases until equilibrium is established and becomes zero at equilibrium.",
								"The rate of the reverse reaction remains constant before and after equilibrium is established. ",
								"The rate of the reverse reaction increases until equilibrium is established and becomes constant at equilibrium.",
								"The rate of the reverse reaction decreases until equilibrium is established and becomes constant at equilibrium."
                            ],
        "correct"       :   [2],
        "explanation"   :   "<br/><img src=\"data/images/5_mc2_2.png\" style=\"max-width:100%;max-height:100%\"/><br/>Reaction rates are directly proportional to concentration. In the beginning, when [NO<sub>2</sub>] was very low, ithad a low rate of reaction. As more NO<sub>2</sub> is produced by the decomposition of N<sub>2</sub>O<sub>4</sub>, the concentrationof NO<sub>2</sub> increases. An increase in the concentration of NO<sub>2</sub> means that there will be more collisionsbetween NO<sub>2</sub> molecules, which will result in a higher rate of production of N<sub>2</sub>O<sub>4</sub>. Once the system hasreached equilibrium, the rate of the forward reaction equals the rate of the reverse reaction. As aresult, each species maintains a set concentration (although the concentrations of the different speciesinvolved in the reaction will almost never be equal to one another.)<br/><br/>Forward and reverse reactions continue to occur after equilibrium is established. Concentrations staythe same because the rate of the forward reaction is equal to the rate of the reverse reaction when thesystem is at equilibrium.<br/><br/>See Equilibrium I for more information.",
    },
	{
        "question"      :   "<center>2SO<sub>2</sub><i>(g)</i> + O<sub>2</sub><i>(g)</i> &#8651; 2SO<sub>3</sub><i>(g)</i> </center><br/><br/>The equilibrium above takes place in a 1.0 L cylinder that is kept at 25<sup>o</sup>C. Which of the following wouldincrease the number of moles of SO<sub>2</sub><i>(g)</i> in the above equilibrium system? ",
        "image"         :   "",
        "choices"       :   [
								"Adding additional O<sub>2</sub><i>(g)</i>.",
								"Increasing the volume of the cylinder.",
								"Increasing the pressure in the cylinder",
								"Removing SO<sub>3</sub><i>(g)</i>."
                            ],
        "correct"       :   [1],
        "explanation"   :   "If we decrease the pressure, which is the same as increasing the volume, the equilibrium will shift in thedirection that produces more moles of gas, in order to increase the pressure. In this case, there are 3moles of gas on the reactants side and 2 moles of gas on the products side. Decreasing the pressure willtrigger the production of more SO<sub>2</sub><i>(g)</i> and O<sub>2</sub><i>(g)</i>, because this will increase the total number of gasparticles in the system.<br/><br/>See Equilibrium II for more information.",
    },
	{
        "question"      :   "<center>N<sub>2</sub><i>(g)</i> + 3H<sub>2</sub><i>(g)</i> &#8651; 2NH<sub>3</sub><i>(g)</i> &nbsp;&nbsp; K<sub>c</sub>=62 at 500 K</center><br/><br/>A chemist adds 1.0 mole of each of the three gases involved in the above equilibrium to a previouslyevacuated 1.0 L vessel that is kept at 500 K. Which of the following is true when the system establishesequilibrium?",
        "image"         :   "",
        "choices"       :   [
								"[NH<sub>3</sub>] > [N<sub>2</sub>] > [H<sub>2</sub>]",
								"[NH<sub>3</sub>] > [H<sub>2</sub>] > [N<sub>2</sub>]",
								"[H<sub>2</sub>] > [NH<sub>3</sub>] > [N<sub>2</sub>]",
								"[H<sub>2</sub>] = [NH<sub>3</sub>] > [N<sub>2</sub>]"
                            ],
        "correct"       :   [0],
        "explanation"   :   "<br/>Q<sub>c</sub>=<div class=\"frac\"> <span>[NH<sub>3</sub>]<sup>2</sup></span> <span class=\"symbol\">/</span> <span class=\"bottom\">[N<sub>2</sub>][H<sub>2</sub>]<sup>3</sup></span></div><br/>Q<sub>c</sub>=<div class=\"frac\"> <span>[1.0]<sup>2</sup></span> <span class=\"symbol\">/</span> <span class=\"bottom\">[1.0][1.0]<sup>3</sup></span></div>=1.0<br/><br/>As Q<sub>c</sub> < K<sub>c</sub>, the reaction will proceed to the right in order to produce more products. As this occurs,three H<sub>2</sub> molecules are consumed for every one N<sub>2</sub> molecule that is consumed. When equilibrium isestablished, NH<sub>3</sub> will be the only species with a concentration that is greater than 1.0 M.<br/><br/>See Equilibrium II for more information. ",
    },
	{
        "question"      :   "<center>CaO<i>(s)</i> + CO<sub>2</sub><i>(g)</i> &#8651; CaCO<sub>3</sub><i>(s)</i> &nbsp;&nbsp; ∆H<sub>rxn</sub>=-178 kJ/mol </center><br/><br/>The above equilibrium takes place in a sealed rigid vessel that is expandable. Which of the following willincrease the amount of CaCO<sub>3</sub><i>(s)</i> in the vessel? ",
        "image"         :   "",
        "choices"       :   [
								"Removing some of the CaO(s)",
								"Allowing some CO<sub>2</sub>(g) to escape the vessel",
								"Decreasing the temperature of the system",
								"Increasing the volume of the vessel"
                            ],
        "correct"       :   [2],
        "explanation"   :   "The shifts that occur with changing temperature can be thought of in the same way as changes inconcentration. The forward reaction here is exothermic, as ∆H<sub>rxn</sub> < 0. Thus, we can include heat as oneof the products of the forward reaction. If we remove heat, the equilibrium shifts to produce moreheat, so the equilibrium would shift to the right to produce more CaCO<sub>3</sub>(s) in this case.<br/><br/>Removing heat from this system will favor the production of compounds that possess less energy intheir bonds. In this case, that would be the production of more CaCO<sub>3</sub>(s).<br/><br/>See Equilibrium II for more information. ",
    },
	{
        "question"      :   "COCl<sub>2</sub><i>(g)</i> &#8651; CO<i>(g)</i> + Cl<sub>2</sub><i>(g)</i> &nbsp;&nbsp;&nbsp; K<sub>p</sub><sup>I</sup>=200<br/><br/>CO<i>(g)</i> &#8651; ½C<i>(s)</i> + ½CO<sub>2</sub><i>(g)</i> &nbsp;&nbsp;&nbsp; K<sub>p</sub><sup>II</sup>=9 x 10<sup>-8</sup><br/><br/>COCl<sub>2</sub><i>(g)</i> &#8651; Cl<sub>2</sub><i>(g)</i> + ½CO<sub>2</sub><i>(g)</i> + ½C<i>(s)</i>&nbsp;&nbsp;&nbsp; K<sub>p</sub><sup>III</sup>=?<br/><br/>Use the information above to determine which statement about the thermodynamic favorability ofreaction (III) is correct. ",
        "image"         :   "",
        "choices"       :   [
								"Reaction (III) is thermodynamically favorable, because K<sub>p</sub><sup>I</sup> > 1.",
								"Reaction (III) is not thermodynamically favorable, because K<sub>p</sub><sup>II</sup> < 1.",
								"Reaction (III) is not thermodynamically favorable, because K<sub>p</sub><sup>III</sup> < 1.",
								"Reaction (III) is thermodynamically favorable, because K<sub>p</sub><sup>III</sup> > 1."
                            ],
        "correct"       :   [2],
        "explanation"   :   "<br/>COCl<sub>2</sub><i>(g)</i> &#8651; CO<i>(g)</i> + Cl<sub>2</sub><i>(g)</i> &nbsp;&nbsp;&nbsp; K<sub>p</sub><sup>I</sup>=200<br/><br/>CO<i>(g)</i> &#8651; ½C<i>(s)</i> + ½CO<sub>2</sub><i>(g)</i> &nbsp;&nbsp;&nbsp; K<sub>p</sub><sup>II</sup>=9 x 10<sup>-8</sup><br/><br/>COCl<sub>2</sub><i>(g)</i> &#8651; Cl<sub>2</sub><i>(g)</i> + ½CO<sub>2</sub><i>(g)</i> + ½C<i>(s)</i>&nbsp;&nbsp;&nbsp; K<sub>p</sub><sup>III</sup>=(200)(9 x 10<sup>-8</sup>)=1.8 x 10<sup>-5</sup><br/><br/>To answer this question, you need to use K<sub>p</sub><sup>I</sup> and K<sub>p</sub><sup>II</sup> to find K<sub>p</sub><sup>III</sup>. If K<sub>p</sub><sup>III</sup> < 1, the reaction is notthermodynamically favored. If K<sub>p</sub><sup>III</sup> > 1, the reaction is thermodynamically favored. In this case, reaction(III) is not thermodynamically favored, because K<sub>p</sub><sup>III</sup> < 1.<br/><br/>See Thermodynamics III for more information.",
    },
	{
        "question"      :   "<center>N<sub>2</sub><i>(g)</i> + 3H<sub>2</sub><i>(g)</i> &#8651; 2NH<sub>3</sub><i>(g)</i> &nbsp;&nbsp; K<sub>c</sub>=0.040 at 500<sup>o</sup>C</center><br/><br/>A 1.0 L rigid vessel contains 2.0 mol of N<sub>2</sub>(g), 1.0 mol of H<sub>2</sub>(g) and 1.0 mol NH<sub>3</sub>(g) at 500<sup>o</sup>C. Identify thestatement that indicates what will happen to the total pressure in the vessel with a correct justification.",
        "image"         :   "",
        "choices"       :   [
								"Q > K<sub>c</sub>, so the total pressure will increase.",
								"Q > K<sub>c</sub>, so the total pressure will decrease.",
								"Q < K<sub>c</sub>, so the total pressure will increase.",
								"Q < K<sub>c</sub>, so the total pressure will decrease."
                            ],
        "correct"       :   [0],
        "explanation"   :   "<br/>Q<sub>c</sub>=<div class=\"frac\"> <span>[NH<sub>3</sub>]<sup>2</sup></span> <span class=\"symbol\">/</span> <span class=\"bottom\">[N<sub>2</sub>][H<sub>2</sub>]<sup>3</sup></span></div><br/>Q<sub>c</sub>=<div class=\"frac\"> <span>[1.0]<sup>2</sup></span> <span class=\"symbol\">/</span> <span class=\"bottom\">[2.0][1.0]<sup>3</sup></span></div>=0.5<br/>As Q<sub>c</sub> > K<sub>c</sub>, the reaction will proceed to the left in order to produce more reactants. As this occurs, twoNH<sub>3</sub> molecules are consumed for every three H<sub>2</sub> molecules and one N<sub>2</sub> molecule that are produced. In other words, the reaction will proceed to the left, and this will increase the total number of moles of gasin the system. Increasing the number of moles of gas in a system will increase the total pressure of thesystem, when the volume and temperature remain the same.<br/><br/>See Equilibrium II for more information. ",
    },
	{
        "question"      :   "<center>A<sub>2</sub>BC<i>(g)</i> &#8651; AB<i>(g)</i> + AC<i>(g)</i> </center><br/><br/>A chemist added pure A<sub>2</sub>BC<i>(g)</i> to a rigid, previously evacuated, vessel at 510 K until the pressure reached1.0 atm. When the above equilibrium was established, the total pressure in the vessel was 1.6 atm.What is the equilibrium constant, K<sub>p</sub>, for this reaction at 510 K?",
        "image"         :   "",
        "choices"       :   [
								"4",
								"1.6",
								"0.9",
								"0.05"
                            ],
        "correct"       :   [2],
        "explanation"   :   "<center><table> <tr><td></td><td>A<sub>2</sub>BC</td><td>AB</td><td>AC</td></tr><tr><td>I</td><td>1.0 atm</td><td>0 atm</td><td>0 atm</td></tr><tr><td>C</td><td>-0.6</td><td>+0.6</td><td>+0.6</td></tr><tr><td>E</td><td>0.4 atm</td><td>0.6 atm</td><td>0.6 atm</td></tr></table> </center><br/><br/>The numbers for Change, C, and Equilibrium, E, in the above ICE chart are the only possible values thatwould give a total pressure of 1.6 atm at equilibrium, as 0.4 atm + 0.6 atm + 0.6 atm=1.6 atm.<br/><br/><center>K<sub>p</sub>=<div class=\"frac\"><span><i>(P<sub>AB</sub>)(P<sub>AC</sub>)</span><span class=\"symbol\">/</span><span class=\"bottom\">P<sub>A<sub>2</sub>BC</sub></span></div><br/>K<sub>p</sub>=<div class=\"frac\"><span><i>(0.6)(0.6)</span><span class=\"symbol\">/</span><span class=\"bottom\">0.4</span></div>=0.9</center><br/><br/>See Equilibrium I for more information. ",
    },
	{
        "question"      :   "<center>A<i>(g)</i> + 2D<i>(g)</i> &#8651; AD<sub>2</sub><i>(g)</i></center><br/><br/>A chemist inserts A(g) and D(g) into an evacuated container until the partial pressure of A(g) is 0.650atm and the partial pressure of D(g) is 0.550 atm. When equilibrium is established, the partial pressureof AD<sub>2</sub> is 0.150 atm. What are the equilibrium partial pressures of A(g) and D(g)? ",
        "image"         :   "",
        "choices"       :   [
								"P<sub>A</sub> = 0.500 atm and P<sub>D</sub> = 0.250 atm",
								"P<sub>A</sub> = 0.250 atm and P<sub>D</sub> = 0.500 atm",
								"P<sub>A</sub> = 0.650 atm and P<sub>D</sub> = 0.550 atm",
								"P<sub>A</sub> = 0.500 atm and P<sub>D</sub> = 0.400 atm"
                            ],
        "correct"       :   [0],
        "explanation"   :   "<center><table> <tr><td></td><td>A <i>(g)</i></td><td>2D <i>(g)</i></td><td>AD<sub>2</sub> <i>(g)</i></td></tr><tr><td>I</td><td>0.650 atm</td><td>0.550 atm</td><td>0 atm</td></tr><tr><td>C</td><td>-0.150</td><td>-2(0.150)</td><td>+0.150</td></tr><tr><td>E</td><td>0.500 atm</td><td>0.250 atm</td><td>0.150 atm</td></tr></table> </center><br/><br/>See Equilibrium I for more information. ",
    },
	{
        "question"      :   "<center>2NO<i>(g)</i> + Cl<sub>2</sub><i>(g)</i> &#8651; 2NOCl<i>(g)</i> </center><br/><br/>The equilibrium constant, K<sub>c</sub>, for the reaction above is 2100 at 500 K. What is the equilibrium constantfor the following reaction at 500 K?<br/><br/><center>NOCl<i>(g)</i> &#8651; NO<i>(g)</i> + 1/2Cl<sub>2</sub><i>(g)</i> </center>",
        "image"         :   "",
        "choices"       :   [
								"(2100)<sup>1/2</sup> = 46",
								"<div class=\"frac\"><span>1</span><span class=\"symbol\">/</span><span class=\"bottom\">2100</span> </div> = 4.8 x 10<sup>-4</sup>",
								"<div class=\"frac\"><span>1</span><span class=\"symbol\">/</span><span class=\"bottom\">(2100)<sup>1/2</sup></span> </div> = 0.022",
								"<div class=\"frac\"><span>1</span><span class=\"symbol\">/</span><span class=\"bottom\">(2100)<sup>2</sup></span> </div> = 2.3 x 10<sup>-7</sup>"
                            ],
        "correct"       :   [2],
        "explanation"   :   "To answer this question you must apply the reciprocal rule and multiple equilibria rule. When you flip achemical reaction the new equilibrium constant is the inverse of the original equilibrium constant.<br/><br/><center><div class=\"frac\"> <span>1</span> <span class=\"symbol\">/</span> <span class=\"bottom\">K<sub>original</sub></span></div>=K<sub>new</sub></center><br/><br/>If you multiply the coefficients by a number, you must raise the original equilibrium constant to thatpower. In this problem, the coefficients were all multiplied by 1/2 so the original equilibrium constantwas raised to the power of 1/2. <br/><br/><center><div class=\"frac\"> <span>1</span> <span class=\"symbol\">/</span> <span class=\"bottom\">(K<sub>original</sub>)<sup>1/2</sup></span></div>=K<sub>new</sub></center><br/><br/>See Equilibrium II for more information. ",
    }

];
