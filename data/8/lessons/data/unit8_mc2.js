/** Simple JavaScript Quiz
 * version 0.1.0
 * http://journalism.berkeley.edu
 * created by: Jeremy Rue @jrue
 *
 * Copyright (c) 2013 The Regents of the University of California
 * Released under the GPL Version 2 license
 * http://www.opensource.org/licenses/gpl-2.0.php
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 */

var quiztitle = "Unit 8 MC2";

/**
* Set the information about your questions here. The correct answer string needs to match
* the correct choice exactly, as it does string matching. (case sensitive)
*
*/
var quiz = [
    {
        "question"      :   "CoCO<sub>3</sub> K<sub>sp</sub>=1 x 10<sup>-10</sup> at 25<sup>o</sup>C<br/><br/>CdCO<sub>3</sub> K<sub>sp</sub>=1 × 10<sup>-12</sup> at 25<sup>o</sup>C<br/><br/>A 0.50 L solutions contains 2 x 10<sup>-5</sup> <i>M</i> Co(NO<sub>3</sub>)<sub>2</sub> and 2 x 10<sup>-9</sup> <i>M</i> Cd(NO<sub>3</sub>)<sub>2</sub>. A student adds 0.50 L of 2 x 10<sup>-4</sup> <i>M</i> Na<sub>2</sub>CO<sub>3</sub> to the solution. Use the information above to determine which of the following will occur. Assume that the volumes are additive.",
        "image"         :   "",
        "choices"       :   [
								"CoCO<sub>3</sub><i>(s)</i> is the only precipitate that forms. ",
								"CdCO<sub>3</sub><i>(s)</i> is the only precipitate that forms. ",
								"CoCO<sub>3</sub><i>(s)</i> and CdCO<sub>3</sub><i>(s)</i> will precipitate ",
								"No precipitate will form "
                            ],
        "correct"       :   [0],
        "explanation"   :   "Find the initial concentrations of CO<sup>2+</sup>, Cd<sup>2+</sup> and CO<sub>3</sub><sup>2-</sup> after mixing but before any reactions take place. The volume doubles so the concentrations are halved. <br/><br/>[Co<sup>2+</sup>]=1 x 10<sup>-5</sup> <i>M</i> <br/><br/>[Cd<sup>2+</sup>]=1 x 10<sup>-9</sup> <i>M</i> <br/><br/>[CO<sub>3</sub><sup>2-</sup>]=1 x 10<sup>-4</sup> <i>M</i> <br/><br/><i>Q</i>=[Co<sup>2+</sup>][CO<sub>3</sub><sup>2-</sup>]=(1 x 10<sup>-5</sup>)(1 x 10<sup>-4</sup>)=1 x 10<sup>-9</sup><br/><i>Q</i> > <i>K<sub>sp</sub></i>, so a precipitate will form<br/><br/><i>Q</i>=[Cd<sup>2+</sup>][CO<sub>3</sub><sup>2-</sup>]=(1 x 10<sup>-9</sup>)(1 x 10<sup>-4</sup>)=1 x 10<sup>-13</sup><br/><i>Q</i> < <i>K<sub>sp</sub></i>, so a precipitate will NOT form<br/><br/>See Solutions II for more information. "
    },
    {
        "question"      :   "Iron and boron are melted and mixed in a ratio of 4 mol Fe to 1 mol B. After mixing, the solution is solidified. Which of the following provides a correct classification for this alloy and statement about its density?",
        "image"         :   "",
        "choices"       :   [
								"It is an interstitial alloy and the density of the alloy is greater than the density of the pure solvent.",
								"It is an interstitial alloy and the density of the alloy is less than the density of the pure solvent.",
								"It is a substitutional alloy and the density of the alloy is greater than the density of the pure solvent.",
								"It is a substitutional alloy and the density of the alloy is less than the density of the pure solvent."
                            ],
        "correct"       :   [0],
        "explanation"   :   "<br/><br/>A solution of boron and iron will form an interstitial alloy. We know this because the atomic radius of boron is much smaller than the atomic radius of iron. For this reason, the boron atoms will reside in the interstitial spaces between the iron atoms (see diagram below). Since the addition of boron increases the mass of the solution with little or no increase in volume, the density of the alloy increases. In this question, iron in the solvent because it has a higher concentration than boron.<br/><br/><center><img src=\"data/images/8_mc2_2.png\" style=\"max-width:100%;max-height:100%\"/></center><br/><br/>See Solutions I for more information."
    },
    {
        "question"      :   "A 10. g sample a Ca(NO<sub>3</sub>)<sub>2</sub> (molar mass=1.0 x 10<sup>2</sup> g/mol) is dissolved in 200. mL of water. What is the concentration of nitrate in the final solution?",
        "image"         :   "",
        "choices"       :   [
								"0.50 <i>M</i>",
								"1.0 <i>M</i>",
								"2.0 <i>M</i>",
								"10. <i>M</i>"
                            ],
        "correct"       :   [1],
        "explanation"   :   "<br/><br/>10. g Ca(NO<sub>3</sub>)<sub>2</sub> x<div class=\"frac\"> <span>1 mol Ca(NO<sub>3</sub>)<sub>2</sub></span> <span class=\"symbol\">/</span> <span class=\"bottom\">1.0×10<sup>2</sup> g Ca(NO<sub>3</sub>)<sub>2</sub></span></div>x<div class=\"frac\"> <span>2 mol NO<sub>3</sub><sup>−</sup></span> <span class=\"symbol\">/</span> <span class=\"bottom\">1 mol Ca(NO<sub>3</sub>)<sub>2</sub></span></div>=0.20 mol NO<sub>3</sub><sup>-</sup><br/><br/>Molarity<sub>Ca(NO<sub>3</sub>)<sub>2</sub></sub>=<div class=\"frac\"> <span>0.20 mol NO<sub>3</sub><sup>−</sup></span> <span class=\"symbol\">/</span> <span class=\"bottom\">0.200 L</span></div>=1.0 <i>M</i><br/><br/>See Solutions I for more information."
    },
    {
          "question"      :   "<center>CuI(<i>s</i>) &#8652; Cu<sup>+</sup>(<i>aq</i>) + I<sup>-</sup>(<i>aq</i>) <br/><br/><i>K</i><sub>sp</sub>=1.3 x 10<sup>-12</sup></center><br/><br/>A student mixes equal volumes of 0.10 <i>M</i> CuNO<sub>3</sub> and 1.0 <i>M</i> NaI and the equilibrium above is established. Which of the following lists the solutes that remain in solution in order of increasing molar concentration?",
        "image"         :   "",
        "choices"       :   [
								"[Cu<sup>+</sup>] < [I<sup>-</sup>] < [NO<sub>3</sub><sup>-</sup>] < [Na<sup>+</sup>]",
								"[Cu<sup>+</sup>] < [NO<sub>3</sub><sup>-</sup>] < [I<sup>-</sup>] < [Na<sup>+</sup>]",
								"[NO<sub>3</sub><sup>-</sup>] < [I<sup>-</sup>] < [Na<sup>+</sup>]",
								"[I<sup>-</sup>] < [NO<sub>3</sub><sup>-</sup>] < [Na<sup>+</sup>]"
                            ],
        "correct"       :   [1],
        "explanation"   :   "<br/><br/>The equilibrium lies far to the left, as the Ksp value is very small. Na<sup>+</sup> will have the highest concentration, as it is a spectator ion and we started out with a much higher concentration of NaI. Equal volumes of the two solutions were mixed, so the initial concentrations are halved. Thus, [Na<sup>+</sup>]=0.5 <i>M</i>. I<sup>-</sup> will have the second highest concentration, as less than 10% of the available I<sup>-</sup> reacted with Cu<sup>+</sup> to form CuI(<i>s</i>). [I<sup>-</sup>] will be close to 0.45 <i>M</i>. NO<sub>3</sub><sup>-</sup> will have the third highest concentration, as it is the spectator ion from the 0.10 M CuNO<sub>3</sub> solution. Thus, [NO<sub>3</sub><sup>-</sup>]=0.05 <i>M</i>. There will be a concentration of Cu<sup>+</sup> in the solution, as it is necessary for the equilibrium system, but it will be very small.<br/><br/>See Solutions II for more information."
    },
    {
        "question"      :   "Which of the following should be used when mixing solutions in order to obtain the most accurate results?",
        "image"         :   "",
        "choices"       :   [
								"Graduated cylinders",
								"Beakers",
								"Erlenmeyer flasks",
								"Volumetric flasks"
                            ],
        "correct"       :   [3],
        "explanation"   :   "Beakers and Erlenmeyer flasks provide the least amount of accuracy. Graduated cylinders are more accurate. Volumetric flasks are the most accurate."
    },
    {
        "question"      :   "If distilled water is added to a 100. mL of 1.5 <i>M</i> NaCl until the total volume reaches 3.0 <i>L</i>, what is the final concentration of the solution?",
        "image"         :   "",
        "choices"       :   [
								"0.50 <i>M</i> NaCl",
								"0.050 <i>M</i> NaCl",
								"0.0050 <i>M</i> NaCl",
								"0.00050 <i>M</i> NaCl"
                            ],
        "correct"       :   [1],
        "explanation"   :   "<br/><br/>0.100 L ×<div class=\"frac\"> <span>1.5 mol NaCl</span> <span class=\"symbol\">/</span> <span class=\"bottom\">1 L</span></div>=0.15 mol NaCl<br/><br/><div class=\"frac\"> <span>0.15 mol NaCl</span> <span class=\"symbol\">/</span> <span class=\"bottom\">3.0 L</span></div>=0.050<i>M</i> NaCl<br/><br/>See Solutions I for more information."
    },
    {
        "question"      :   "A saturated solution is created by adding excess Ag<sub>2</sub>SO<sub>4</sub>(<i>s</i>) to distilled water. When equilibrium is established, the concentration of Ag<sup>+</sup> is 0.030 <i>M</i>. What is the value for <i>K</i><sub>sp</sub>?",
        "image"         :   "",
        "choices"       :   [
								"1.3 x 10<sup>-6</sup>",
								"1.4 x 10<sup>-5</sup>",
								"4.5 x 10<sup>-4</sup>",
								"2.7 x 10<sup>-5</sup>"
                            ],
        "correct"       :   [1],
        "explanation"   :   "<br/><br/><i>K</i><sub>sp</sub>=Ag<sup>+</sup><sup>2</sup>SO<sub>4</sub><sup>2−</sup><br/><br/><i>K</i><sub>sp</sub>=(0.030)<sup>2</sup>(0.015)<br/><br/><i>K</i><sub>sp</sub>=(9.0x10<sup>−4</sup>)×(1.5x10<sup>−2</sup>)<br/><br/><i>K</i><sub>sp</sub>=1.4x10<sup>-5</sup><br/><br/>See Solutions II for more information."
    },
    {
        "question"      :   "A chemist puts 2.0 atm of O<sub>2</sub>(<i>g</i>) over 5.0 L of pure water in a rigid and sealed vessel, and puts 2.0 atm of NO(<i>g</i>) over 5.0 L of pure water in an identical rigid and sealed vessel. Both gases are capable of dissolving in water and both systems are kept at 15<sup>o</sup>C. The systems are left until the equilibriums are established. Which statement identifies the gas that will have the highest concentration in water after the equilibriums are established with a correct justification?",
        "image"         :   "",
        "choices"       :   [
								"O<sub>2</sub>(<i>aq</i>) will have a higher concentration in the water, as it is more polarizable due to its additional electron.",
								"O<sub>2</sub>(<i>aq</i>) will have a higher concentration in the water, as its unique geometry allows it to sit interstitially between the hydrogen atoms of water molecules.",
								"NO(<i>aq</i>) will have a higher concentration in thewater, because it establishes dipole-dipole interactions with water.",
								"NO(<i>aq</i>) will have a higher concentration in the water, because it has fewer lone pairs."
                            ],
        "correct"       :   [2],
        "explanation"   :   "<br/><br/>The electronegativity difference between N and O is 0.4, so NO(<i>g</i>) is slightly polar. It is more soluble, as it is able to form dipole-dipole interactions with water.<br/><br/>See Solutions II and Intermolecular Forces I for more information."
    },
    {
        "question"      :   "KCl is least soluble in which of the following liquids?",
        "image"         :   "",
        "choices"       :   [
								"H<sub>2</sub>O",
								"NH<sub>3</sub>",
								"CH<sub>3</sub>CH<sub>2</sub>OH",
								"CH<sub>3</sub>OH"
                            ],
        "correct"       :   [2],
        "explanation"   :   "<br/><br/>KCl is very soluble in both H<sub>2</sub>O and NH<sub>3</sub> and they are both very polar solvents. KCl is soluble in both CH<sub>3</sub>OH and CH<sub>3</sub>CH<sub>2</sub>OH, as they are both polar solvents. KCl is less soluble in CH<sub>3</sub>CH<sub>2</sub>OH, because it has a longer non-polar carbon chain.<br/><br/>See Solutions I for more information."
    },
    {
        "question"      :   "<center>AB(<i>s</i>) &#8652; A<sup>+</sup>(<i>aq</i>) + B<sup>-</sup>(<i>aq</i>)</center><br/><br/>It was observed that when the beaker containing the equilibrium system above was placed in a cold water bath, all of the solid eventually dissolved from the bottom of the beaker. When the solution was removed from the cold water bath and returned to room temperature, solid appeared on the bottom of the beaker again. Which of the following statements provides the most likely explanation for these occurrences?",
        "image"         :   "",
        "choices"       :   [
								"The forward reaction is most likely endothermic.",
								"The forward reaction is most likely exothermic.",
								"The volume of the solution decreased when it was cooled and this caused the equilibrium to shift to the right.",
								"The atmospheric pressure in the room decreased after the beaker was placed in the cold water bath and this caused the equilibrium to shift to the right."
                            ],
        "correct"       :   [1],
        "explanation"   :   "<br/><br/>If the forward reaction is exothermic and heat is removed (the temperature decreases), the equilibrium position will shift to the right in order to produce more heat. There are exceptions to this rule, but it holds true in most cases.<br/><br/>See Solutions I for more information."
    }
];
