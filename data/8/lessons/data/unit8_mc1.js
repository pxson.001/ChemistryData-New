/** Simple JavaScript Quiz
 * version 0.1.0
 * http://journalism.berkeley.edu
 * created by: Jeremy Rue @jrue
 *
 * Copyright (c) 2013 The Regents of the University of California
 * Released under the GPL Version 2 license
 * http://www.opensource.org/licenses/gpl-2.0.php
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 */

var quiztitle = "Unit 8 MC1";

/**
* Set the information about your questions here. The correct answer string needs to match
* the correct choice exactly, as it does string matching. (case sensitive)
*
*/
var quiz = [
    {
        "question"      :   "<center><img src=\"data/images/8_mc1_1.png\" style=\"max-width:100%;max-height:100%\"/></center><br/><br/>The structures for 1-pentanol, C<sub>5</sub>H<sub>11</sub>OH, and 1-butanol, C<sub>4</sub>H<sub>9</sub>OH, are shown above. A solution contains an unknown concentration of each compound. A student chooses to separate them using fractional distillation. Which statement identifies the compound that will have a higher concentration in the distillate than it did in the original solution with a correct justification?",
        "image"         :   "",
        "choices"       :   [
								"1-butanol, because it has a shorter carbon chain.",
								"1-butanol, because it has the lower equilibrium vapor pressure at any temperature.",
								"1-pentanol, because it has a longer carbon chain.",
								"1-pentanol, because it boils at a lower temperature than 1-butanol."
                            ],
        "correct"       :   [0],
        "explanation"   :   "<br/><br/>Both structures form hydrogen bonds. 1-butanol has a shorter carbon chain, so it is smaller, has fewer electrons and is less polarizable than 1-pentanol. For these reasons, it experiences weaker London dispersion forces. Since the intermolecular forces are weaker for 1-butanol, it will have a higher vapor pressure, which means that the rate at which it evaporates will be greater than that of 1-penatanol. A higher rate of evaporation will lead to a higher concentration in the distillate.<br/><br/>See Solutions I for more information."
    },
    {
        "question"      :   "Brass is a solid solution of copper and zinc. Which of the following provides a correct classification for this solution?",
        "image"         :   "",
        "choices"       :   [
								"Interstitial alloy",
								"Substitutional alloy",
								"Amorphous solid",
								"Covalent network solid"
                            ],
        "correct"       :   [1],
        "explanation"   :   "<br/><br/>A solution of copper and zinc will form a substitutional alloy. We know this because their atomic radii are very similar. For this reason, one atom can substitute for another in such a way that the number of atoms per unit volume basically stays the same (see diagram below).<br/><br/><center><img src=\"data/images/8_mc1_2.png\" style=\"max-width:100%;max-height:100%\"/></center><br/><br/>See Solutions I and Solids for more information."
    },
    {
        "question"      :   "A 1 x 10<sup>-4</sup> mol sample of Na2OH is added to 1.0 L of 2.0 x 10<sup>-2</sup> M Ca(NO<sub>3</sub>)<sub>2</sub>. Assume that the volume of the solution does not change. <i>K</i><sub>sp</sub>=5 x 10-6 for Ca(OH)<sub>2</sub>. Which of the following states what will occur with a correct justification?<br/><br/>",
        "image"         :   "",
        "choices"       :   [
								"A precipitate of Ca(OH)<sub>2</sub>(s)  will form because <i>Q</i> < <i>K</i><sub>sp</sub>.",
								"A precipitate of Ca(OH)<sub>2</sub>(s)  will form because <i>Q</i> > <i>K</i><sub>sp</sub>.",
								"A precipitate of Ca(OH)<sub>2</sub>(s)  will not form because <i>Q</i> < <i>K</i><sub>sp</sub>.",
								"A precipitate of Ca(OH)<sub>2</sub>(s)  will not form because <i>Q</i> < <i>K</i><sub>sp</sub>."
                            ],
        "correct"       :   [0],
        "explanation"   :   "<br/><br/>Find the initial concentrations of Ca<sup>2+</sup> and OH<sup>-</sup> after mixing but before any reactions take place. [Ca<sup>2+</sup>]=2.0 x 10<sup>-2</sup> <i>M</i>, as the volume did not change.<br/><br/>[OH<sup>-</sup>]=1 x 10<sup>-4</sup> <i>M</i>, as a 1 x 10<sup>-4</sup> mol sample of Na<sub>2</sub>OH was added to 1.0 L of solutions and the volume did not change.<br/><br/><i>Q</i>=[Ca<sup>2+</sup>][OH<sup>-</sup>]<sup>2</sup>=(2.0x10<sup>2-</sup>)(1x10<sup>-4</sup>)<sup>2</sup><br/><br/><i>Q</i>=2x10<sup>−10</sup><br/><br/><i>Q</i><<i>K</i><sub>sp</sub> , so a precipitate will not form<br/><br/>See Solutions II for more information."
    },
    {
        "question"      :   "<br/><br/><center><img src=\"data/images/8_mc1_4.png\" style=\"max-width:100%;max-height:100%\"/></center><br/><br/>A pigment containing three colored compounds (A, B and C) was separated in a paper chromatography experiment. Hexane, C<sub>6</sub>H<sub>14</sub>, was used as the chromatography solvent. The results are shown in the diagram above. Which of the following lists the three compounds in order of increasing polarity?",
        "image"         :   "",
        "choices"       :   [
								"compound A < compound B < compound C",
								"compound C < compound B < compound A",
								"compound A < compound C < compound B",
								"compound B < compound A < compound C"
                            ],
        "correct"       :   [1],
        "explanation"   :   "<br/><br/>The stationary phase is the chromatography paper, and the moving phase is the solvent used. As the solvent moves up the piece of paper it carries solute particles with it. However, the distances that the different solute particles travel up the paper depend on their relative attractions for the moving solvent and the stationary paper.<br/><br/>Paper is made of cellulose fibers, which have polar and non-polar components. Cellulose fibers have non-polar carbon chains with polar (–OH) groups that stick out at various locations along the structure. The pigments that are considered to be ‘most polar’ can form H-bonds at several locations on their structures. These structures will have a greater attraction for the stationary paper than they do for the mobile, non-polar, hexane solvent. For this reason, the ‘most polar’ pigment will travel the least distance up the paper, the second ‘most polar’ pigment will be deposited in the middle, and the least polar pigment will travel the greatest distance up the paper.<br/><br/>If there are strong intermolecular forces of attraction between the solvent and the solute, it will travel a fairly far distance up the paper. If there are weak intermolecular forces of attraction between the solvent and the solute, the solute will fall out of solution and cling to the paper fairly close to the solution’s surface. All identical solute particles will fall out of the solution and be deposited on the paper at the same height, but this height will be different for different types of solute particles. As the different solute particles are often different colors, the experiment produces different colored stripes at different heights above the surface of the chromatography solvent.<br/><br/>See Solutions I for more information."
    },
    {
        "question"      :   "<br/><br/>A 0.20 mol sample of Sr(OH)<sub>2</sub>, a 0.10 mol sample of Ba(OH)<sub>2</sub> and a 0.40 mol sample of NaOH are dissolved in 100 mL of distilled water in a 250 mL volumetric flask. Distilled water is then added until the total volume of the solution reaches 250.00 mL. What is the concentration of OH<sup>-</sup> in the solution?",
        "image"         :   "",
        "choices"       :   [
								"1.0 <i>M</i>",
								"2.0 <i>M</i>",
								"3.0 <i>M</i>",
								"4.0 <i>M</i>"
                            ],
        "correct"       :   [3],
        "explanation"   :   "<br/><br/>0.20 mol Sr(OH)<sub>2</sub> x<div class=\"frac\"> <span>2 mol OH<sup>−</sup></span> <span class=\"symbol\">/</span> <span class=\"bottom\">1 mol Sr(OH)<sub>2</sub></span></div>=0.40 mol OH<sup>−</sup><br/><br/>0.10 mol Ba(OH)<sub>2</sub> x<div class=\"frac\"> <span>2 mol OH<sup>−</sup></span> <span class=\"symbol\">/</span> <span class=\"bottom\">1 mol Ba(OH)<sub>2</sub></span></div>=0.20 mol OH<sup>−</sup><br/><br/>0.40 mol NaOH x<div class=\"frac\"> <span>1 mol OH<sup>−</sup></span> <span class=\"symbol\">/</span> <span class=\"bottom\">1 mol NaOH</span></div>=0.40 mol OH−<br/><br/><i>n</i><sub>OH<sup>-</sup></sub>=0.40 + 0.20 + 0.40=1.00 mol OH<sup>−</sup><br/><br/>[OH<sup>−</sup>]=<div class=\"frac\"> <span>1.00 mol OH<sup>−</sup></span> <span class=\"symbol\">/</span> <span class=\"bottom\">0.25000 L</span></div>=4.00<i>M</i> OH<sup>−</sup><br/><br/>See Solutions I for more information"
    },
    {
        "question"      :   "What volume of water must be added to a 100. mL solution of 2.0 <i>M</i> NaBr to make a 0.10 <i>M</i> NaBr solution?",
        "image"         :   "",
        "choices"       :   [
								"19.9 L",
								"1.9 L",
								"1.5 L",
								"0.09 L"
                            ],
        "correct"       :   [1],
        "explanation"   :   "<br/><br/>0.100 L ×<div class=\"frac\"> <span>2.0 mol NaBr</span> <span class=\"symbol\">/</span> <span class=\"bottom\">1 L</span></div>=0.20 mol NaBr<br/><br/>0.20 mol NaBr x<div class=\"frac\"> <span>1 L</span> <span class=\"symbol\">/</span> <span class=\"bottom\">0.10 mol NaBr</span></div>=2.0 L<br/><br/>2.0 L − 0.100 L=1.9 L<br/><br/>See Solutions I for more information."
    },
    {
        "question"      :   "A students dissolves 80.0 g of NaOH (molar mass = 40.0 g/mol) in distilled water. The total volume of the resulting solution is 8.0 L. What is the concentration of NaOH in the final solution?",
        "image"         :   "",
        "choices"       :   [
								"0.063 <i>M</i> NaOH",
								"0.25 <i>M</i> NaOH",
								"2.5 <i>M</i> NaOH",
								"10. <i>M</i> NaOH"
                            ],
        "correct"       :   [1],
        "explanation"   :   "<br/><br/>80.0 g x<div class=\"frac\"> <span>1 mol NaOH</span> <span class=\"symbol\">/</span> <span class=\"bottom\">40.0 g NaOH</span></div>=2.00 mol NaOH<br/><br/><div class=\"frac\"> <span>2.00 mol NaOH</span> <span class=\"symbol\">/</span> <span class=\"bottom\">8.0 L</span></div>=0.25<i>M</i> NaOH<br/><br/>See Solutions I for more information."
    },
    {
        "question"      :   "Which of the following statements about the solubility of gas in water is true?",
        "image"         :   "",
        "choices"       :   [
								"The solubility of a gas increases as the temperature of the gas on the surface of the liquid increases.",
								"The solubility of a gas increases as the temperature of the liquid decreases.",
								"The solubility of agasin creases as the partial pressure of that gas on the surface of the liquid decreases.",
								"Cl<sub>2</sub>(<i>g</i>) is less soluble in water than H<sub>2</sub>(<i>g</i>)."
                            ],
        "correct"       :   [1],
        "explanation"   :   "<br/><br/>Gases tend to have very weak intermolecular interactions with water. Temperature is a measure of the average kinetic energy of the particles in a system. The formula for kinetic energy is <i>KE</i>=1⁄2 mv<sup>2</sup>. Since the mass of the molecules does not change, their velocity increases as the temperature increases. As the speed of the water molecules increases, the weak intermolecular attractions between water and gas molecules are broken, and the gas leaves the solution. For this reason, gas solubility decreases as the temperature of the solution increases.<br/><br/>See Solutions II and Intermolecular Forces I for more information."
    },
    {
        "question"      :   "<center>CuCl(<i>s</i>) &#8652; Cu<sup>+</sup>(<i>aq</i>) + Cl<sup>-</sup>(<i>aq</i>) <br/><br/><i>K</i><sub>sp</sub>=1.7 x 10<sup>-7</sup></center>",
        "image"         :   "",
        "choices"       :   [
								"[Na<sup>+</sup>] > [Cl<sup>-</sup>] > [Cu<sup>+</sup>]",
								"[Na<sup>+</sup>] = [Cl<sup>-</sup>] = [Cu<sup>+</sup>]",
								"[Cl<sup>-</sup>] > [Na<sup>+</sup>] > [Cu<sup>+</sup>]",
								"[Cu<sup>+</sup>] = 0 <i>M</i> and [Na<sup>+</sup>] = [Cl<sup>-</sup>]"
                            ],
        "correct"       :   [2],
        "explanation"   :   "<br/><br/>When the NaCl is added to the solution, the CuCl(<i>s</i>) &#8652; Cu<sup>+</sup>(<i>aq</i>) + Cl<sup>-</sup>(<i>aq</i>) equilibrium will shift to the left do to the addition of the common chloride ion. This will cause the concentrations of Cu<sup>+</sup>(<i>aq</i>) and Cl<sup>-</sup>(<i>aq</i>) to decrease until equilibrium is established once again. When equilibrium is re-established, [Cu<sup>+</sup>] will be very low, but a non-zero concentration must exist as it is necessary for the following equilibrium.<br/><br/><center>CuCl(<i>s</i>) &#8652; Cu<sup>+</sup>(<i>aq</i>) + Cl<sup>-</sup>(<i>aq</i>)</center><br/><br/>[Cl<sup>-</sup>]=[Cu<sup>+</sup>] + [Na<sup>+</sup>], as the number of moles of chloride that came from NaCl will be in solution and the very small number of moles of chloride that came from the CuCl will be in solution. We also know that this is true, because the overall solution must be neutral.<br/><br/>See Solutions II for more information."
    },
    {
        "question"      :   "A gaseous solution contains 1.5 mol of Ar, 2.5 mol of Ne, and 1.0 mol of He. What is the mole fraction of each gas?",
        "image"         :   "",
        "choices"       :   [
								"X<sub>Ar</sub> = 0.30, X<sub>Ne</sub> = 0.50, and X<sub>He</sub> = 0.20",
								"X<sub>Ar</sub> = 1.5, X<sub>Ne</sub> = 2.5, and X<sub>He</sub> = 1.0",
								"X<sub>Ar</sub> = 0.15, X<sub>Ne</sub> = 0.25, and X<sub>He</sub> = 0.10",
								"X<sub>Ar</sub> =30., X<sub>Ne</sub> = 50., and X<sub>He</sub> = 20."
                            ],
        "correct"       :   [0],
        "explanation"   :   "<br/><br/><i>X</i><sub>Ar</sub>=<div class=\"frac\"> <span><i>n</i><sub>Ar</sub></span> <span class=\"symbol\">/</span> <span class=\"bottom\"><i>n</i><sub>Ar</sub> + <i>n</i><sub>He</sub> + <i>n</i><sub>Ne</sub></span></div>=<div class=\"frac\"> <span>1.5</span> <span class=\"symbol\">/</span> <span class=\"bottom\">1.5 + 2.5 + 1.0</span></div>=0.30<br/><br/><i>X</i><sub>He</sub>=<div class=\"frac\"> <span><i>n</i><sub>He</sub></span> <span class=\"symbol\">/</span> <span class=\"bottom\"><i>n</i><sub>Ar</sub> + <i>n</i><sub>He</sub> + <i>n</i><sub>Ne</sub></span></div>=<div class=\"frac\"> <span>2.5</span> <span class=\"symbol\">/</span> <span class=\"bottom\">1.5 + 2.5 + 1.0</span></div>=0.50<br/><br/><i>X</i><sub>Ne</sub>=<div class=\"frac\"> <span><i>n</i><sub>Ne</sub></span> <span class=\"symbol\">/</span> <span class=\"bottom\"><i>n</i><sub>Ar</sub> + <i>n</i><sub>He</sub> + <i>n</i><sub>Ne</sub></span></div>=<div class=\"frac\"> <span>1.0</span> <span class=\"symbol\">/</span> <span class=\"bottom\">1.5 + 2.5 + 1.0</span></div>=0.20<br/><br/>See Solutions I for more information."
    }
];
