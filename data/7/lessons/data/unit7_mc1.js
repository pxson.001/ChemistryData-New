/** Simple JavaScript Quiz
 * version 0.1.0
 * http://journalism.berkeley.edu
 * created by: Jeremy Rue @jrue
 *
 * Copyright (c) 2013 The Regents of the University of California
 * Released under the GPL Version 2 license
 * http://www.opensource.org/licenses/gpl-2.0.php
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 */

var quiztitle = "Unit 7 MC1";

/**
* Set the information about your questions here. The correct answer string needs to match
* the correct choice exactly, as it does string matching. (case sensitive)
*
*/
var quiz = [
    {
        "question"      :   "Which of the following cations would have the strongest interactions with NH<sub>3</sub>.",
        "image"         :   "",
        "choices"       :   [
								"Li<sup>+</sup>",
								"Mg<sup>2+</sup>",
								"Na<sup>+</sup>",
								"Ca<sup>2+</sup>"
                            ],
        "correct"       :   [1],
        "explanation"   :   "According to Coulomb’s law, forces of attraction increase as radii decrease and charges increase. When comparing two radii that are similar, choose the ion with the largest charge as charges tend to be more significant than distances.<br/><br/><center><i>F</i>=<div class=\"frac\"> <span><i>kQ</i><sub>1</sub><i>Q</i><sub>2</sub></span> <span class=\"symbol\">/</span> <span class=\"bottom\"><i>r</i><sup>2</sup></span></div></center> <br/><br/>See Intermolecular Forces I, Chemical Bonding I and Atomic Theory III for more information. ",
    },
	{
        "question"      :   "Hydrogen bonds occur between the molecules in pure samples of which of the following liquids? ",
        "image"         :   "",
        "choices"       :   [
								"H<sub>2</sub>S",
								"C<sub>4</sub>H<sub>8</sub>",
								"CH<sub>3</sub>F",
								"NH<sub>3</sub>"
                            ],
        "correct"       :   [3],
        "explanation"   :   "Hydrogen bonds only occur between a hydrogen atom that is covalently bonded to fluorine, oxygen, or nitrogen and another fluorine, oxygen, or nitrogen with at least one lone pair. The only compound from the above list that has these characteristics is NH<sub>3</sub>. <br/><br/><center><img src=\"data/images/7_mc1_2.png\" style=\"max-width:100%,max-height:100%\"/></center><br/><br/>Hydrogen is bonded to sulfur in (A) and to carbon in (B) and (C). <br/>See Intermolecular Forces I for more information. ",
    },
	{
        "question"      :   "London dispersion forces are the only forces of attraction that occur between the molecules in which of the following pure liquids? ",
        "image"         :   "",
        "choices"       :   [
								"NH<sub>3</sub>",
								"CH<sub>3</sub>F",
								"H<sub>2</sub>O",
								"BF<sub>3</sub>"
                            ],
        "correct"       :   [3],
        "explanation"   :   "BF<sub>3</sub> is trigonal planar, which is a symmetrical geometry. As the electronegative differences between the central and terminal atoms are the same in this molecule, the dipoles cancel. You can think of the bond dipoles in this molecule as vectors that are the same length. Since the structure is symmetrical, those vectors will cancel. The other molecules do not have symmetrical geometries, so their bond dipoles do not cancel. This makes them polar. All molecules experience London dispersion forces; however, for pure samples of non-polar species, London dispersion forces are the only intermolecular forces of attraction. <br/><br/> See Intermolecular Forces I and Chemical Bonding IV for more information.",
    },
	{
        "question"      :   "Which of the following compounds has the lowest solubility in water? ",
        "image"         :   "",
        "choices"       :   [
								"CH<sub>3</sub>F",
								"CH<sub>4</sub>",
								"CH<sub>2</sub>Cl<sub>2</sub>",
								"NH<sub>3</sub>"
                            ],
        "correct"       :   [1],
        "explanation"   :   "<br/><center><img src=\"data/images/7_mc1_4.1.png\" style=\"max-width:100%,max-height:100%\"/></center> <br/><br/>The compound above has a tetrahedral geometry but it is polar, as the bond dipoles do not cancel. It will experience dipole-dipole interactions and London dispersion forces with water. <br/><br/><center><img src=\"data/images/7_mc1_4.2.png\" style=\"max-width:100%,max-height:100%\"/></center> <br/><br/>The compound above has a tetrahedral geometry and is nonpolar, as the bond dipoles cancel due to its symmetry. It will only experience weak London dispersion and dipole-induced dipole forces with water. This compound is the lease soluble in water as it experiences the weakest intermolecular forces with water. <br/><br/><center><img src=\"data/images/7_mc1_4.3.png\" style=\"max-width:100%,max-height:100%\"/></center> <br/><br/>The compound above has a tetrahedral geometry but it is polar, as the bond dipoles do not cancel. It will experience dipole-dipole interactions and London dispersion forces with water. <br/><br/><center><img src=\"data/images/7_mc1_4.4.png\" style=\"max-width:100%,max-height:100%\"/></center> <br/><br/>The compound above has a trigonal pyramidal geometry. It is polar and it experiences hydrogen bonding and London dispersion forces with water. This compound is very soluble in water, due to the strong hydrogen bonds that it makes with neighboring water molecules. <br/><br/>See Intermolecular Forces I and Chemical Bonding III for more information. ",
    },
	{
        "question"      :   "<center><img src=\"data/images/7_mc1_5.png\" style=\"max-width:100%,max-height:100%\"/></center> <br/><br/> The structures for 2-aminopentane, C<sub>5</sub>H<sub>11</sub>NH<sub>2</sub>, and aminopropane, C<sub>3</sub>H<sub>7</sub>NH<sub>2</sub>, are shown above. Which of the below statements identifies the structure with the highest boiling point and provides a correct justification? ",
        "image"         :   "",
        "choices"       :   [
								"Aminopropane, C<sub>3</sub>H<sub>7</sub>NH<sub>2</sub>, has the higher boiling point because hydrogen bonds are the dominant intermolecular force in that structure. ",
								"Aminopropane, C<sub>3</sub>H<sub>7</sub>NH<sub>2</sub>, has the higher boiling point because its smaller size allows for interactions with a larger number of neighboring molecules",
								"2-aminopentane, C<sub>5</sub>H<sub>11</sub>NH<sub>2</sub>, has a higher boiling point because it has a longer nonpolar carbon chain that allows for stronger London dispersion forces. ",
								"2-aminopentane, C<sub>5</sub>H<sub>11</sub>NH<sub>2</sub>, has a higher boiling point because it is able to form hydrogen bonds at more locations along its structure. "
                            ],
        "correct"       :   [2],
        "explanation"   :   "Both structure are able to form hydrogen bonds with neighboring molecules due to the amino group (NH<sub>2</sub>). 2-aminopentane, C<sub>5</sub>H<sub>11</sub>NH<sub>2</sub>, has a higher boiling point because it has a longer nonpolar carbon chain that allows for stronger London dispersion forces. The boiling point of aminopropane, C<sub>3</sub>H<sub>7</sub>NH<sub>2</sub>, is -83<sup>o</sup>C and that of 2-aminopentane, C<sub>5</sub>H<sub>11</sub>NH<sub>2</sub>, is 91<sup>o</sup>C. <br/><br/>See Intermolecular Forces I for more information. ",
    },
	{
        "question"      :   "<center>CaCl<sub>2</sub><i>(s)</i> <b>-></b> Ca<sup>2+</sup><i>(aq)</i> + Cl<sup>-</sup><i>(aq)</i> &nbsp;&nbsp; ΔH<sup>o</sup>=-80 kJ/mol </center> <br/><br/> Why is the enthalpy change for the dissolving of CaCl<sub>2</sub>(s) in water an exothermic process? ",
        "image"         :   "",
        "choices"       :   [
								"The energy required to break the ionic bonds in CaCl<sub>2</sub> is greater than the energy released when the ion-dipole intermolecular forces are established. ",
								"The energy required to break the ionic bonds in CaCl<sub>2</sub> is less than the energy released when the ion-dipole intermolecular forces are established. ",
								"The energy required to break the ionic bonds in CaCl<sub>2</sub> is greater than the energy released when the dipole-induced dipole intermolecular forces are established. ",
								"The energy required to break the ionic bonds in CaCl<sub>2</sub> is less than the energy released when the dipole-induced dipole intermolecular forces are established."
                            ],
        "correct"       :   [1],
        "explanation"   :   "Energy is always required to break a bond, so breaking ionic bonds is always an endothermic process. Energy is always released when intermolecular interactions are established. This is similar to forming chemical bonds but the forces of attraction are weaker. If the overall process is exothermic, as it is here, more energy was released when the ion-dipole interactions were established than was required to break the ionic bonds.<br/><br/> If the overall process was endothermic, it required more energy to break the ionic bonds than was released when the ion-dipole interactions were established. <br/><br/>See Thermochemistry I and Intermolecular Forces I for more information. ",
    },
	{
        "question"      :   "<table> <tr><td>Diethyl <br/>ether </td><td><img src=\"data/images/7_mc1_7.1.png\" style=\"max-width:100%;max-height:100%\"/></td></tr><tr><td>Pentane</td><td><img src=\"data/images/7_mc1_7.2.png\" style=\"max-width:100%;max-height:100%\"/></td></tr><tr><td>n-butanol </td><td><img src=\"data/images/7_mc1_7.3.png\" style=\"max-width:100%;max-height:100%\"/></td></tr></table> <br/><br/>Which of the following choices lists pure samples of the compounds from the table above in order of decreasing melting point? ",
        "image"         :   "",
        "choices"       :   [
								"n-butanol > diethyl ether > pentane ",
								"pentane > diethyl ether > n-butanol ",
								"diethyl ether > n-butanol > pentane",
								"pentane > n-butanol > diethyl ether "
                            ],
        "correct"       :   [0],
        "explanation"   :   "A pure sample of the compound with the strongest intermolecular forces will have the highest melting point. All of these compounds are about the same size and have the same number of electrons, so it can be assumed that they are equally polarizable. From this, we can assume that the London dispersion forces or attraction are very similar in pure samples of all three compounds. Pentane is nonpolar, so London dispersion forces are the only intermolecular forces that exist in that pure sample. <br/><br/>The lone pairs are not drawn on these Lewis structure. This is often how such structures are drawn on the AP Chemistry Exam. You should recognize that the oxygen atoms in n-butanol and diethyl ether must have two lone pairs of electrons in order to give oxygen a full octet. Since the oxygen atom in diethyl ether has two lone pairs, that compound will be bent at the oxygen at approximately 104.5<sup>o</sup>. That geometry makes diethyl ether polar, as oxygen is more electronegative than the other elements. Thus, a pure sample of diethyl ether experiences dipole-dipole interactions and London dispersion forces. <br/><br/>A pure sample of n-butanol will experience hydrogen bonding at the –OH group and London dispersion forces. Thus, n-butanol will have the highest melting point (-89.8<sup>o</sup>C), diethyl ether will have the second highest melting point (-116<sup>o</sup>C), and pentane will have the lowest melting point (-129.8<sup>o</sup>C). <br/><br/>See Intermolecular Forces I for more information. ",
    },
	{
        "question"      :   "This question deals with pure samples of NH<sub>3</sub> and PH<sub>3</sub>.  Which of the statements below identifies the compound that has the highest boiling point with a correct justification? ",
        "image"         :   "",
        "choices"       :   [
								"A pure sample of NH<sub>3</sub> has a higher boiling point, because it is able to establish hydrogen bonds with neighboring molecules. ",
								"A pure sample of NH<sub>3</sub> has a higher boiling point, because NH<sub>3</sub> has a trigonal pyramidal geometry and PH<sub>3</sub> has a trigonal planar geometry. ",
								"A pure sample of PH<sub>3</sub> has a higher boiling point, because it is larger, has more electrons, and is more polarizable than NH<sub>3</sub>. ",
								"A pure sample of PH<sub>3</sub> has a higher boiling point, because it experiences stronger dipole-dipole interactions than NH<sub>3</sub>. "
                            ],
        "correct"       :   [0],
        "explanation"   :   "In a pure sample of NH<sub>3</sub>, London dispersion forces and hydrogen bonds are the intermolecular forces between molecules. In a pure sample of PH<sub>3</sub>, London dispersion forces and dipole-dipole interactions are the intermolecular forces between molecules. Hydrogen bonds are much stronger than dipoledipole interactions. As a greater amount of energy is required to break stronger forces of attraction, the boiling point of NH<sub>3</sub> is higher. <br/><br/>See Intermolecular Forces I for more information. ",
    },
	{
        "question"      :   "The electrical conductivity of silicon increases when it is doped with certain elements.  Which of the following statements provides an element that will increase the conductivity of Si with a correct justification? ",
        "image"         :   "",
        "choices"       :   [
								"Electrical conductivity increases when Si is doped with Al, because Al provides additional free electrons. ",
								"Electrical conductivity increases when Si is doped with Al, because Al provides positive sites that electrons can easily migrate to. ",
								"Electrical conductivity increases when Si is doped with P, because P is more electronegative than Si. ",
								"Electrical conductivity increases when Si is doped with P, because P has a slightly smaller radius which creates voids that electrons can easily migrate through. "
                            ],
        "correct"       :   [1],
        "explanation"   :   "A p-type semiconductor is created when silicon is doped with a very small amount of a Group 3A element.  This causes it to conduct better.  Group 3A elements have one less valence electron, which leaves unfilled orbitals in the valence band.  Conductivity increases, as electrons from Si are able to migrate to these positive sites.  The electrons that move to the positive holes also leave positive holes.  This can create a constant flow of electrons.<br/><br/>See Solids for more information. ",
    },
	{
        "question"      :   "Which of the following gases is most soluble in water? ",
        "image"         :   "",
        "choices"       :   [
								"Ne",
								"Cl<sub>2</sub>",
								"H<sub>2</sub>",
								"CH<sub>4</sub>"
                            ],
        "correct"       :   [1],
        "explanation"   :   "All of the gases in the list are nonpolar, so all of the gases experience only London dispersion and dipoleinduced dipole intermolecular forces.  Cl<sub>2</sub> is larger and has more electrons than any of the other choices.  Thus, Cl<sub>2</sub> is the most polarizable gas from the list and it experience the greatest intermolecular forces with water.  For that reason, Cl<sub>2</sub> is the most soluble gas from the list.<br/><br/>See Intermolecular Forces I for more information. ",
    },
	
	
];
