/** Simple JavaScript Quiz
 * version 0.1.0
 * http://journalism.berkeley.edu
 * created by: Jeremy Rue @jrue
 *
 * Copyright (c) 2013 The Regents of the University of California
 * Released under the GPL Version 2 license
 * http://www.opensource.org/licenses/gpl-2.0.php
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 */

var quiztitle = "Unit 7 MC2";

/**
* Set the information about your questions here. The correct answer string needs to match
* the correct choice exactly, as it does string matching. (case sensitive)
*
*/
var quiz = [
    {
        "question"      :   "Which of the following cations would have the weakest interactions with NH<sub>3</sub>. ",
        "image"         :   "",
        "choices"       :   [
								"Li<sup>+</sup>",
								"Mg<sup>2+</sup>",
								"Na<sup>+</sup>",
								"Ca<sup>2+</sup>"
                            ],
        "correct"       :   [2],
        "explanation"   :   "According to Coulomb’s law, forces of attraction decrease as radii increase and charges decrease. When comparing two radii that are similar, choose the ion with the smallest charge as charges tend to be more significant than distances. <br/><br/><center><div class=\"frac\"> <span>kQ<sub>1</sub>Q<sub>2</sub></span> <span class=\"symbol\">/</span> <span class=\"bottom\">r<sup>2</sup></span></div></center><br/><br/>See Intermolecular Forces I, Chemical Bonding I and Atomic Theory III for more information. ",
    },
	{
        "question"      :   "Dipole-dipole forces occur between the molecules in which of the following liquids? ",
        "image"         :   "",
        "choices"       :   [
								"Br<sub>2</sub>",
								"CH<sub>4</sub>",
								"CH<sub>3</sub>F",
								"C<sub>2</sub>H<sub>6</sub>"
                            ],
        "correct"       :   [2],
        "explanation"   :   "The molecules in (A), (B) and (D) are all symmetrical. As the electronegative differences between the central and terminal atoms are the same in these molecules, the dipoles cancel. You can think of the bond dipoles in these molecules as vectors that are the same length. Since the structures are symmetrical, those vectors will cancel. This makes the resultant vector zero. Although the molecule in (C) is also symmetrical (tetrahedral geometry) the bond dipoles do not cancel. Carbon is more electronegative than hydrogen, and fluorine is more electronegative than carbon. This results in three partially positive hydrogen atoms and a partially negative fluorine atom. As one end of the molecule consists of three positive hydrogen atoms and the other end of the molecule consists of one negative fluorine atom, the molecule is polar. Polar molecules experience dipole-dipole forces of attraction. <br/><br/><center><img src=\"data/images/7_mc2_2.png\" style=\"max-width:100%,max-height:100%\"/></center><br/><br/>See Intermolecular Forces I and Chemical Bonding IV for more information. ",
    },
	{
        "question"      :   "<img src=\"data/images/7_mc2_3.png\" style=\"max-width:100%;max-height:100%\"/><br/><br/>The heating curve for a 100.0 g sample of pure water is shown above. Heat was applied to the sample at a constant rate. Which of the following statements accounts for the difference in the lengths of lines B and D? ",
        "image"         :   "",
        "choices"       :   [
								"The specific heat of liquid water is less than that of water vapor. ",
								"The specific heat of water vapor is less than that of liquid water. ",
								"The heat of fusion of water is less than the heat of vaporization of water. ",
								"The heat of fusion of water is greater than the heat of vaporization of water.   "
                            ],
        "correct"       :   [2],
        "explanation"   :   "<b>∆H<sub>fusion</sub></b><br/><br/> As you continue to add heat to a solid that is liquefying, it stays at the same temperature until all of it has liquefied. The internal energy of the system is increasing, but the temperature is not, because the energy is being used to stretch and sever intermolecular forces. The heat required to liquefy 1.0 mole of a substance is its enthalpy (heat) of fusion, ∆H<sub>fus</sub>. For water, this occurs at 0.0<sup>o</sup>C. <br/><br/><b>∆H<sub>vaporization</sub></b><br/><br/>When a system reaches its boiling point, it stays at the same temperature until all of the liquid has vaporized. The heat that is being injected into the system is being used to sever intermolecular forces. The heat absorbed in this process is the enthalpy of vaporization. For water this occurs at 100.0<sup>o</sup>C. <br/><br/>See Intermolecular Forces II for more information. ",
    },
	{
        "question"      :   "<img src=\"data/images/7_mc2_4.png\" style=\"max-width:100%;max-height:100%\"/><br/><br/>The Lewis structures for octane, C<sub>8</sub>H<sub>18</sub>, and decane, C<sub>10</sub>H<sub>22</sub>, are above. Which of the following statements correctly identifies the pure liquid that has the highest equilibrium vapor pressure at 65<sup>o</sup>C with a correct justification? ",
        "image"         :   "",
        "choices"       :   [
								"C<sub>8</sub>H<sub>18</sub><i>(l)</i> has the higher vapor pressure at 65<sup>o</sup>C, because its  London dispersion forces are stronger",
								"C<sub>8</sub>H<sub>18</sub><i>(l)</i> has the higher vapor pressure at 65<sup>o</sup>C, because its  London dispersion forces are weaker. ",
								"C<sub>10</sub>H<sub>22</sub><i>(l)</i> has the higher vapor pressure at 65<sup>o</sup>C, because its  London dispersion forces are stronger. ",
								"C<sub>10</sub>H<sub>22</sub><i>(l)</i> has the higher vapor pressure at 65<sup>o</sup>C, because its  London dispersion forces are weaker. "
                            ],
        "correct"       :   [1],
        "explanation"   :   "Both structures are nonpolar, so they only experience London dispersion forces of attraction. C<sub>8</sub>H<sub>18</sub> is smaller, has fewer electrons, and, therefore, is less polarizable. For this reason, C<sub>8</sub>H<sub>18</sub> will experience weaker London dispersion forces. Because of the weaker forces of attraction, the rate at which C<sub>8</sub>H<sub>18</sub> molecules are able to break free from neighboring molecules and enter the gas phase will be higher than that of C<sub>10</sub>H<sub>22</sub> when both are at the same temperature. <br/><br/>See Intermolecular Forces I for more information. ",
    },
	{
        "question"      :   "<center><img src=\"data/images/7_mc2_5.png\" style=\"max-width:100%;max-height:100%\"/></center><br/><br/>The Lewis structure for pentane, C<sub>5</sub>H<sub>12</sub>, is above. A student mixes equal volumes of C<sub>5</sub>H<sub>12</sub> and NH<sub>3</sub>. Identify the type(s) of intermolecular forces that exist between C<sub>5</sub>H<sub>12</sub> and NH<sub>3</sub>. ",
        "image"         :   "",
        "choices"       :   [
								"Hydrogen bonds and London dispersion forces ",
								"Dipole-dipole and London dispersion forces ",
								"Dipole-induced dipole and London dispersion forces ",
								"Ion-dipole and London dispersion forces "
                            ],
        "correct"       :   [2],
        "explanation"   :   "Pentane, C<sub>5</sub>H<sub>12</sub>, has a linear geometry overall and is nonpolar. NH<sub>3</sub> has a trigonal pyramidal geometry and is polar. NH<sub>3</sub> can induce a dipole in C<sub>5</sub>H<sub>12</sub>. Electrons in the non-polar molecule are attracted to the positive end on the polar molecule. This will cause a temporary increase in the electron density on the side of the non-polar molecule that is facing the positive end of the polar molecule. The extra electron density will only remain on the side of the molecule that points toward the positive end of the polar molecule for a very brief moment of time, as the electrons continue to move. A moment later they will re-align. Thus, the increase in electron density on one side of the molecule is temporary but constantly reoccurring. This type of attractive force produces a series of small tugs. These small tugs sum up to an overall net force of attraction. However, the attraction is small, as it is constantly being turned on and off. <br/><br/>See Intermolecular Forces I for more information. ",
    },
	{
        "question"      :   "Which of the following correctly depicts a hydrogen bond?  Hydrogen bonds are represented with the dashed red lines. ",
        "image"         :   "",
        "choices"       :   [
								"<img src=\"data/images/7_mc2_6.1.png\" style=\"max-width:100%;max-height:100%\"/>",
								"<img src=\"data/images/7_mc2_6.2.png\" style=\"max-width:100%;max-height:100%\"/>",
								"<img src=\"data/images/7_mc2_6.3.png\" style=\"max-width:100%;max-height:100%\"/>",
								"<img src=\"data/images/7_mc2_6.4.png\" style=\"max-width:100%;max-height:100%\"/>"
                            ],
        "correct"       :   [2],
        "explanation"   :   "A hydrogen bonds occurs between a hydrogen, which is covalently bonded to a fluorine, oxygen, or nitrogen; and another fluorine, oxygen, or nitrogen with at least one lone pair.<br/><br/>See Intermolecular Forces I for more information. ",
    },
	{
        "question"      :   "<center><table> <tr><td>Pure Substance </td><td>Vapor Pressure<br/> at 20<sup>o</sup>C </td></tr><tr><td>C<sub>4</sub>H<sub>8</sub>O<i>(l)</i></td><td>115 torr</td></tr><tr><td>C<sub>4</sub>H<sub>9</sub>OH(l)</td><td>6 torr</td></tr><tr><td>C<sub>5</sub>H<sub>12</sub>(l)</td><td>420 torr</td></tr><tr><td>CH<sub>3</sub>COCH<sub>2</sub>CH<sub>3</sub>(l)</td><td>79 torr</td></tr></table> </center><br/><br/>A pure sample of which compound will have the lowest boiling point? ",
        "image"         :   "",
        "choices"       :   [
								"C<sub>4</sub>H<sub>8</sub>O<i>(l)</i>",
								"C<sub>4</sub>H<sub>9</sub>OH<i>(l)</i>",
								"C<sub>5</sub>H<sub>12</sub><i>(l)</i>",
								"CH<sub>3</sub>COCH<sub>2</sub>CH<sub>3</sub><i>(l)</i>"
                            ],
        "correct"       :   [2],
        "explanation"   :   "The substance with the highest equilibrium vapor pressure will have the weakest intermolecular forces, because the rate at which these molecules break free from their attractions for neighboring molecules in order to leave the liquid phase and enter the gas phase is greater than that of the other substances. Since a pure sample of C<sub>5</sub>H<sub>12</sub><i>(l)</i> has the highest vapor pressure at 20<sup>o</sup>C, it has the weakest intermolecular forces, and thus, will have the lowest boiling point. <br/><br/>See Intermolecular Forces I for more information. ",
    },
	{
        "question"      :   "This question deals with pure samples of BF<sub>3</sub> and BCl<sub>3</sub>.  Which of the statements below identifies the compound that has the highest boiling point with a correct justification? ",
        "image"         :   "",
        "choices"       :   [
								"A pure sample of BF<sub>3</sub> has a higher boiling point, because the covalent bonds are stronger",
								"A pure sample of BF<sub>3</sub> has a higher boiling point, because BH<sub>3</sub> has a trigonal pyramidal geometry and BCl<sub>3</sub> has a trigonal planar geometry. ",
								"A pure sample of BCl<sub>3</sub> has a higher boiling point, because it is larger, has more electrons, and is more polarizable than BF<sub>3</sub>. ",
								"A pure sample of BCl<sub>3</sub> has a higher boiling point, because it experiences stronger dipole-dipole interactions than BCl<sub>3</sub>"
                            ],
        "correct"       :   [2],
        "explanation"   :   "BF<sub>3</sub> and BCl<sub>3</sub> have trigonal planar geometries and are nonpolar, so London dispersion forces are the only intermolecular force of attraction between neighboring molecules in their pure samples. BCl<sub>3</sub> is larger and has more electrons, making it more polarizable than BF<sub>3</sub>. Thus, BCl<sub>3</sub> experiences greater London dispersion forces. As a greater amount of energy is required to break stronger forces of attraction, the boiling point of BCl<sub>3</sub> is higher. <br/><br/>See Intermolecular Forces I for more information. ",
    },
	{
        "question"      :   "When boiling pure water at sea level, heat continues to be absorbed by the water, but the temperature remains as 100.<sup>o</sup>C.  Which of the following statements provides the best explanation for why this is? ",
        "image"         :   "",
        "choices"       :   [
								"The energy being absorbed is used to break covalent bonds in the molecules.   ",
								"The energy being absorbed is used to break the intermolecular forces between neighboring water molecules",
								"The energy being absorbed increases the average kinetic energy of the water molecules in the liquid phase which helps to push them into the gas phase. ",
								"The thermal energy absorbed by the water is equal to the total kinetic energy of the water molecules leaving the surface of the liquid. "
                            ],
        "correct"       :   [1],
        "explanation"   :   "In a gas, we generally assume that particles have no intermolecular forces of attraction. In a liquid, attractive forces are still quite large. When water is boiling, the heat that is absorbed is used to break the intermolecular forces between neighboring water molecules, which is what allows the molecules to enter the gas phase. <br/><br/>Phase changes for pure samples of molecular compounds, such as liquid to gas, result from changes in intermolecular forces, not chemical bonds. No chemical bonds are broken in the following process. <br/><br/><center>H<sub>2</sub>O(l) <b>-></b> H<sub>2</sub>O(g) </center><br/><br/>Temperature is a measure of the average kinetic energy of the molecules in a system, so if the temperature of the liquid does not increase, the average kinetic energy of the molecules did not increase. <br/><br/>See Intermolecular Forces II for more information. ",
    },
	{
        "question"      :   "Which of the following pure substances has the highest melting point? ",
        "image"         :   "",
        "choices"       :   [
								"CH<sub>4</sub>",
								"C<sub>6</sub>H<sub>14</sub>",
								"BBr<sub>3</sub>",
								"SiO<sub>2</sub>"
                            ],
        "correct"       :   [3],
        "explanation"   :   "SiO<sub>2</sub> is a covalent network solid. Covalent network solids have very high melting temperatures, as all of the atoms are covalently bonded together. In other words, there are not any intermolecular forces. It is a covalent network of SiO<sub>4</sub> tetrahedra, where every silicon atom is covalently bonded to four oxygen atoms and every oxygen atom is covalently bonded to two silicon atoms. The melting point of SiO<sub>2</sub> is 1710 <sup>o</sup>C. <br/><br/>See Solids for more information. ",
    },
];
