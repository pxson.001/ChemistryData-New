/** Simple JavaScript Quiz
 * version 0.1.0
 * http://journalism.berkeley.edu
 * created by: Jeremy Rue @jrue
 *
 * Copyright (c) 2013 The Regents of the University of California
 * Released under the GPL Version 2 license
 * http://www.opensource.org/licenses/gpl-2.0.php
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 */

var quiztitle = "Unit 4 MC2";

/**
* Set the information about your questions here. The correct answer string needs to match
* the correct choice exactly, as it does string matching. (case sensitive)
*
*/
var quiz = [
    {
        "question"      :   "<center> Ba<sup>2+</sup><i>(aq)</i> + SO<sub>4</sub><sup>2-</sup><i>(aq)</i> <b>-></b> BaSO<sub>4</sub><i>(s)</i></center> <br/><br/> In order to find ΔH<sub>rxn</sub> for the above process, a student mixes 100.0 mL of 1.0 M Na<sub>2</sub>SO<sub>4</sub> at 25.0<sup>o</sup>C with 100.0 mL of 1.0 M Ba(NO<sub>3</sub>)<sub>2</sub> at 25.0<sup>o</sup>C. The temperature of the solution went up to 28.0<sup>o</sup>C. Assume that the density of the solution is 1.0 g/mL and the specific heat of the solution is 4.2 J/(g·K). Find the experimental value for ΔH<sub>rxn</sub>.",
        "image"         :   "",
        "choices"       :   [
								"-25 kJ/mol",
								"-13 kJ/mol",
								"-25,000 kJ/mol",
								"-13,000 kJ/mol"
                            ],
        "correct"       :   [0],
        "explanation"   :   "<br/><i>m</i> = 200.0 mL x 1.0 g/mL = 2.0 x 10<sup>2</sup> g <br/><br/> q<sub>sol</sub> = mc∆T <br/><br/> q<sub>sol</sub> = 2.0 x 10<sub>2</sub> g (4.2 J/(g·K))(28.0<sup>o</sup>C – 25.0<sup>o</sup>C) <br/><br/> q<sub>sol</sub> = 2500 J <br/><br/> q<sub>rxn</sub> = - q<sub>sol</sub> <br/><br/> q<sub>rxn</sub> = - 2500 J (The heat gained by the solution equal the heat lost by the reaction) <br/><br/> 0.1000 L x<div class=\"frac\"> <span>1.0 mol Ba<sup>2+</sup></span> <span class=\"symbol\">/</span> <span class=\"bottom\">1 L</span></div> = 0.10 mol Ba<sup>2+</sup> <br/><br/> ∆H<sub>rxn</sub> =<div class=\"frac\"> <span>q<sub>rxn</sub></span> <span class=\"symbol\">/</span> <span class=\"bottom\">mol BaSO<sub>4</sub></span></div> <br/><br/> ∆H<sub>rxn</sub> =<div class=\"frac\"> <span>-2500 J</span> <span class=\"symbol\">/</span> <span class=\"bottom\">0.10 mol BaSO<sub>4</sub></span></div> <br/><br/> ∆H<sub>rxn</sub> = -25000 J/mol <br/><br/> ∆H<sub>rxn</sub> = -25 kJ/mol <br/><br/> See Thermodynamics II for more information.",
    },
	{
        "question"      :   "<center> 2 H<sub>2</sub>O<i>(l)</i> <b>-></b> 2 H<sub>2</sub><i>(g)</i> + O<sub>2</sub><i>(g)</i> &nbsp;&nbsp; ∆H<sup>o</sup><sub>298</sub> = + 572 kJ/mol; ∆S<sup>o</sup><sub>298</sub> = + 327 J/molK</center> <br/><br/> Identify the correct statement about the thermodynamic favorability of the above process.",
        "image"         :   "",
        "choices"       :   [
								"The process is not thermodynamically favored at 298K, but it becomes thermodynamically favored at higher temperatures.",
								"The process is not thermodynamically favored at 298K, but it becomes thermodynamically favored at lower temperatures.",
								"The process is thermodynamically favored at 298K and it is driven by the positive ∆S value",
								"This process is not thermodynamically favored at any temperature."
                            ],
        "correct"       :   [0],
        "explanation"   :   "<br/>∆G<sup>o</sup> = ∆H<sup>o</sup> - T∆S<sup>o</sup> <br/> ∆G<sup>o</sup> = 572 kJ - 298 K(0.327 kJ/K) <br/> ∆G<sup>o</sup> = 475 kJ/mol (Non-TFP) <br/><br/> A positive ∆G value tells you that this is not a thermodynamically favored process. The significance of the favourably positive ∆S value increases as the temperature increases, because you are multiplying it by a larger number. Thus, at high temperatures, the absolute value of -T∆S will exceed that of ∆H, making ∆G negative. From this, we know that the process becomes thermodynamically favored at high temperatures. <br/><br/> As students do not have calculators for the multiple choice section, it is recommended that you round numbers to make the calculations faster and easier. Instead of 298 K, use 300 K. Instead of 0.327 kJ/K, use 0.3 kJ/K. <br/><br/> See Thermodynamics III for more information.",
    },
	{
        "question"      :   "A hot piece of metal is placed in a calorimeter containing water at 25<sup>o</sup>C. If 21 kJ of heat is transferred to the water, which causes the water’s temperature rises to 30.<sup>o</sup>C, what is the volume of water in the calorimeter? The specific heat of water is 4.2 J/(g·K). ",
        "image"         :   "",
        "choices"       :   [
								"1 mL H<sub>2</sub>O",
								"10 mL H<sub>2</sub>O",
								"100 mL H<sub>2</sub>O",
								"1000 mL H<sub>2</sub>O"
                            ],
        "correct"       :   [2],
        "explanation"   :   "<br/><i>q<sub>w</sub></i> = mc∆T <br/> <i>m</i> =<div class=\"frac\"> <span><i>q<sub>w</sub></i></span> <span class=\"symbol\">/</span> <span class=\"bottom\">c∆T</span></div> =<div class=\"frac\"> <span>2100 J</span> <span class=\"symbol\">/</span> <span class=\"bottom\">(4.2 J / g<sup>o</sup> C)(30 <sup>o</sup>C - 25 <sup>o</sup>C)</span></div> <br/> <i>m</i> =<div class=\"frac\"> <span>2100 J</span> <span class=\"symbol\">/</span> <span class=\"bottom\">21 J/g</span></div> <br/> <i>q<sub>w</sub></i> = 100 g H<sub>2</sub>O <br/> <i>V<sub>w</sub></i> = 100 mL H<sub>2</sub>O <br/><br/> See Thermodynamics II for more information.",
    },
	{
		"question"      :   "<center> 8 Fe<i>(s)</i> + 6 O<sub>2</sub><i>(g)</i> <b>-></b> 4 Fe<sub>2</sub>O<sub>3</sub><i>(s)</i> &nbsp;&nbsp; ∆H<sup>o</sup><sub>298</sub> = -3296.8 kJ</center> <br/><br/> The process above is thermodynamically favored at 298 K. Which of the following is true about the free energy and entropy of the system at 298 K?",
        "image"         :   "",
        "choices"       :   [
								"∆G<sup>o</sup><sub>298</sub> > 0 and ∆S<sup>o</sup><sub>298</sub> > 0",
								"∆G<sup>o</sup><sub>298</sub> > 0 and ∆S<sup>o</sup><sub>298</sub> < 0",
								"∆G<sup>o</sup><sub>298</sub> < 0 and ∆S<sup>o</sup><sub>298</sub> > 0",
								"∆G<sup>o</sup><sub>298</sub> > 0 and ∆S<sup>o</sup><sub>298</sub> < 0"
                            ],
        "correct"       :   [3],
        "explanation"   :   "<center> ∆G<sup>o</sup> =∆H<sup>o</sup> - T∆S<sup>o</sup></center> <br/><br/> ∆G<sup>o</sup> is always less than zero for a thermodynamically favored process. ∆S<sup>o</sup> must be less than zero, as the balanced chemical equation shows a reduction in the number of moles – 14 moles of reactants become 4 moles of products. Furthermore, it shows six moles of gas phase reactants and only solid phase products. Both demonstrate a decrease in disorder. Since ∆H<sup>o</sup> is favorably negative, the process can still be thermodynamically favored even though its ∆S<sup>o</sup> value is unfavorably negative. At higher temperatures, this process can become non-thermodynamically favored, as the higher value for the temperature would increase the worth of the unfavorably negative ∆S<sup>o</sup>. <br/><br/> See Thermodynamics III for more information.",
	},
	{
		"question"      :   "∆S<sup>o</sup> < 0 in which of the following reactions?",
        "image"         :   "",
        "choices"       :   [
								"Cu<i>(s)</i> + 4 HNO<sub>3</sub><i>(aq)</i> <b>-></b> Cu(NO<sub>3</sub>)<sub>3</sub><i>(aq)</i> + 2 NO<sub>2</sub><i>(g)</i> + 2 H<sub>2</sub>O<i>(l)</i>",
								"2 H<sub>2</sub>O<sub>2</sub><i>(aq)</i> <b>-></b> 2 H<sub>2</sub>O<i>(l)</i> + O<sub>2</sub><i>(g)</i>",
								"2 Na<i>(s)<i> + 2 H<sub>2</sub>O(l) <b>-></b> 2 Na<sup>+</sup><i>(aq)</i> + 2 OH<sup>-</sup><i>(aq)</i> + H<sub>2</sub><i>(g)</i>",
								"3 O<sub>2</sub><i>(g)</i> <b>-></b> 2 O<sub>3</sub><i>(g)</i>"
                            ],
        "correct"       :   [3],
        "explanation"   :   "∆S<sup>o</sup> > 0 for choices (A), (B) and (C). All three of these choices show an increase in the number of moles as reactants turn into products. In some cases they also show the formation of more random phases of matter, such as gases. Answer choice (D) is the only reaction that shows a decrease in disorder, as three moles of gas become two moles of gas. This is the only reaction where ∆S<sup>o</sup> < 0.<br/><br/> See Thermodynamics III for more information. ",
	},
	{
		"question"      :   "<center> N<sub>2</sub>O<i>(g)</i> + NO<sub>2</sub><i>(g)</i> <b>-></b> 3 NO<i>(g)</i> &nbsp;&nbsp; ∆H<sub>rxn</sub> = 157 kJ/mol</center> <br/><br/> Which of the following statements about the bond energies for the above reaction is true?",
        "image"         :   "",
        "choices"       :   [
								"The energy required to break the bonds of the reactants is greater than the energy released when the bonds of the products form.",
								"The energy released when the bonds of the reactants are broken is greater than the energy required to form the bonds of the products.",
								"The energy required to break the bonds of the reactants is less than the energy released when the bonds of the products form.",
								"The energy released when the bonds of the reactants are broken is less than the energy required to form the bonds of the products."
                            ],
        "correct"       :   [0],
        "explanation"   :   "It always requires energy to break a bond, and energy is always released when a bond is formed. If more energy is required to break the bonds of the reactants than is released when the bonds of the products form, the reaction is endothermic and value for ∆H<sub>rxn</sub> is positive.<br/></br> See Thermodynamics I for more information. ",
	},
	{
		"question"      :   "<img src=\"data/images/4_mc2_7.png\" style=\"max-width:100%;max-height:100%\"/> <br/><br/> The graph above shows the speed distribution of a pure sample of the same gas at three different temperatures. Identify the order of increasing temperature for the three plots.",
        "image"         :   "",
        "choices"       :   [
								"Temperature A < Temperature B < Temperature C",
								"Temperature C < Temperature B < Temperature A",
								"Temperature A < Temperature C < Temperature B",
								"Temperature B < Temperature C < Temperature A"
                            ],
        "correct"       :   [0],
        "explanation"   :   "Temperature is a measure of the average kinetic energy of the particles in a system. The formula for kinetic energy is KE = ½mv<sup>2</sup>. The mass of the particles does not change, so kinetic energy increases as velocity, or speed, increases. Since the average speed increases from A to B to C, the temperature must also increase from A to B to C.<br/><br/> See Thermodynamics I for more information.",
	},
	{
		"question"      :   "<center> N<sub>2</sub><i>(g)</i> + 3 H<sub>2</sub><i>(g)</i> <b>-></b> 2 NH<sub>3</sub><i>(g)</i> &nbsp;&nbsp; ∆H<sub>rxn</sub> = - 90 kJ/mol</center> <br/><br/> How much heat is released or absorbed when 0.03 moles of H<sub>2</sub><i>(g)</i> are formed from NH<sub>3</sub><i>(g)</i>?",
        "image"         :   "",
        "choices"       :   [
								"0.9 kJ are released",
								"0.9 kJ are absorbed",
								"2.7 kJ are released",
								"2.7 kJ are absorbed"
                            ],
        "correct"       :   [1],
        "explanation"   :   "0.03 mol H<sub>2</sub> x<div class=\"frac\"> <span>90 kJ</span> <span class=\"symbol\">/</span> <span class=\"bottom\">3 mol H<sub>2</sub></span></div> = 0.9 kJ <br/><br/> The reaction is flipped when H<sub>2</sub> is being produced. Since the reverse reaction is endothermic, 0.9 kJ of heat is absorbed. 90 kJ is divided by three moles of hydrogen gas, as there are three moles of hydrogen gas in the balanced equation. ∆H<sub>rxn</sub> is the heat lost or gained is the balanced chemical equation. In this case, 90 kJ of heat is lost when one mole of nitrogen gas reacts with three moles of hydrogen gas to produce two mole of ammonia.<br/><br/> See Thermodynamics II for more information.",
	},
	{
		"question"      :   "Which of the following is a true statement about entropy?",
        "image"         :   "",
        "choices"       :   [
								"A negative ∆S value indicates an increase in entropy",
								"The evaporation of a liquid causes an increase in entropy.",
								"Dissolving KNO<sub>3</sub> in water decreases entropy.",
								"The entropy of the universe is always decreasing."
                            ],
        "correct"       :   [1],
        "explanation"   :   "A positive ∆S value indicates an increase in entropy, dissolving KNO<sub>3</sub> in water increases entropy, and the entropy of the universe is always increasing. <br/> Evaporation causes an increase in entropy. Forces of attraction between molecules in the gas phase are very weak under normal conditions, so they spread out and are free to move as they please causing the number of possible arrangements increases by a very large degree. ∆S = S<sub>products</sub> - S<sub>reactants</sub> >> 0 <br/><br/> See Thermodynamics III for more information.",
	},
	{
		"question"      :   "If 70. g of water at 80.<sup>o</sup>C is mixed with 30. g of water at 10.<sup>o</sup>C in an insulated cup, what will be the final temperature of the solution? ",
        "image"         :   "",
        "choices"       :   [
								"45<sup>o</sup>C",
								"59<sup>o</sup>C",
								"66<sup>o</sup>C",
								"133<sup>o</sup>C"
                            ],
        "correct"       :   [1],
        "explanation"   :   "The heat lost by the hot water equals the heat gained by the cold water. <br/><br/> <i> q<sub>hot</sub> = - q<sub>cold</sub> </i><br/> m<sub>hot</sub>c∆T<sub>hot</sub> = -(m<sub>cold</sub>c∆T<sub>cold</sub>) <br/> 70.(T<sub>f</sub> - T<sub>i</sub>) = -30.(T<sub>f</sub> - T<sub>i</sub>) <br/> 70.(T<sub>f</sub> - 80.) = -30.(T<sub>f</sub> - 10.) <br/> 70.T<sub>f</sub> - 5600 = -30T<sub>f</sub> + 300 <br/> 100T<sub>f</sub> = 5900<br/> T<sub>f</sub> = 59<sup>o</sup>C<br/><br/> See Thermodynamics II for more information.",
	}
];
