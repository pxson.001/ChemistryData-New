/** Simple JavaScript Quiz
 * version 0.1.0
 * http://journalism.berkeley.edu
 * created by: Jeremy Rue @jrue
 *
 * Copyright (c) 2013 The Regents of the University of California
 * Released under the GPL Version 2 license
 * http://www.opensource.org/licenses/gpl-2.0.php
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 */

var quiztitle = "Unit 4 MC1";

/**
* Set the information about your questions here. The correct answer string needs to match
* the correct choice exactly, as it does string matching. (case sensitive)
*
*/
var quiz = [
    {
        "question"      :   "Identify the molecule that has the greatest bond enthalpy.",
        "image"         :   "",
        "choices"       :   [
								"H<sub>2</sub>",
								"N<sub>2</sub>",
								"O<sub>2</sub>",
								"F<sub>2</sub>"
                            ],
        "correct"       :   [1],
        "explanation"   :   "As you increase the number of bonds between two atoms, the energy contained within the bond increases and the distance between the nuclei decreases.  The more electrons you put between the two nuclei, the stronger the attraction that those positive nuclei have for the negative electrons.  The two nuclei have a stronger attraction for three shared pairs of electrons than they do for two shared pairs of electrons.  Furthermore, two nuclei have a stronger attraction for two shared pairs of electrons than they do for one shared pair of electrons.<br/><br/>See Chemical Bonding II and Thermodynamics I for more information. ",
    },
	{
        "question"      :   "<center>2 NO<sub>2</sub><i>(g)</i> + 7 H<sub>2</sub><i>(g)</i> <b>-></b> 2 NH<sub>3</sub><i>(g)</i> + 4 H<sub>2</sub>O<i>(l)</i> ΔH<sub>rxn</sub>=-1300. kJ </center> <br/><br/> A 4.6 g sample of NO<sub>2</sub><i>(g)</i> (molar mass 46 g/mol) is mixed with an 7.0 gram sample of H<sub>2</sub><i>(g)</i> (molar mass 2.0 g/mol) and the reaction above goes to completion. Which of the following identifies the limiting reactant and the amount of heat released, q.",
        "image"         :   "",
        "choices"       :   [
								"H<sub>2</sub> is the limiting reactant.  q = -1300 kJ",
								"H<sub>2</sub> is the limiting reactant.  q = -650 kJ ",
								"NO<sub>2</sub> is the limiting reactant.  q = -130 kJ ",
								"NO<sub>2</sub> is the limiting reactant.  q = -65 kJ "
                            ],
        "correct"       :   [3],
        "explanation"   :   "<br/>4.6 g NO<sub>2</sub> x <div class=\"frac\"> <span>1 mol NO<sub>2</sub></span> <span class=\"symbol\">/</span> <span class=\"bottom\">46 g NO<sub>2</sub></span></div>x<div class=\"frac\"> <span>2 mol NH<sub>3</sub></span> <span class=\"symbol\">/</span> <span class=\"bottom\">2 mol NO<sub>2</sub></span></div>=0.10 mol NH<sub>3</sub><br/><br/>7.0 g H<sub>2</sub> x <div class=\"frac\"> <span>1 mol H<sub>2</sub></span> <span class=\"symbol\">/</span> <span class=\"bottom\">2.0 g H<sub>2</sub></span></div>x<div class=\"frac\"> <span>2 mol NH<sub>3</sub></span> <span class=\"symbol\">/</span> <span class=\"bottom\">7 mol H<sub>2</sub></span></div>=1.0 mol NH<sub>3</sub><br/><br/>NO<sub>2</sub> is the limiting reactant as it produces fewer moles of NH<sub>3</sub> .<br/><br/>0.10 mol NO<sub>2</sub> x <div class=\"frac\"> <span>-1300kJ</span> <span class=\"symbol\">/</span> <span class=\"bottom\">2 mol NO<sub>2</sub></span></div>=-65kJ<br/><br/>See Thermodynamics II for more information. ",
    },
	{
        "question"      :   "<center><table> <tr><th>Bond</th><th>Average Bond Enthalpies </th> </tr><tr><td>H - H<br/><br/>N - N<br/><br/>N=N<br/><br/>N ≡ N<br/><br/>N - H</td><td>436 kJ/mol<br/><br/>193 kJ/mol<br/><br/>418 kJ/mol<br/><br/>941 kJ/mol<br/><br/>393 kJ/mol</td></tr></table> </center><br/><br/><center>N<sub>2</sub><i>(g)</i> + 3 H<sub>2</sub><i>(g)</i> <b>-></b> 2 NH<sub>3</sub><i>(g)</i> </center> <br/><br/> Use the data in the above table to determine the correct enthalpy change, ∆H<sub>rxn</sub>, that occurs in the above reaction. ",
        "image"         :   "",
        "choices"       :   [
								"-632 kJ/mol ",
								"-857 kJ/mol",
								"-109 kJ/mol ",
								"109 kJ/mol "
                            ],
        "correct"       :   [2],
        "explanation"   :   "<br/><img src=\"data/images/4_mc1_3.1.png\" style=\"max-width:100%;max-height:100%\"/><img src=\"data/images/4_mc1_3.2.png\" style=\"max-width:100%;max-height:100%\"/><img src=\"data/images/4_mc1_3.3.png\" style=\"max-width:100%;max-height:100%\"/><br/><br/>&Delta;H=&Sigma;(BE)<i><sub>bonds broken</sub></i> - &Sigma;(BE)<i><sub>bonds formed</sub></i><br/>&Delta;H=941 kJ + 3(436 kJ) - (2 x 3 x 393 kJ)<br/>&Delta;H=-109 kJ/mol <br/><br/>See Thermodynamics I for more information. ",
    },
	{
		"question"		:	"<img src=\"data/images/4_mc1_4.png\" style=\"max-width:100%;max-height:100%\"/> <br/><br/> The heating curve for a 200.0 g sample of pure water is shown above. Which or the following statements about the entropy of the water is true as the sample absorbs heat?",
		"image"			:	"",
		"choices"		:	[
								"The entropy of the water only increases during sections A, C, and E.",
								"The entropy of the water only increases during sections B and D.",
								"The entropy of the water increases during sections A, B, C, D, and E.",
								"The entropy of the water decreases during sections A, B, C, D, and E."
							],
		"correct"		:	[2],
		"explanation"	:	"At 0 Kelvin the entropy is zero, as everything would be lined up in perfect crystals and there would be no motion whatsoever.  Therefore there is no randomness at absolute zero.  As temperature increases from that point, vibration increases within a solid, which causes an increase in entropy.  At the melting point, the temperature does not increase, although heat continues to be absorbed.  The entropy increases greatly at this point (entropy of fusion), as molecules or ions gain the freedom to translate (move in a straight line) and rotate; thereby greatly increasing the number of ways that they can be arranged.  As heat is added to the liquid, these movements become more rapid, and entropy continues to increase.  A much greater increase in entropy occurs at the boiling point (entropy of vaporization).  Here, the particles acquire total freedom from intermolecular forces (ideally).  Their motion, velocity, and randomness increase by the largest degree at this point.  If you continue to heat the gas, the particles will move further away from one another as their velocities increase, thereby further increasing their entropy.<br/><br/>See Thermodynamics III for more information.",
	},
	{
		"question"		:	"<center><table> <tr><th>Bond</th><th>Average Bond Enthalpies</th> </tr><tr><td>O=C<br/><br/>O=O<br/><br/>C - H</td><td>8.0 x 10<sup>2</sup> kJ/mol<br/><br/>5.0 x 10<sup>2</sup> kJ/mol<br/><br/>4.1 x 10<sup>2</sup> kJ/mol</td></tr></table> </center><br/><br/><center>CH<sub>4</sub><i>(g)</i> + 2 O<sub>2</sub><i>(g)</i> <b>-></b> CO<sub>2</sub><i>(g)</i> + 2 H<sub>2</sub>O<i>(g)</i> ∆H<sub>rxn</sub>=- 8.0 x 102 kJ </center><br/><br/>Use the thermochemical equation for the combustion of methane and the table of average bond energies above to find the average bond energy in an O – H bond.",
		"image"			:	"",
		"choices"		:	[
								 "340 kJ/mol",
								 "670 kJ/mol",
								 "460 kJ/mol",
								 "910 kJ/mol"
							],
		"correct"		:	[2],
		"explanation"	:	"<br/>&Delta;H=&Sigma;(BE)<i><sub>bonds broken</sub></i> - &Sigma;(BE)<i><sub>bonds formed</sub></i><br/><br/>-800 kJ=4(BE C-H) + 2(BE O=O) - [2(BE C=O) + 4 (BE O-H)]<br/><br/>-800 kJ=4(410 kJ) + 2(500 kJ) - 2(800 kJ) - 4 (BE O-H)<br/><br/>(BE O-H)=460 kJ/mol<br/><br/>See Thermodynamics I for more information. ",
	},
	{
		"question"		:	"A 10. g piece of iron at 45<sup>o</sup>C is placed in an insulated cup containing 10. g of water at 25<sup>o</sup>C. Which of the following statements about the final temperature of the water and iron is true?",
		"image"			:	"",
		"choices"		:	[
								"The final temperature will be 35<sup>o</sup>C.",
								"The final temperature will be less than 45<sup>o</sup>C and greater than 35<sup>o</sup>C.",
								"The final temperature will be less than 35<sup>o</sup>C and greater than 25<sup>o</sup>C.",
								"The final temperature will be 25<sup>o</sup>C. "
							],
		"correct"		:	[2],
		"explanation"	:	"Metals have low specific heat values and water has a relatively high specific heat value. The specific heat of iron is 0.450 J/g<sup>o</sup>C and the specific heat of water is 4.184 J/g<sup>o</sup>C. The calculation for the heat lost by iron is below. <br/><br/> <i>q</i><sub>Fe</sub> = <i>m</i><sub>Fe</sub><i>c</i><sub>Fe</sub><i>∆T</i><sub>Fe</sub> <br/> <i>q</i><sub>Fe</sub> = 10. g(0.450 J/g<sup>o</sup>C)<i>∆T</i><sub>Fe</sub> <br/><br/> The heat gained by the water can be calculated as follows. <br/><br/> <i>q</i><sub>H<sub>2</sub>O</sub> = <i>m</i><sub>H<sub>2</sub>O</sub><i>c</i><sub>H<sub>2</sub>O</sub><i>∆T</i><sub>H<sub>2</sub>O</sub> <br/> <i>q</i><sub>H<sub>2</sub>O</sub> = 10. g(4.184 J/g<sup>o</sup>C)<i>∆T</i><sub>H<sub>2</sub>O</sub> <br/></br> The heat lost by the iron is equal to the heat gained by the water, so… <br/><br/><center><i>q</i><sub>H<sub>2</sub>O</sub> = -<i>q</i><sub>Fe</sub></center> <br/> 10. g(4.184 J/g<sup>o</sup>C)<i>∆T</i><sub>H<sub>2</sub>O</sub> = -(10. g(0.450 J/g<sup>o</sup>C)<i>∆T</i><sub>Fe</sub>) <br/><br/> From the above, one can see that the temperature change of the iron must be larger than the temperature change of the water. For that reason, the final temperature of the water and iron must be less than 35<sup>o</sup>C (halfway between the two initial temperatures) and greater than 25<sup>o</sup>C. <br/><br/> See Thermodynamics II for more information",
	},
	{
		"question"		:	"<center> A<i>(s)</i> + BC<sub>2</sub><i>(g)</i> <b>-></b> AC<i>(g)</i> + BC<i>(g)</i> ∆H<sup>o</sup><sub>298</sub> = 596 kJ/mol</center> <br/><br/> If the above process is thermodynamically favored at 298 K, which of the following statements must be true?",
		"image"			:	"",
		"choices"		:	[
								"∆S<sup>o</sup><sub>298</sub> > 0",
								"∆S<sup>o</sup><sub>298</sub> > 2",
								"∆S<sup>o</sup><sub>298</sub> < 0",
								"∆S<sup>o</sup><sub>298</sub> < 2"
							],
		"correct"		:	[1],
		"explanation"	:	"<br/>∆G<sup>o</sup> = ∆H<sup>o</sup> - T∆S<sup>o</sup> <br/><div class=\"frac\"> <span>∆G<sup>o</sup></span> <span class=\"symbol\">/</span> <span class=\"bottom\">T</span></div> =<div class=\"frac\"> <span>∆H<sup>o</sup></span> <span class=\"symbol\">/</span> <span class=\"bottom\">T</span></div> - ∆S<sup>o</sup> <br/><div class=\"frac\"> <span>∆G<sup>o</sup></span> <span class=\"symbol\">/</span> <span class=\"bottom\">298 K</span></div> =<div class=\"frac\"> <span>∆H<sup>o</sup></span> <span class=\"symbol\">/</span> <span class=\"bottom\">298 K</span></div> - ∆S<sup>o</sup> <br/><div class=\"frac\"> <span>∆G<sup>o</sup></span> <span class=\"symbol\">/</span> <span class=\"bottom\">298 K</span></div> =<div class=\"frac\"> <span>596 kJ/mol</span> <span class=\"symbol\">/</span> <span class=\"bottom\">298 K</span></div> - 2.1kJ/molK <br/><div class=\"frac\"> <span>∆G<sup>o</sup></span> <span class=\"symbol\">/</span> <span class=\"bottom\">298 K</span></div> = 2kJ/molK - 2.1kJ/molK <br/><div class=\"frac\"> <span>∆G<sup>o</sup></span> <span class=\"symbol\">/</span> <span class=\"bottom\">298 K</span></div> = -0.1 kJ/molK <br></br> From the above, you can see that only values for ∆S<sup>o</sup><sub>298</sub> that are greater than 2 (or ∆H<sup>o</sup><sub>298</sub>/298 K) will yield a negative value for ∆G<sup>o</sup><sub>298</sub>. Thermodynamically favored processes always have a negative value for ∆G<sup>o</sup>. <br/><br/> See Thermodynamics III for more information.",
	},
	{
		"question"		:	"Which of the following chemical reactions is NOT thermodynamically favored at any temperature?",
		"image"			:	"",
		"choices"		:	[
								"∆<i>H</i> = - 254 kJ/mol and ∆<i>S</i> = - 176 J/molK",
								"∆<i>H</i> = + 156 kJ/mol and ∆<i>S</i> = + 194 J/molK",
								"∆<i>H</i> = - 365 kJ/mol and ∆<i>S</i> = + 148 J/molK",
								"∆<i>H</i> = + 446 kJ/mol and ∆<i>S</i> = - 12.9 J/molK"
							],
		"correct"		:	[3],
		"explanation"	:	"<center>∆G<sup>o</sup> = ∆H<sup>o</sup> -T∆S<sup>o</sup></center> <br/><br/> Answer (A) is not correct. If ∆H and ∆S are both negative, the process can be thermodynamically favored at lower temperatures. As you can see from the equation, lowering the temperature reduces the worth of the unfavorably negative ∆S value. In a sense, the lower temperature reduces the worth of ∆S until the favorably negative ∆H overtakes it. <br/><br/> Answer (B) is not correct. If ∆H and ∆S are both positive, the process can be thermodynamically favored at high temperatures. As you can see from the equation, raising the temperature increases the worth of the favorably positive ∆S value. The higher temperature increases the worth of ∆S until the unfavorably positive ∆H is overtaken. <br/><br/> Answer (C) is not correct. A negative ∆H and a positive ∆S are both favorable and will produce a thermodynamically favored system at any temperature. <br/><br/> Answer (D) is correct. An endothermic reaction that reduces disorder will never be thermodynamically favored at any temperature, as neither condition is favorable. Neither will contribute to the expansion of the entropy of the universe. <br/><br/> See Thermodynamics III for more information.",
	},
	{
		"question"		:	"It is found that a certain process is thermodynamically favored at a temperature of 350 K, but is NOT thermodynamically favored at 150 K. Which of the following statements is true when the system is at 150 K?",
		"image"			:	"",
		"choices"		:	[
								"ΔS and ΔH are positive, and ΔG is negative.",
								"ΔS, ΔH, and ΔG are negative.",
								"ΔS and ΔH are negative, and ΔG is positive.",
								"ΔS, ΔH, and ΔG are positive."
							],
		"correct"		:	[3],
		"explanation"	:	"At 150 K, ΔG is positive because the process is not thermodynamically favored at that temperature. Since the reaction becomes thermodynamically favored at higher temperatures, ΔS and ΔH must be positive. <br/><br/><center>ΔG = ΔH – TΔS</center> <br/><br/> The significance of the favourably positive ∆S value increases as the temperature increases, because you are multiplying it by a larger number. Thus, at high temperatures, the absolute value of -T∆S will exceed that of the unfavorably positive ∆H, making ∆G negative. The significance of the favourably positive ∆S decreases as the temperature decreases, because you are multiplying it by a smaller number. Thus, at low temperatures, the positive ∆H will make ∆G positive. From this, we know that the process becomes thermodynamically favored at high temperatures when ΔS and ΔH are positive. <br/><br/> See Thermodynamics III for more information.",
	},
	{
		"question"		:	"<center> 2 CH<sub>3</sub>OH<i>(g)</i> <b>-></b> 2 C<i>(s)</i> + 4 H<sub>2</sub><i>(g)</i> + O<sub>2</sub><i>(g)</i> &nbsp;&nbsp; ΔH<sup>o</sup><sub>298</sub> = 402 kJ/mol</center> <br/><br/> Use the information above to find the standard enthalpy of formation for CH<sub>3</sub>OH at 298 K.",
		"image"			:	"",
		"choices"		:	[
								"402 kJ/mol",
								"-402 kJ/mol",
								"201 kJ/mol",
								"-201 kJ/mol"
							],
		"correct"		:	[3],
		"explanation"	:	"Heat of formation values are defined as the heat lost or gained when one mole of a compound is assembled from the most common forms of its elements under standard conditions, so we must flip the chemical equation and divide all of the coefficients by two in order to produce one mole of methanol. <br/> <br/><center> C<i>(s)</i> + 2 H<sub>2</sub><i>(g)</i> + ½ O<sub>2</sub><i>(g)</i> <b>-></b> CH<sub>3</sub>OH<i>(g)</i></center> <br/><br/> Oxygen is a diatomic gas under standard conditions. Since we only need one oxygen atom in methanol, we must put ½ in front of O<sub>2</sub><i>(g)</i>. You cannot use O<i>(g)</i>, as this is not the most common form of oxygen at 1 atm and 25<sup>o</sup>C. <br/><br/> When we flip the equation the sign for ΔH must change. If half as many moles react, half as much heat will be released. <br/><br/> See Thermodynamics II for more information.",
	}
];
