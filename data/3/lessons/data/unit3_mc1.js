/** Simple JavaScript Quiz
 * version 0.1.0
 * http://journalism.berkeley.edu
 * created by: Jeremy Rue @jrue
 *
 * Copyright (c) 2013 The Regents of the University of California
 * Released under the GPL Version 2 license
 * http://www.opensource.org/licenses/gpl-2.0.php
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 */

var quiztitle = "Unit 3 MC 1";

/**
* Set the information about your questions here. The correct answer string needs to match
* the correct choice exactly, as it does string matching. (case sensitive)
*
*/
var quiz = [
    {
        "question"      :   "<center>Cu<sub><i>(s)</i></sub> + 4 HNO<sub>3<i>(aq)</i></sub> -> Cu(NO<sub>3</sub>)<sub>2<i>(aq)</i></sub> + 2 NO<sub>2<i>(g)</i></sub> + 2 H<sub>2</sub>O(<sub><i>l</i></sub>)</center><br/>Which statement identifies the element that is reduced in the above chemical reaction with a correctjustification?",
        "image"         :   "",
        "choices"       :   [
                                "Cu is reduced, as each atom loses two electrons.",
                                "O is reduced, as each atom gains two electrons.",
                                "N is reduced, as two of the four nitrogen atoms from the 4 HNO<sub>3</sub> in the balanced equation gain one electron.",
                                "H is reduced, as each atom loses one electron."
                            ],
        "correct"       :   [2],
        "explanation"   :   "<br/>HNO<sub>3</sub> is the compound that is reduced. The oxidation number of nitrogen is reduced from +5 in HNO<sub>3</sub> to +4 in NO<sub>2</sub>. This shows that each of the nitrogen atoms in HNO<sub>3</sub> gained one electron when they bonding with oxygen to form NO<sub>2</sub>.<br/>See Aqueous Solutions and Chemical Reactions I for more information."
    },
    {
        "question"      :   "A 0.25 g sample of Na(<i>s</i>) is placed in a metal container filled with excess water and a 0.25 g sample of K(<i>s</i>) is placed in a separate metal container filled with excess water. Which of the following statements about the production of H2(<i>g</i>) is true?",
        "image"         :   "",
        "choices"       :   [
                                "The 0.25 g sample of K(<i>s</i>) will produce more moles of H<sub>2</sub>(<i>g</i>) in the reaction, as its reaction with water occurs at a faster rate due to its larger radius.",
                                "The 0.25 g sample of Na(<i>s</i>) will produce more moles of H<sub>2</sub>(<i>g</i>) in the reaction, as it has a smaller molar mass.",
                                "Both metals will produce the same number of moles of H<sub>2</sub>(g).",
                                "H<sub>2</sub>(g) will not be produced by either metal."
                            ],
        "correct"       :   [1],
        "explanation"   :   "<br/>The reactions that take place are below.<br/><center>2 Na<sub>(s)</sub> + 2 H<sub>2</sub>O<sub>(l)</sub> -> 2 Na<sup>+</sup><sub>(aq)</sub> + 2 OH<sup>-</sup><sub>(aq)</sub> + H<sub>2(g)</sub></center><br/><center>2 K<sub>(s)</sub> + 2 H<sub>2</sub>O<sub>(l)</sub> -> 2 K<sup>+</sup><sub>(aq)</sub> + 2 OH<sup>-</sup><sub>(aq)</sub> + H<sub>2(g)</sub></center><br/>Sodium metal will produce more moles of hydrogen gas, as it has a smaller molar mass. Because it has a smaller molar mass, more moles of sodium are available to react when equal masses of both Group IA metals are available.<br/>0.25 g Na × <div class=\"frac\"> <span>1 mol Na</span> <span class=\"symbol\">/</span> <span class=\"bottom\">22.99 g Na</span></div>=0.011 mol Na<br/><br/>0.25 g K × <div class=\"frac\"> <span>1 mol K</span> <span class=\"symbol\">/</span> <span class=\"bottom\">39.10 g K</span></div>=0.0064 mol K<br/><br/>See Aqueous Solutions and Chemical Reaction II for more information."
    },
    {
        "question"      :   "<center>MgO<sub><i>(s)<i></sub> + H<sub><i>2(g)</sub></i> -> H<sub>2</sub>O<sub><i>(l)</i></sub> + Mg<sub><i>(s)</i></sub></center><br/>Which of the following statements correctly identifies the elements that are being oxidized and reducedin the above chemical equation?",
        "image"         :   "",
        "choices"       :   [
                                "Mg is oxidized and H is reduced.",
                                "H is oxidized and Mg is reduced.",
                                "O is oxidized and Mg is reduced.",
                                "H is oxidized and O is reduced."
                            ],
        "correct"       :   [1],
        "explanation"   :   "<br/>The hydrogen atoms in H<sub>2</sub> are oxidized, as its oxidation number increases from 0 to +1. Each hydrogen atom lost one electron. The magnesium atom is MgO was reduced, as its oxidation number was reduced from +2 to 0. The magnesium atom gained two electrons.<br/>See Aqueous Solutions and Chemical Reaction I for more information."
    },
    {
        "question"      :   "<table> <tr> <td>Mass of CaA<sub>2</sub></td><td>2.00 g</td></tr><tr> <td>Mass of filter paper</td><td>0.75 g</td></tr><tr> <td>Mass of filter paper and CaCO<sub>3</sub> after first drying</td><td>1.78 g</td></tr><tr> <td>Mass of filter paper and CaCO<sub>3</sub> after second drying</td><td>1.75 g</td></tr><tr> <td>Mass of filter paper and CaCO<sub>3</sub> after third drying</td><td>1.75 g</td></tr></table><br/>An unknown nonmetal anion, A<sup>-</sup>, carries a -1 charge. In a gravimetric analysis, a 2.00 g sample of CaA<sub>2(<i>s</i>)</sub> was dissolved in water. Excess Na<sub>2</sub>CO<sub>3</sub> was added to the solution, which caused CaCO<sub>3</sub> to precipitate. The molar mass of CaCO<sub>3</sub> is 100.1 g/mol. The solution was filtered and the filter paper and precipitate were dried three times. Use the data above to identify the unknown anion, A<sup>-</sup>.",
        "image"         :   "",
        "choices"       :   [
                                "F<sup>-</sup>",
                                "Cl<sup>-</sup>",
                                "Br<sup>-</sup>",
                                "I<sup>-</sup>"
                            ],
        "correct"       :   [2],
        "explanation"   :   "<br/><br/>Find mass of CaCO<sub>3</sub><br/><br/>1.75 g − 0.75 g=1.00 g CaCO<sub>3</sub><br/><br/>Find moles of Ca<sup>2+</sup><br/><br/>1.00 g CaCO<sub>3</sub>x<div class=\"frac\"> <span>1 mol CaCO<sub>3</sub></span> <span class=\"symbol\">/</span> <span class=\"bottom\">100.1 g CaCO<sub>3</sub></span></div>x<div class=\"frac\"> <span>1 mol Ca<sup>2+</sup></span> <span class=\"symbol\">/</span> <span class=\"bottom\">100.1 mol CaCO<sub>3</sub></span></div>=0.0100 mol Ca<sup>2+</sup><br/><br/>0.0100 mol Ca<sup>2+</sup>×<div class=\"frac\"> <span>40.08 g Ca<sup>2+</sup></span> <span class=\"symbol\">/</span> <span class=\"bottom\">1 mol Ca<sup>2+</sup></span></div>=0.401 g Ca<sup>2+</sup>=0.40 g Ca<sup>2+</sup><br/><br/>Find mass of A<sup>−</sup><br/><br/>2.00 g - 0.40 g=1.60 g A<sup>−</sup><br/><br/>Find molar mass of A<sup>−</sup><br/><br/><i>MM</i>=<div class=\"frac\"> <span>mass A<sup>-</sup></span> <span class=\"symbol\">/</span> <span class=\"bottom\">2 x <i>n</i><sub>Ca<sup>2+</sup></sub></span></div>=<div class=\"frac\"> <span>1.6 g A<sup>-</sup></span> <span class=\"symbol\">/</span> <span class=\"bottom\">2(0.0100 mol)</span></div>=80.0 g/mol<br/><br/>This is closest to the molar mass of Br, which is 79.9 g/mol."
    },
    {
        "question"      :   "<center>Cr<sub>2</sub>O<sub>7</sub><sup>2-</sup><sub>(aq)</sub> + 6 Fe<sup>2+</sup><sub>(aq)</sub> + 14 H<sup>+</sup><sub>(aq)</sub> -> 2 Cr<sup>3+</sup><sub>(aq)</sub> + 7 H<sub>2</sub>O<sub>(l)</sub> + 6 Fe<sup>3+</sup><sub>(aq)</sub></center><br/><br/>In the chemical reaction above, the number of moles of Cr<sub>2</sub>O<sub>7</sub><sup>2-</sup> that are consumed is equal to...",
        "image"         :   "",
        "choices"       :   [
                                "six times the number of moles of Fe<sup>2+</sup> that are consumed.",
                                "half of the number moles of Cr<sup>3+</sup> that are produced.",
                                "seven times the number of moles of H<sub>2</sub>O that are produced.",
                                "one-seventh of the number of moles of H<sup>+</sup> that are consumed."
                            ],
        "correct"       :   [1],
        "explanation"   :   "<br/><br/>Each mole of Cr<sub>2</sub>O<sub>7</sub><sup>2-</sup> will produce two moles of Cr<sup>3+</sup> according to the balanced chemical equation, so half of the number of moles of Cr<sup>3+</sup> that are produced would equal the number of moles of Cr<sub>2</sub>O<sub>7</sub><sup>2-</sup> that are consumed."
    },
    {
        "question"      :   "Combustion of which of the following hydrocarbons will produce two moles of CO<sub>2</sub> for every one mole of H<sub>2</sub>O?",
        "image"         :   "",
        "choices"       :   [
                                "C<sub>2</sub>H<sub>2</sub>",
                                "C<sub>2</sub>H<sub>4</sub>",
                                "C<sub>4</sub>H<sub>8</sub>",
                                "C<sub>6</sub>H<sub>14</sub>"
                            ],
        "correct"       :   [0],
        "explanation"   :   "<br/><center>C<sub>2</sub>H<sub>2</sub> + 1.5 O<sub>2</sub> -> 2 CO<sub>2</sub> + H<sub>2></sub>O<br/><br/>or<br/><br/>2 C<sub>2</sub>H<sub>2</sub> + 3 O<sub>2</sub> -> 4 CO<sub>2</sub> + 2 H<sub>2</sub>O</center><br/>You can figure this out without writing the balanced chemical equation. Every carbon atom will bond with oxygen to make one CO<sub>2</sub> and every two hydrogen atoms will bond with oxygen to form one H<sub>2</sub>O. If the question is asking for a 2 CO<sub>2</sub> to 1 H<sub>2</sub>O ratio, there must be an equal number of carbon atoms and hydrogen atoms in the chemical formula for the hydrocarbon. (A) is the only hydrocarbon that has that ratio."
    },
    {
        "question"      :   "A chemist pours 100.0 mL of 1.0 <i>M</i> Pb(NO<sub>3</sub>)<sub>2</sub> into 100.0 mL of 1.0 <i>M</i> NaI and a yellow precipitate forms. Which of the following provides the concentrations of all species in solution when the reaction has gone to completion?",
        "image"         :   "",
        "choices"       :   [
                                "1.0 <i>M</i> NO<sub>3</sub><sup>-</sup> and 1.0 <i>M</i> Na<sup>+</sup>",
                                "0.25 <i>M</i> Pb<sup>2+</sup>, 0.5 <i>M</i> Na<sup>+</sup>, and 1.0 <i>M</i> NO<sub>3</sub><sup>-</sup>",
                                "0.5 <i>M</i> Pb<sup>2+</sup>, 1.0 <i>M</i> Na<sup>+</sup>, and 2.0 <i>M</i> NO<sub>3</sub><sup>-</sup>",
                                "0.5 <i>M</i> Pb<sup>2+</sup>, 1.0 <i>M</i> NO<sub>3</sub><sup>-</sup>, 0.5 <i>M</i> Na<sup>+</sup>, 0.5 <i>M</i> I<sup>-</sup>"
                            ],
        "correct"       :   [1],
        "explanation"   :   "<br/>When the solutions were combined, the total volume doubled so the initial concentrations are divided by two (the same number of moles of each ion divided by twice as many liters). The precipitate that forms must be PbI<sub>2</sub>, as Na<sup>+</sup> and NO<sub>3</sub><sup>-</sup> are soluble with everything so they must be spectator ions.<table> <tr> <th bgcolor=\"#000000\"><font color=\"#fff\">Species</font></th> <th bgcolor=\"#000000\"><font color=\"#fff\">Initial Concentrations when volume doubled</font></th> <th bgcolor=\"#000000\"><font color=\"#fff\">Concentrations after the precipitation of PbI<sub>2</sub>(s)</font></th> </tr><tr> <td><strong>Pb<sup>2+</sup></strong></td><td style=\"text-align:left;vertical-align:top;\">0.5 <i>M</i><br/>The volume doubled so the concentration was halved.</td><td style=\"text-align:left;vertical-align:top;\">0.25 <i>M</i><br/>1 Pb<sup>2+</sup> reacts with 2 I<sup>-</sup> to form PbI<sub>2</sub>. All of the I<sup>-</sup> will react but only half of the Pb<sup>2+</sup> will react.</td></tr><tr> <td><strong>NO<sub>3</sub><sup>-</sup></strong></td><td style=\"text-align:left;vertical-align:top;\">1.0 <i>M</i><br/>There are two moles of NO<sub>3</sub><sup>-</sup> for every one mole of Pb<sup>2+</sup></td><td style=\"text-align:left;vertical-align:top;\">1.0 <i>M</i><br/>NO<sub>3</sub><sup>-</sup> is a spectator ion so its concentration does not change.</td></tr><tr> <td><strong>Na<sup>+</sup></strong></td><td style=\"text-align:left;vertical-align:top;\">0.5 <i>M</i><br/>The volume doubled so the concentration was halved.</td><td style=\"text-align:left;vertical-align:top;\">0.5 <i>M</i><br/>Na<sup>+</sup> is a spectator ion so its concentration does not change.</td></tr><tr> <td><strong>I<sup>-</sup></strong></td><td style=\"text-align:left;vertical-align:top;\">0.5 <i>M</i><br/>The volume doubled so the concentration was halved.</td><td style=\"text-align:left;vertical-align:top;\">0 <i>M</i><br/>This was the limiting reactant in the precipitation reaction.</td></tr></table>"
    },
    {
        "question"      :   "The same element is oxidized and reduced in which of the following reactions?",
        "image"         :   "",
        "choices"       :   [
                                "2 Mg<sub>(<i>s</i>)</sub> + TiCl<sub>4(<i>g</sub>)</i> -> 2 MgCl<sub>2(<i>s</i>)</sub> + Ti<sub>(<i>s</i>)</sub>",
                                "Cu<sub>(<i>s</i>)</sub> + 4 HNO<sub>3(<i>aq</i>)</sub> -> Cu(NO<sub>3</sub>)<sub>(<i>aq</i>)</sub> + 2 NO<sub>2(<i>g</i>)</sub> + 2 H<sub>2</sub>O<sub>(<i>l</i>)</sub>",
                                "2 H<sub>2</sub>O<sub>2(<i>aq</i>)</sub> -> 2 H<sub>2</sub>O<sub>(<i>l</i>)</sub> + O<sub>2(<i>g</i>)</sub>",
                                "Ca<sub>(<i>s</i>)</sub> + Cl<sub>2(<i>g</i>)</sub> -> CaCl<sub>2(<i>s</i>)</sub>"
                            ],
        "correct"       :   [2],
        "explanation"   :   "In H<sub>2</sub>O<sub>2</sub> the oxidation number assigned to oxygen is -1. Oxygen is reduced when H<sub>2</sub>O<sub>2</sub> decomposes to form water, as its oxidation number assigned to oxygen in H<sub>2</sub>O is -2. Oxygen is oxidized when it bonds with itself to form oxygen gas, as its oxidation number on oxygen in O<sub>2</sub> is 0.<br/>See Aqueous Solutions and Chemical Reactions I for more information."
    },
    {
        "question"      :   "Which of the following solutions is the best conductor of electricity?",
        "image"         :   "",
        "choices"       :   [
                                "1.0 <i>M</i> NaCl",
                                "1.0 <i>M</i> Ca(NO<sub>3</sub>)<sub>2</sub>",
                                "1.0 <i>M</i> Al(NO<sub>3</sub>)<sub>3</sub>",
                                "2.0 <i>M</i> NCl<sub>3</sub>"
                            ],
        "correct"       :   [2],
        "explanation"   :   "Conductivity increases as the concentration of ions increases. 1.0 <i>M</i> NaCl is 2.0 <i>M</i> ions, as it dissociates to become 1.0 <i>M</i> Na<sup>+</sup> and 1.0 <i>M</i> Cl<sup>-</sup>. 1.0 <i>M</i> Ca(NO<sub>3</sub>)<sub>2</sub> is 3.0 <i>M</i> ions, as it dissociates to become 1.0 <i>M</i> Ca<sup>2+</sup> and 2.0 <i>M</i> NO<sub>3</sub><sup>-</sup>. 1.0 <i>M</i> Al(NO<sub>3</sub>)<sub>3</sub> is 4.0 <i>M</i> ions. NCl<sub>3</sub> is a molecule so it does not dissociate into ions. Thus, 1.0 <i>M</i> Al(NO<sub>3</sub>)<sub>3</sub> is the best conductor of electricity because it has the highest concentration of ions. See Aqueous Solutions and Chemical Reactions I for more information.<br/>See Atomic Theory III for more information."
    },

    {
        "question"      :   "<center>4 K<sub>(s)</sub> + O<sub>2(g)</sub> -> 2 K<sub>2</sub>O<sub>(s)</sub></center>How many moles of electrons are transferred in the above chemical equation?",
        "image"         :   "",
        "choices"       :   [
                                "2 e<sup>-</sup>",
                                "3 e<sup>-</sup>",
                                "4 e<sup>-</sup>",
                                "6 e<sup>-</sup>"
                            ],
        "correct"       :   [2],
        "explanation"   :   "The oxidation number on potassium is 0 in K<sub>(s)</sub> and +1 in K<sub>2</sub>O. There are 4 potassium atoms on each side of the chemical equation, and they each lost 1 electron, so a total of 4 electrons were lost by potassium in the balanced chemical equation. The oxidation number on oxygen in O<sub>2</sub> is 0 and the oxidation number of oxygen in K<sub>2</sub>O is -2. There are 2 oxygen atoms on each side of the chemical equation, and they each gained 2 electrons, so a total of 4 electrons were gained by oxygen in the balanced chemical equation.<br/>See Aqueous Solutions and Chemical Reactions I for more information."
    }
];
