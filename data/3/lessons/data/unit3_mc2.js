/** Simple JavaScript Quiz
 * version 0.1.0
 * http://journalism.berkeley.edu
 * created by: Jeremy Rue @jrue
 *
 * Copyright (c) 2013 The Regents of the University of California
 * Released under the GPL Version 2 license
 * http://www.opensource.org/licenses/gpl-2.0.php
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 */

var quiztitle = "Unit 3 MC 2";

/**
* Set the information about your questions here. The correct answer string needs to match
* the correct choice exactly, as it does string matching. (case sensitive)
*
*/
var quiz = [
    {
        "question"      :   "<center>Ca<sub>(<i>s</i>)</sub> + Cl<sub>2(<i>s</i>)</sub> -> CaCl<sub>2(<i>s</i>)</sub></center><br/><br/>A 2.0 g sample of Ca<sub>(<i>s</i>)</sub> and 3.2 g sample of Cl<sub>2(<i>g</i>)</sub> are placed in a sealed vessel and the above reaction goes to completion. Which of the following statements correctly compares the mass of the system before and after the reaction?",
        "image"         :   "",
        "choices"       :   [
                                "The mass of the system after the reaction will be greater than that before the reaction because the product is in the solid phase.",
                                "The mass of the system after the reaction will be less than that before the reaction because the reduction of gas phase molecules reduced the pressure in the system.",
                                "The mass of the system after the reaction will be the same as that before the reaction because atoms were conserved",
                                "The mass of the system after the reaction will be greater than that before the reaction because the CaCl<sub>2</sub> will absorb H<sub>2</sub>O to form CaCl<sub>2</sub>ˑ2H<sub>2</sub>O"
                            ],
        "correct"       :   [2],
        "explanation"   :   "Atoms are always conserved in chemical reactions. There is one calcium atom and two chloride atoms on each side of the balanced chemical equation, so you know that atoms were conserved in the chemical reaction. The nuclei of atoms account for the mass of the system. Since the number of each type of atom does not change, the total mass of the system does not change. Water cannot be absorbed, as the vessel is sealed."
    },
    {
        "question"      :   "What is the correct balanced net-ionic equation for the reaction between Na(<i>s</i>) and H<sub>2</sub>O?",
        "image"         :   "",
        "choices"       :   [
                                "2 Na(<i>s</i>) + 2 H<sub>2</sub>O(<i>l</i>) -> 2 Na<sup>+</sup>(<i>aq</i>) + 2 OH<sup>-</sup>(<i>aq</i>) + H<sub>2</sub>(<i>g</i>)",
                                "2 Na(<i>s</i>) + 2 H<sub>2</sub>O(<i>l</i>) -> 2 NaOH(<i>aq</i>) + H<sub>2</sub>(<i>g</i>)",
                                "Na(<i>s</i>) + H<sub>2</sub>O(<i>l</i>) -> Na<sup>+</sup>(<i>aq</i>) + OH<sup>-</sup>(<i>aq</i>) + H<sup>+</sup>(<i>aq</i>)",
                                "Na(<i>s</i>) + H<sub>2</sub>O(<i>l</i>) -> NaOH(<i>s</i>) + H+(<i>aq</i>)"
                            ],
        "correct"       :   [0],
        "explanation"   :   "<br/>All Group 1A metals and Ca, Sr, and, Ba from Group 2A will displace H<sub>2</sub>(<i>g</i>) from water when in their elemental form. Answer (B) gives NaOH(<i>aq</i>) as a product. This is not correct, as Na<sup>+</sup>(<i>aq</i>) and OH<sup>-</sup>(<i>aq</i>) float around independently of one another and this must be shown in the net ionic equation. Answer (C) cannot be correct, as the charges do not balance. Both sides of a chemical equation must balance for atoms and charge. The total charge on the reactants side is zero and the total charge on the products side is +1 for answer (C). Answer (D) cannot be correct as Group 1A metal cations are soluble with everything, so they never form solids in water. In other words, NaOH will not form a solid in water.<br/>See Aqueous Solutions and Chemical Reactions I and II for more information."
    },{
        "question"      :   "5.0 g samples of each of the compounds listed below are burned in the presence of excess oxygen gas. Which sample would produce the greatest mass of H<sub>2</sub>O?",
        "image"         :   "",
        "choices"       :   [
                                "CH<sub>4</sub>",
                                "C<sub>3</sub>H<sub>6</sub>",
                                "C<sub>2</sub>H<sub>2</sub>",
                                "C<sub>6</sub>H<sub>12</sub>O<sub>6</sub>"
                            ],
        "correct"       :   [0],
        "explanation"   :   "<br/>The products of the combustion of any hydrocarbon or compound containing H, C, and O are always CO<sub>2</sub> and H<sub>2</sub>O. To answer this question, determine which compound from the answer choices has the highest mass percent of hydrogen. You have equal masses of each compound, so the compound with the greatest mass of hydrogen will produce the greatest mass of H<sub>2</sub>O. To find the mass percent of H, divide the mass of H in the compound by the total mass of the compound and multiply by 100. As you do not have a calculator in this section, you can also make a ratio of mass of hydrogen to total mass of the compound for each choice and then compare them. Use 1 atm for H, 12 atm for C, and 16 amu for O to simplify the math. Sample (A) is about 25% H, as 4/16=0.25.<br/>See Aqueous Solutions and Chemical Reactions II and Quantitative Chemistry II for more information."
    },{
        "question"      :   "<center>Pb<sup>2+</sup><sub>(aq)</sub> + SO<sub>4</sub><sup>2-</sup><sub>(aq)</sub> -> PbSO<sub>4(<i>s</i>)</sub></center><br/>The above reaction occurs and the solution is filtered to collect the precipitate. After completing the experiment, the teacher tells the student that her experimentally determined concentration of Pb<sup>2+</sup> is higher than the actual concentration of Pb<sup>2+</sup>. Which statement provides a possible source of error that could have caused these results?",
        "image"         :   "",
        "choices"       :   [
                                "She did not rinse the precipitate in the filter paper with distilled water before drying and weighing.",
                                "She did not add enough Na<sub>2</sub>SO<sub>4</sub> to precipitate all of the Pb<sup>2+</sup>.",
                                "She did not rinse out the beaker containing the precipitate to wash any residual PbSO<sub>4</sub> into the filter paper.",
                                "The sample of Na<sub>2</sub>SO<sub>4</sub> that she added was not pure and contained a small amount of NaNO<sub>3</sub>."
                            ],
        "correct"       :   [0],
        "explanation"   :   "<br/>The precipitate in the filter paper must be rinsed with distilled water before drying and weighing. This is done in order to remove any excess unreacted SO<sub>4</sub><sup>2-</sup>, Na<sup>+</sup> or other ions out of the wet filter paper and precipitate and into the filtrate below. If the filter paper and precipitate are not rinsed before drying, the unreacted SO<sub>4</sub><sup>2-</sup> and spectator ions will remain in the filter paper and precipitate. This will cause the measured mass of precipitate to be higher than the actual mass of PbSO<sub>4</sub>. When using the mass of PbSO<sub>4</sub> to calculate the moles of Pb<sup>2+</sup>, she would obtain a value that is higher than the actual value. When dividing the calculated number of moles of Pb<sup>2+</sup> by the original volume of the solution, the calculated concentration of Pb<sup>2+</sup> would end up being higher than the actual concentration. The other three answer choices would all lead to a low value for the concentration of Pb<sup>2+</sup>."
    },{
        "question"      :   "<center>Cr<sub>2</sub>O<sub>7</sub><sup>2-</sup><sub>(aq)</sub> + 6 Fe<sup>2+</sup><sub>(aq)</sub> + 14 H<sup>+</sup><sub>(aq)</sub> -> 2 Cr<sup>3+</sup><sub>(aq)</sub> + 7 H<sub>2</sub>O<sub>(<i>l</i>)</sub> + 6 Fe<sup>3+</sup><sub>(<i>aq</i>)</sub></center><br/>Which statement identifies the element that is reduced in the above reaction and provides the correct change in that element’s oxidation number?",
        "image"         :   "",
        "choices"       :   [
                                "Fe is reduced, as its oxidation number changes from +2 to +3.",
                                "Cr is reduced, as its oxidation number changes from +6 to +3.",
                                "O is reduced, as its oxidation number changes from 0 to -2.",
                                "H is reduced, as its oxidation number changes from +1 to 0."
                            ],
        "correct"       :   [1],
        "explanation"   :   "<br/><br/>An element is reduced when its oxidation number is reduced, which means that it lost electrons. The oxidation number on chromium in Cr<sub>2</sub>O<sub>7</sub><sup>2-</sup> is +6 (-2(7) + 6(2)=-2) and the oxidation number on chromium in Cr<sup>3+</sup> is +3. The oxidation number on chromium was reduced by three, so each chromium atom lost three electrons in the reaction.<br/>See Aqueous Solutions and Chemical Reactions I for more information."
    },{
        "question"      :   "Which of the following is most easily oxidized?",
        "image"         :   "",
        "choices"       :   [
                                "Na<sup>+</sup><sub>(<i>aq</i>)</sub>",
                                "Na<sub>(<i>s</i>)</sub>",
                                "Cl<sup>-</sup><sub>(<i>aq</i>)</sub>",
                                "Cl<sub>(<i>g</i>)</sub>"
                            ],
        "correct"       :   [1],
        "explanation"   :   "For an element to be oxidized, it must lose electrons. When it does so, its oxidation number increasers. Group 1A elements only lose one electron in order to acquire a full octet. Na+ has already lost an electron and has a full octet. It will not lose another electron very easily. Cl<sup>-</sup> also has a full octet, and will not give up electrons very easily. Halogens tend to gain an electron to acquire a full octet. They do not tend to lose electrons, so they are not easily oxidized. Na<sub>(<i>s</i>)</sub> is very reactive and will lose its 3<sub>s</sub> electron very easily to acquire a full octet. Therefore, it is the most easily oxidized species in the list."
    },{
        "question"      :   "Which of the following compounds contains an element with an oxidation state of +2?",
        "image"         :   "",
        "choices"       :   [
                                "NH<sub>3</sub>",
                                "Na<sub>2</sub>O",
                                "H<sub>2</sub>O<sub>2</sub>",
                                "ZnS"
                            ],
        "correct"       :   [2],
        "explanation"   :   "As an ion, the oxidation number on zinc is always +2. If you forgot this, you can also figure out the oxidation state on zinc by looking at sulfur. Sulfur is in Group VIA and as an ion it exists as S<sup>2-</sup>. Since the overall compound is neutral, zinc must have an oxidation state of +2.<br/>See Aqueous Solutions and Chemical Reactions I and Nomenclature for more information."
    },{
        "question"      :   "<center>Cr<sub>2</sub>O<sub>7</sub><sup>2-</sup><sub>(aq)</sub> + 6 Cu<sup>+</sup><sub>(aq)</sub> + 14 H<sup>+</sup><sub>(aq)</sub> -> 2 Cr<sup>3+</sup><sub>(aq)</sub> + 7 H<sub>2</sub>O<sub>(l)</sub> + 6 Cu<sup>2+</sup><sub>(aq)</sub></center><br/>How many moles are Cr<sup>3+</sup> are produced when 25. mL of acidified 0.40 <i>M</i> Na<sub>2</sub>Cr<sub>2</sub>O<sub>7</sub> is added to 60. mL of 0.1 <i>M</i> CuCl?",
        "image"         :   "",
        "choices"       :   [
                                "O.010 mol",
                                "0.020 mol",
                                "0.0020 mol",
                                "0.0060 mol"
                            ],
        "correct"       :   [2],
        "explanation"   :   "<br/>0.025 L Cr<sub>2</sub>O<sub>7</sub><sup>2-</sup>x<div class=\"frac\"> <span>0.40 mol Cr<sub>2</sub>O<sub>7</sub><sup>2-</sup></span> <span class=\"symbol\">/</span> <span class=\"bottom\">1 L Cr<sub>2</sub>O<sub>7</sub><sup>2-</sup></span></div>x<div class=\"frac\"> <span>2 mol Cr<sup>3+</sup></span> <span class=\"symbol\">/</span> <span class=\"bottom\">1 mol Cr<sub>2</sub>O<sub>7</sub><sup>2-</sup></span></div>=0.020 mol Cr<sup>3+</sup><br/><br/>0.060 L CuClx<div class=\"frac\"> <span>0.10 mol CuCl</span> <span class=\"symbol\">/</span> <span class=\"bottom\">1 L CuCl</span></div>x<div class=\"frac\"> <span>2 mol Cr<sup>3+</sup></span> <span class=\"symbol\">/</span> <span class=\"bottom\">6 mol CuCl</span></div>=0.0020 mol Cr<sup>3+</sup><br/><br/>Cu<sup>+</sup> is the limiting reactant, as it will produce fewer moles of Cr<sup>3+</sup>."
    },{
        "question"      :   "<center>CO<sub>(g)</sub> + PbO<sub>(s)</sub> -> CO<sub>2(g)</sub> + Pb<sub>(s)</sub></center>How many moles of electrons are transferred in the above chemical equation?",
        "image"         :   "",
        "choices"       :   [
                                "2 e<sup>-</sup>",
                                "3 e<sup>-</sup>",
                                "4 e<sup>-</sup>",
                                "6 e<sup>-</sup>"
                            ],
        "correct"       :   [0],
        "explanation"   :   "<br/>The oxidation number on carbon is +2 in CO and +4 in CO<sub>2</sub>, so carbon lost 2 electrons in this reaction. The oxidation number on lead in PbO is +2 and the oxidation number of lead in Pb<sub>(s)</sub> is 0, so lead gained two electrons in this reaction.<br/>See Aqueous Solutions and Chemical Reactions I for more information."
    },{
        "question"      :   "A green precipitate forms when a solution of 1.0 <i>M</i> NaOH is mixed with which of the following solutions.",
        "image"         :   "",
        "choices"       :   [
                                "1.0 <i>M</i> Fe(NO<sub>3</sub>)<sub>2</sub>",
                                "1.0 <i>M</i> K(NO<sub>3</sub>)<sub>2</sub>",
                                "1.0 <i>M</i> NaNO<sub>3</sub>",
                                "1.0 <i>M</i> KI"
                            ],
        "correct"       :   [0],
        "explanation"   :   "<br/>To answer this question, you need to look for a cation and anion pair that are not soluble together using the solubility rules for AP Chemistry. The College Board says that AP Chemistry students need to know that Na<sup>+</sup>, K<sup>+</sup>, NH<sub>4</sub><sup>+</sup>, and NO3<sup>-</sup> are soluble with everything, so they never form precipitates. This means that your cation and anion combination cannot include any of those four ions. The question gives NaOH, and we know that Na<sup>+</sup> is soluble with everything, so we need to find a cation from the list that is not soluble with OH<sup>-</sup>. The only possible answer is 1.0 <i>M</i> Fe(NO<sub>3</sub>)<sub>2</sub>, because Fe<sup>2+</sup> is the only cation that is not soluble with everything.See Aqueous Solutions and Chemical Reactions I for more information."
    }
];
