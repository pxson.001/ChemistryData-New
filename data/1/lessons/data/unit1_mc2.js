/** Simple JavaScript Quiz
 * version 0.1.0
 * http://journalism.berkeley.edu
 * created by: Jeremy Rue @jrue
 *
 * Copyright (c) 2013 The Regents of the University of California
 * Released under the GPL Version 2 license
 * http://www.opensource.org/licenses/gpl-2.0.php
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 */

var quiztitle = "Unit 1 MC 2";

/**
* Set the information about your questions here. The correct answer string needs to match
* the correct choice exactly, as it does string matching. (case sensitive)
*
*/
var quiz = [
    {
        "question"      :   "Pure samples of lithium and oxygen were analyzed using photoelectron spectroscopy. Which of the following statements about the energy required to remove electrons from the 1s subshell of these two elements is correct?",
        "image"         :   "",
        "choices"       :   [
                                "It requires more energy to remove 1s electrons from Li, because it has a larger radius than O.",
                                "It requires more energy to remove 1s electrons from Li, because its nucleus contains fewer protons than O.",
                                "It requires more energy to remove 1s electron s from O, because its nucleus contains more protons than Li.",
                                "It requires more energy to remove 1s electrons from O, because its nucleus contains more neutrons than Li."
                            ],
        "correct"       :   [2],
        "explanation"   :   "<i>F</i>=<i>k</i><div class=\"frac\"> <span><i>Q</i><sub>1</sub><i>Q</i><sub>2</sub></span> <span class=\"symbol\">/</span> <span class=\"bottom\"><i>d</i><sup>2</sup></span></div><br/>According to Coulomb’s Law, forces of attraction increase as the charge increases. Since oxygen has more protons in its nucleus, the net positive charge that is pulling on the 1<i>s</i> electrons is greater, and thus, the force of attraction is greater. Because of the greater force of attraction, more energy is required to remove electrons from the 1<i>s</i> shell in oxygen.<br/>See Atomic Theory II for more information."
    },
    {
        "question"      :   "A sample of a compound containing only carbon, hydrogen, and oxygen was burned in excess oxygen gas. 88.0 g of CO<sub>2</sub> and 54.0 g of H<sub>2</sub>O were produced. What is a possible empirical formula for the compound?",
        "image"         :   "",
        "choices"       :   [
                                "C<sub>2</sub>H<sub>5</sub>O",
                                "C<sub>2</sub>H<sub>5</sub>OH",
                                "C<sub>3</sub>H<sub>7</sub>OH",
                                "C<sub>2</sub>H<sub>2</sub>O<sub>2</sub>"
                            ],
        "correct"       :   [1],
        "explanation"   :   "<strong>Step 1) Find moles of carbon and hydrogen in the compound</strong><br />Moles Carbon:<br/>88.0g CO<sub>2</sub>x<div class=\"frac\"> <span>1 mol CO<sub>2</sub></span> <span class=\"symbol\">/</span> <span class=\"bottom\">44.0g CO<sub>2</sub></span></div>x<div class=\"frac\"> <span>1 mol C</span> <span class=\"symbol\">/</span> <span class=\"bottom\">1 mol CO<sub>2</sub></span></div>=2.00 mol C<br/>Moles Hydrogen:<br/>54.0g H<sub>2</sub>Ox<div class=\"frac\"> <span>1 mol H<sub>2</sub>O</span> <span class=\"symbol\">/</span> <span class=\"bottom\">18.0g H<sub>2</sub>O</span></div>x<div class=\"frac\"> <span>2 mol H</span> <span class=\"symbol\">/</span> <span class=\"bottom\">1 mol H<sub>2</sub>O</span></div>=6.00 mol H<br/><strong>Step 2) Put all moles over the smallest number of moles to find the empirical formula</strong><br/><i>Carbon</i> :<div class=\"frac\"> <span>2.00 mol</span> <span class=\"symbol\">/</span> <span class=\"bottom\">2.00 mol</span></div>=1.00<br/><i>Hydrogen</i> :<div class=\"frac\"> <span>6.00 mol</span> <span class=\"symbol\">/</span> <span class=\"bottom\">2.00 mol</span></div>=3.00<br/>The question does not offer a solution with 1 carbon and 3 hydrogen atoms. If we multiply both by two we get C<sub>2</sub>H<sub>6</sub>. The question also tells us the original compound contains oxygen. In order to solve for oxygen, we would have needed the mass of the sample before it was burned. There is only one answer that is possible as only one answer has a 1 to 3 ratio of carbon to hydrogen. The empirical formula must be C<sub>2</sub>H<sub>5</sub>OH.<br/>See Quantitative Chemistry II for more information."
    },
    {
        "question"      :   "The mass percent of calcium in a pure sample of CaF<sub>2</sub> is 51.3 percent. A sample of CaF<sub>2</sub> is contaminated with a small amount of calcium chloride hexahydrate, CaCl<sub>2</sub>ˑ6H<sub>2</sub>O, and heated to eliminate all water. Which of the following statements is true?",
        "image"         :   "",
        "choices"       :   [
                                "The mass percent of calcium in the contaminated sample would be greater than 51.3 % before heating and less than 51.3% after heating.",
                                "The mass percent of calcium in the contaminated sample would be less than 51.3 % before heating and greater than 51.3% after heating.",
                                "The mass percent of calcium in the contaminated sample would be 51.3% before and after heating.",
                                "The mass percent of calcium in the contaminated sample would be less than 51.3 % before and after heating."
                            ],
        "correct"       :   [3],
        "explanation"   :   "The mass percent of calcium in a pure sample of CaCl<sub>2</sub> is less than 51.3 %, because the mass of chlorine is greater than the mass of fluorine. In fact, the mass percent of calcium in a pure sample of CaCl<sub>2</sub> is 36.1%. Consider a 200.0 g sample that contains 50% CaF<sub>2</sub> and 50% CaCl<sub>2</sub> by mass. In this sample, the total mass of calcium would be 51.3 g + 36.1 g=87.4 g, and the mass percent to calcium would be 43.7% (see calculation below), which is less than that of pure CaF<sub>2</sub>. Because the mass percent of calcium in CaCl<sub>2</sub> is less than 51.3 %, mixing a small amount of CaCl<sub>2</sub> into a pure sample of CaF<sub>2</sub> will always reduce the overall mass percent of calcium. The mass percent of calcium in CaCl<sub>2</sub>ˑ6H<sub>2</sub>O is much less than 51.3%, so the presence of that impurity in a sample of CaF<sub>2</sub> would also reduce the overall mass percent of calcium.<br/><div class=\"frac\"> <span>87.4 g</span> <span class=\"symbol\">/</span> <span class=\"bottom\">200.0 g</span></div>×100=43.7% 200.0 g</center>"
    },
    {
        "question"      :   "Equal masses of C<i>(s)</i> and CO<sub>2</sub><i>(g)</i> are put in a sealed vessel. They react according the chemical equation below. Identify the species that remain in the vessel after the reaction has gone to completion.<br/><center>C<i>(s)</i> + CO<sub>2</sub></sub><i>(g)</i> -> 2 CO<i>(g)</i></center>",
        "image"         :   "",
        "choices"       :   [
                                "Only CO",
                                "Only CO and CO<sub>2</sub>",
                                "Only C and CO",
                                "C, CO<sub>2</sub> and CO"
                            ],
        "correct"       :   [2],
        "explanation"   :   "<br/>CO<sub>2</sub> is the limiting reactant. The molar mass of CO<sub>2</sub> is greater than that of C. For this reason, the number of moles of C is greater than that of CO<sub>2</sub> when equal masses of each are placed in the vessel. Since the two reactants react in a 1 to 1 ratio (see the balanced equation) CO<sub>2</sub> must be the limiting reactant and C must be the excess reactant. When the reaction has gone to completion, all of the CO<sub>2</sub> has been consumed. The excess reactant, C, and the product, CO, are the only species that would remain in the vessel.<br/>See Quantitative Chemistry I for more information."
    },
    {
        "question"      :   "<table> <tr> <th>Element</th> <th>Atomic Radius</th> <th>Electronegativity</th> </tr><tr> <td>Sodium</td><td>190 pm</td><td>0.93</td></tr><tr> <td>Phosphorus</td><td>___ pm</td><td>___</td></tr></table><br/>Using what you know about periodic trends and the available data in the above table, identify the most likely values for the atomic radius and electronegativity of phosphorus.",
        "image"         :   "",
        "choices"       :   [
                                "Atomic Radius (98 pm), Electronegativity (0.84)",
                                "Atomic Radius (243 pm), Electronegativity (0.84)",
                                "Atomic Radius (98 pm), Electronegativity (2.19)",
                                "Atomic Radius (243 pm), Electronegativity (2.19)"
                            ],
        "correct"       :   [2],
        "explanation"   :   "<br/><i>F</i>=<i>k</i><div class=\"frac\"> <span><i>Q</i><sub>1</sub><i>Q</i><sub>2</sub></span> <span class=\"symbol\">/</span> <span class=\"bottom\"><i>d</i><sup>2</sup></span></div><br/>The valence electrons in Na and P are in the n=3 shell. According to Coulomb’s Law, forces or attraction increase as the amount of charge increases. Since P has more protons in its nucleus, the forces of attraction acting on the valence electrons will be greater, and thus, the radius will be smaller. Thus, the radius must be 98 pm. Atomic radii always decrease when moving from left to right along a period in the periodic table, since all of the outer electrons are in the same shell and more protons are being added to the nucleus. Electronegativity is defined as an elements ability to attract electrons toward itself in a chemical bond. Elements with smaller radii have higher electonegativity values. According to Coulomb’s Law, forces of attraction increase as distances decrease. Since P has a smaller radius, it will have a higher electronegativity value. Thus, the electronegativity value for P must be 2.19.<br/>See Atomic Theory III for more information."
    },
    {
        "question"      :   "If 1.0 mole of CO2 is produces in the reaction below, find the total number of moles of reactants that are consumed in the reaction below.<br/><center>C<sub>4</sub>H<sub>8</sub><i>(g)</i> + 6 O<sub>2</sub><i>(g)</i> -> 4 CO<sub>2</sub><i>(g)</i> + 4 H<sub>2</sub>O<i>(g)</i></center>",
        "image"         :   "",
        "choices"       :   [
                                "0.66 moles C<sub>4</sub>H<sub>8</sub> <i>(g)</i> and 2.31 moles O2<i>(g)</i>",
                                "0.33 moles C<sub>4</sub>H<sub>8</sub> <i>(g)</i> and 2.0 moles O2<i>(g)</i>",
                                "0.25 moles C<sub>4</sub>H<sub>8</sub> <i>(g)</i> and 1.5 moles O2<i>(g)</i>",
                                "1.0 moles C<sub>4</sub>H<sub>8</sub> <i>(g)</i> and 6.0 moles O2<i>(g)</i>"
                            ],
        "correct"       :   [2],
        "explanation"   :   "There are 4 moles of CO<sub>2</sub> in the balanced chemical equation, and 1.0 mole of CO<sub>2</sub> was produced. Since 4/4=1.0 mole of CO<sub>2</sub><i>(g)</i>, the stoichiometric coefficients for the reactants must be divided by 4 as well. For C<sub>4</sub>H<sub>8</sub><i>(g)</i>, 1/4=0.25 moles of C<sub>4</sub>H<sub>8</sub><i>(g)</i>. For O<sub>2</sub><i>(g)</i>, 6/4=1.5 moles of O<sub>2</sub><i>(g)</i>."
    },
    {
        "question"      :   "According to the reaction below, what mass of H<sub>2</sub>O<i>(g)</i> can be produces when 2.0 x 10<sup>23</sup> molecules of CH<sub>4</sub> react with excess O<sub>2</sub><i>(g)</i> and the reaction goes to completion.<center>CH<sub>4</sub><i>(g)</i> + 2 O<sub>2</sub><i>(g)</i> -> CO<sub>2</sub><i>(g)</i> + 2 H<sub>2</sub>O<i>(g)</i></center>",
        "image"         :   "",
        "choices"       :   [
                                "6.0 g H<sub>2</sub>O",
                                "12 g H<sub>2</sub>O",
                                "18 g H<sub>2</sub>O",
                                "32 g H<sub>2</sub>O"
                            ],
        "correct"       :   [1],
        "explanation"   :   "<br/>To answer this question, you must recognize that 2.0 x 10<sup>23</sup> is 1/3 of a mole (a mole is 6.022 x 10<sup>23</sup>). According to the balanced equation, 1/3 of a mole of CH<sub>4</sub> will produce 2/3 of a mole of water. The molar mass of water is 18 g/mol, and 18g/mol x 2/3 mol=12 g H<sub>2</sub>O.<br/>See Quantitative Chemistry I for more information."
    },
    {
        "question"      :   "<center><img src=\"data/images/1_mc2_8.png\" style=\"max-width: 100%; max-height: 100%\"/></center>Which peak is associated with the 1<i>s</i> sublevel of the element represented by the above photoelectric spectra?",
        "image"         :   "",
        "choices"       :   [
                                "A",
                                "B",
                                "C",
                                "D"
                            ],
        "correct"       :   [3],
        "explanation"   :   "<br/>When looking at PES data, observe the numbers on the x-axis. If you are given the complete photoelectric spectrum for an element, the largest number will be associated with the 1<i>s</i> sublevel. On past AP Chemistry exams, the largest number has tended to be located the furthest to the right; however, the x-axis is often reversed so that the largest number appears closest to the origin. In this case, the largest number (178 MJ/mol) was the furthest to the right. The largest number is always associated with the 1<i>s</i> sublevel, as those electrons are closest to the nucleus, and thus, require the greatest amount of energy to be removed.<br/>See Atomic Theory II for more information."
    },
    {
        "question"      :   "Which of the following lists Ca, Zn, and Br in order of decresing atomic radii?",
        "image"         :   "",
        "choices"       :   [
                                "Ca > Zn > Br",
                                "Br > Zn > Ca",
                                "Ca > Br > Zn",
                                "Zn > Br > Ca"
                            ],
        "correct"       :   [0],
        "explanation"   :   "<br/>Atomic radii always decrease when moving from left to right along a period in the periodic table, since all of the outer electrons are in the same shell and more protons are being added to the nucleus. According to Coulomb’s Law, forces of attraction increases as the magnitude of a charge increases. The greater charge that is created as additional protons are added increases the force of attraction acting on the electrons in a given shell and pulls them closer to the nucleus.<br/>See Atomic Thoery III for more information."
    },
    {
        "question"      :   "<table> <tr> <th></th> <th>Ionization Energy (kJ/mol)</th> </tr><tr> <td>IE<sub>1</sub></td><td>590</td></tr><tr> <td>IE<sub>2</sub></td><td>1145</td></tr><tr> <td>IE<sub>3</sub></td><td>4912</td></tr><tr> <td>IE<sub>4</sub></td><td>6491</td></tr><tr> <td>IE<sub>5</sub></td><td>8153</td></tr></table><br/><br/>Use the data for successive ionization energies of element Y, given above, to determine the most likely empirical formula for an oxide of element Y.",
        "image"         :   "",
        "choices"       :   [
                                "YF",
                                "Y<sub>2</sub>F",
                                "YF<sub>2</sub>",
                                "Y<sub>2</sub>F<sub>3</sub>"
                            ],
        "correct"       :   [2],
        "explanation"   :   "When an element completely loses its outer shell of electrons, the electron configuration drops a quantum number, and the radius becomes significantly smaller. This causes a very large increase in the subsequent ionization energy, as a much smaller radius means that there are much stronger forces of attraction between the nucleus and the electrons, in accordance with Coulomb’s Law. In this question, the largest increase in ionization energy occurs between the second and third ionization. This tells you that the element lost all of the electrons in its outer shell and acquired the electron configuration of a noble gas after its second ionization. This can only mean that element Y is a Group II element. The chemical formula when fluoride bonds with a Group II element is YF<sub>2</sub>. The data for successive ionization energies in this question is for calcium, so it would have the formal CaF<sub>2</sub>.<br/>See Atomic Theory III, Nomenclature, and Quantitative Chemistry II for more information."
    }
];
