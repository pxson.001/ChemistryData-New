/** Simple JavaScript Quiz
 * version 0.1.0
 * http://journalism.berkeley.edu
 * created by: Jeremy Rue @jrue
 *
 * Copyright (c) 2013 The Regents of the University of California
 * Released under the GPL Version 2 license
 * http://www.opensource.org/licenses/gpl-2.0.php
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 */

var quiztitle = "Unit 1 MC 1";

/**
* Set the information about your questions here. The correct answer string needs to match
* the correct choice exactly, as it does string matching. (case sensitive)
*
*/
var quiz = [
    {
        "question"      :   "A pure sample of a compound composed of only carbon, hydrogen and oxygen contains 24.0 g of carbon, 4.0 g of hydrogen, and 8.0 g of oxygen. What is the empirical formula for this compound?",
        "image"         :   "",
        "choices"       :   [
                                "C<sub>2</sub>H<sub>5</sub>O",
                                "C<sub>6</sub>HO<sub>2</sub>",
                                "C<sub>4</sub>H<sub>8</sub>O",
                                "C<sub>24</sub>H<sub>4</sub>O<sub>8</sub>"
                            ],
        "correct"       :   [2],
        "explanation"   :   "Step 1) Find moles of C, H, and O</strong><br/><br/>24.0g C × <div class=\"frac\"> <span>1 mol C</span> <span class=\"symbol\">/</span> <span class=\"bottom\">12.0g C</span></div>=2.00 mol C<br/><br/>4.0g H × <div class=\"frac\"> <span>1 mol H</span> <span class=\"symbol\">/</span> <span class=\"bottom\">1.0g H</span></div>=4.0 mol H<br/><br/>8.0g O × <div class=\"frac\"> <span>1 mol O</span> <span class=\"symbol\">/</span> <span class=\"bottom\">16.0g O</span></div>=0.50 mol O<br/><br/><strong>Step 2) Put all three over the least number of moles</strong><br/><br/><i>Carbon</i> :<div class=\"frac\"> <span>2.00 mol</span> <span class=\"symbol\">/</span> <span class=\"bottom\">0.50 mol</span></div>=4.0<br/><br/><i>Hydrogen</i> :<div class=\"frac\"> <span>4.00 mol</span> <span class=\"symbol\">/</span> <span class=\"bottom\">0.50 mol</span></div>=8.0<br/><br/><i>Oxygen</i> :<div class=\"frac\"> <span>0.50 mol</span> <span class=\"symbol\">/</span> <span class=\"bottom\">0.50 mol</span></div>=1.0<br/><br/><strong>The empirical formula is C<sub>4</sub>H<sub>8</sub>O</strong><br/><br/>See Quantitative Chemistry II for more information."
    },
    {
        "question"      :   "<table> <tr> <th></th> <th>Ionization Energy (kJ/mol)</th> </tr><tr> <td>1<sup>st</sup></td><td>1,087</td></tr><tr> <td>2<sup>nd</sup></td><td>2,353</td></tr><tr> <td>3<sup>rd</sup></td><td>4,621</td></tr><tr> <td>4<sup>th</sup></td><td>6,223</td></tr><tr> <td>5<sup>th</sup></td><td>37,831</td></tr><tr> <td>6<sup>th</sup></td><td>47,277</td></tr></table><br /><p>The above table provides the first six ionization energies for an element in period 2. Which of the following identifies the correct element and provides the best justification?</p>",
        "image"         :   "",
        "choices"       :   [
                                "O, as it has six valence electrons.",
                                "O, as it has four electrons in its 2<i>p</i> sublevel.",
                                "C, as it has four valence electrons.",
                                "C, as it has six electrons in total."
                            ],
        "correct"       :   [2],
        "explanation"   :   "<i>F</i>=<i>k</i><div class=\"frac\"> <span><i>Q</i><sub>1</sub><i>Q</i><sub>2</sub></span> <span class=\"symbol\">/</span> <span class=\"bottom\"><i>d</i><sup>2</sup></span></div><br/><p>According to Coulomb’s Law, reducing the distance between the nucleus and electrons increases the force of attraction on the electrons. When the electron configuration drops a principal quantum number (loses a shell), the radius becomes significantly smaller. This accounts for the significantly larger increase in ionization potential that is observed, as a much smaller radius means that there is a much stronger force of attraction between the nucleus and the electrons. As carbon has 4 electrons in its n=2 shell (4 valence electrons) the most significant increase in ionization energy occurs when the fifth electron is removed. The fifth electron is removed from n=1, and the average distance between n=1 electrons and nucleus is much less than that of n=2 electrons.See Atomic Theory III for more information.</p>"
    },
    {
        "question"      :   "A 10.0 g sample of which of the following compounds would contain the greatest number of moles of oxygen?",
        "image"         :   "",
        "choices"       :   [
                                "Li<sub>2</sub>O",
                                "BeO",
                                "Na<sub>2</sub>O",
                                "MgO"
                            ],
        "correct"       :   [1],
        "explanation"   :   "All samples have the same mass. The sample that has the smallest overall mass of the metal cation will have the largest mass of oxygen. If the mass of oxygen is greater, the number of moles of oxygen will also be greater. The atomic mass of one beryllium atom is less than that of two lithium atoms, two sodium atoms, or one magnesium atom. For that reason, the sample of BeO will have the greatest number of moles of oxygen. You could also solve this by comparing the molar masses of each compound. The calculation below would be used to find the moles of oxygen in a 10.0 g sample of Li<sub>2</sub>O. If you were to calculate the number of moles of oxygen in a 10.0 g sample of the other compounds, the only number that would change is molar mass of that compound. As the molar mass is the only denominated number, an oxide with a larger molar mass will always contain fewer moles of oxygen – and vise versa.<br /><center> 10.0 g Li<sub>2</sub>O x<div class=\"frac\"> <span>1 mol Li<sub>2</sub>O</span> <span class=\"symbol\">/</span> <span class=\"bottom\">29.88 g Li<sub>2</sub>O</span></div> x<div class=\"frac\"> <span>1 mol O</span> <span class=\"symbol\">/</span> <span class=\"bottom\">1 mol Li<sub>2</sub>O</span></div></center>"
    },
    {
        "question"      :   "The mass percent of sodium in a pure sample of NaCl is 39.34%. A sample of NaCl containing some impurities was analyzed and it was determined that the mass percent of sodium was 42.76%. Which of the following impurities could account for the increase?",
        "image"         :   "",
        "choices"       :   [
                                "NaF",
                                "NaBr",
                                "NaI",
                                "LiCl"
                            ],
        "correct"       :   [0],
        "explanation"   :   "LiCl would decrease the mass percent of sodium, as the number of moles of sodium in that sample would definitely be less than that in a pure sample of NaCl. The mass percent of sodium in pure samples of NaBr or NaI is less than 39.34%, because the masses of Br and I are greater than the mass of fluorine. In fact, the mass percent of sodium in a pure sample of NaBr is 22.3%. Consider a 200.0 g sample that contains 50% NaCl and 50% NaBr by mass. In this sample, the total mass of sodium would be 39.34 g + 22.3 g = 61.6 g, and the mass percent to sodium would be 30.8% (see calculation below), which is less than that of pure NaCl. Because the mass percent of sodium in NaBr and NaI are less than 39.34 %, mixing a small amount of NaBr or NaI into a pure sample of NaCl would always reduce the overall mass percent of sodium. However, since the mass percent of sodium in NaF is greater than 39.34%, mixing NaF with NaCl in any proportion will increase the mass percent of sodium in the sample.<br /><center><div class=\"frac\"> <span>61.6 g</span> <span class=\"symbol\">/</span> <span class=\"bottom\">200.0 g</span></div> x 100 = 30.8%</center>"
    },
    {
        "question"      :   "Of the following elements, which one has the highest first ionization energy?",
        "image"         :   "",
        "choices"       :   [
                                "K",
                                "Na",
                                "Mg",
                                "S"
                            ],
        "correct"       :   [3],
        "explanation"   :   "The first ionization energy is the amount of energy required to remove the least tightly held electron from an atom in the gas phase. Elements with smaller radii have higher first ionization energies. According to Coulomb’s Law, forces of attraction increase as atomic radii decrease. This means that if the radius is smaller the forces of attraction between the protons and the valence electrons will be larger. When the force of attraction is greater, it requires more energy to remove a valence electron. Since sulfur has the smallest radius, it will have the highest first ionization energy. <br /> See Atomic Theory II and III for more information."
    },
    {
        "question"      :   "Identify the number of protons, neutrons, and electrons that are contained in the <sup>64</sup>Zn isotope.",
        "image"         :   "",
        "choices"       :   [
                                "30 Protons, 34 Neutrons, 30 Electrons",
                                "34 Protons, 30 Neutrons, 34 Electrons",
                                "64 Protons, 94 Neutrons, 64 Electrons",
                                "64 Protons, 30 Neutrons, 64 Electrons"
                            ],
        "correct"       :   [0],
        "explanation"   :   "The mass number, which is 64 amu, is equal to the number of protons plus the number of neutrons. We know that zinc has 30 protons, as its atomic number is 30. Since 64 - 30 = 34, this isotope has 34 neutrons. In a neutral isotope the number of protons equals the number of electrons, so it has 30 electrons as well. <br /> See Atomic Theory I for more information."
    },
    {
        "question"      :   "The atomic radius of calcium is 194 pm and its first ionization energy is 590 kJ/mol. Identify the most likely values for the atomic radius and first ionization energy of magnesium.",
        "image"         :   "",
        "choices"       :   [
                                "Atomic Radius (219 pm), IE<sub>1</sub> (738 kJ/mol)",
                                "Atomic Radius (145 pm), IE<sub>1</sub> (738 kJ/mol)",
                                "Atomic Radius (219 pm), IE<sub>1</sub> (419 kJ/mol)",
                                "Atomic Radius (145 pm), IE<sub>1</sub> (419 kJ/mol)"
                            ],
        "correct"       :   [1],
        "explanation"   :   "<i>F</i> = <i>k</i><div class=\"frac\"> <span><i>Q</i><sub>1</sub><i>Q</i><sub>2</sub></span> <span class=\"symbol\">/</span> <span class=\"bottom\"><i>d</i><sup>2</sup></span></div><br />The valence electrons in Mg are in the 3s subshell and those of Ca are in 4s. Since Mg has fewer shells, its radius will be smaller. Thus, the radius must be 145 pm. Atomic radii always decrease when moving down a group in the periodic table. Elements with smaller radii have higher first ionization energies. According to Coulomb’s Law, forces of attraction increase as atomic radii decrease. Since Mg has a smaller radius, it will have a higher first ionization energy. Thus, the first ionization energy must be 738 kJ/mol. First ionization energies always decrease when moving down a group in the periodic table. <br /> See Atomic Theory III for more information."
    },
    {
        "question"      :   "<center><img src=\"data/images/1_mc1_8.png\" style=\"max-width:100%; max-height=100%\" /></center> <br /> Use the above photoelectron spectra for an unknown element to determine its identity.",
        "image"         :   "",
        "choices"       :   [
                                "B",
                                "C",
                                "Si",
                                "P"
                            ],
        "correct"       :   [3],
        "explanation"   :   "When looking at PES data, observe the numbers on the x-axis. If you are given the complete photoelectric spectrum for an element, the largest number will be associated with the 1<i>s</i> sublevel. On past AP Chemistry exams, the largest number has tended to be located the furthest to the right; however, the x-axis is often reversed so that the largest number appears closest to the origin. In this case, the largest number (104 MJ/mol) was the furthest to the right. The largest number is always associated with the 1<i>s</i> sublevel, as those electrons are closest to the nucleus, and thus, require the greatest amount of energy to be removed. The second largest number, 6.84 in this case, is associated with the 2<i>s</i> sublevel. The third largest number, 4.98 in this case, is associated with the 2p sublevel. The fourth largest number, 2.29 in this case, is associated with the 3<i>s</i> sublevel. The fifth largest number, 1.76 in this case, is associated with the 3<i>p</i> sublevel. The heights of the vertical peaks indicate the relative number of electrons in each sublevel. We know that this is phosphorus, as the 3<i>p</i> sublevel contains 3 electrons. <br /> See Atomic Theory II for more information."
    },
    {
        "question"      :   "<center><img src=\"data/images/1_mc1_9.png\" style=\"max-width:100%; max-height=100%\"/></center> <br /> Which of the above shell models of neutral atoms would have the lowest first ionization energy?",
        "image"         :   "",
        "choices"       :   [
                                "A",
                                "B",
                                "C",
                                "D"
                            ],
        "correct"       :   [0],
        "explanation"   :   "Shell model (A) would have the lowest first ionization energy. This shell model represnts sodium. Sodium has a larger atomic radius than (B) or (C). The valence electrons in (A), (B), and (C) are all in the n=3 shell; however, according to Coulomb’s Law, forces of attraction increases as the magnitude of a charge increases. Sodium has the least number of protons in its nucleus, so the magnitude of the positive charge pulling on the valence electrons in sodium is less than it is in (B) and (C). For this reason, sodium has a larger radius and less energy is required to remove its valence electron. Neon, shell model (D), also has a smaller radius than sodium, as all of its valence electrons are in the n=2 shell and it only has one less proton than sodium. According to Coulomb’s Law, forces of attraction also increase as distance decreases. <br /> See Atomic Theory III for more information."
    },

    {
        "question"      :   "<center><img src=\"data/images/1_mc1_10.png\" style=\"max-width:100%; max-height=100%\"/></center> <br /> The mass spectrum for a specific element is given above. Which of the following statements is a correct conclusion that can be drawn from this spectrum?",
        "image"         :   "",
        "choices"       :   [
                                "The most abundant isotope of this element has a mass of 24 amu.",
                                "This element in question has one isotope.",
                                "The element in question is from the halogen family.",
                                "The atomic mass of this element is 24 amu."
                            ],
        "correct"       :   [0],
        "explanation"   :   "Each bar in this graph represents the percent abundance of difference isotopes of the element. It shows you that there are three isotopes of this element. 78.99% of this element is the isotope with a mass of 24 amu, 10.00% of this element is the isotope with a mass of 25 amu, and 11.01% of this element is the isotope with a mass of 26 amu. The average atomic mass of this element is 24.31 amu. <br /> See Quantitative Chemistry II for more information."
    }
];
