/** Simple JavaScript Quiz
 * version 0.1.0
 * http://journalism.berkeley.edu
 * created by: Jeremy Rue @jrue
 *
 * Copyright (c) 2013 The Regents of the University of California
 * Released under the GPL Version 2 license
 * http://www.opensource.org/licenses/gpl-2.0.php
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 */

var quiztitle = "Unit 6 MC2";

/**
* Set the information about your questions here. The correct answer string needs to match
* the correct choice exactly, as it does string matching. (case sensitive)
*
*/
var quiz = [
    {
        "question"      :   "<center>Pb<sup>2+</sup><sub>(<i>aq</i>)</sub> + Mn<sub>(<i>s</i>)</sub> -> Pb<sub>(<i>s</i>)</sub> + Mn<sup>2+</sup>(<i>aq</i>) &nbsp;&nbsp;&nbsp; E<sup>o</sup>=1.05 V</center><br/><br/>Which of the following is true for the chemical reaction represented above?",
        "image"         :   "",
        "choices"       :   [
								"ΔG<sup>o</sup> > 0 and K<sub>c</sub> > 1",
								"ΔG<sup>o</sup> < 0 and K<sub>c</sub> > 1",
								"ΔG<sup>o</sup> > 0 and K<sub>c</sub> < 1",
								"ΔG<sup>o</sup> < 0 and K<sub>c</sub> < 1"
                            ],
        "correct"       :   [1],
        "explanation"   :   "<br/><br/>Because the value for E<sup>o</sup> is positive, we know that the reaction is thermodynamically favored, and thus, ΔG<sup>o</sup> < 0. The equilibrium position always lies to the right (mostly products at equilibrium) for all thermodynamically favored processes, so K<sub>c</sub> > 1.<br/><br/>See Electrochemistry II for more information."
    },
    {
        "question"      :   "<style>table{table-layout: fixed;}td{width: 50%; text-align: left; border: 0px solid black; word-wrap:break-word;font-size: 14px;}.first{width: 10%;}</style><table> <tr> <td class=\"first\">(I)</td><td>3Ni<sup>2+</sup><sub>(<i>aq</i>)</sub> + 2Cr<sub>(<i>s</i>)</sub> -> 3Ni<sub>(<i>s</i>)</sub> + 2Cr<sup>3+</sup><sub>(<i>aq</i>)</sub></td></tr><tr> <td class=\"first\"></td><td>E<sup>o</sup><sub>cell</sub>=0.49 V</td></tr><tr> <td class=\"first\">(II)</td><td>3Ni<sup>2+</sup><sub>(<i>aq</i>)</sub> + 2Al<sub>(<i>s</i>)</sub> -> 3Ni<sub>(<i>s</i>)</sub> + 2Al<sup>3+</sup></td></tr><tr> <td></td><td>E<sup>o</sup><sub>cell</sub>=1.41 V</td></tr><tr> <td class=\"first\">(III)</td><td>Cr<sup>3+</sup><sub>(<i>aq</i>)</sub> + Al<sub>(<i>s</i>)</sub> -> Cr<sub>(<i>s</i>)</sub> + Al<sup>3+</sup><sub>(<i>aq</i>)</sub></td></tr><tr> <td></td><td>E<sup>o</sup><sub>cell</sub>=?</td></tr></table><br/>The reactions for three galvanic cells are listed above. The standard cell potentials are given for galvanic cells (I) and (II). What is the standard cell potential for galvanic cell (III)?",
        "image"         :   "",
        "choices"       :   [
								"0.46 V",
								"0.92 V",
								"1.90 V",
								"2.36 V"
                            ],
        "correct"       :   [1],
        "explanation"   :   "<style>table{table-layout: fixed;}td{width: 50%; text-align: left; border: 0px solid black; word-wrap:break-word;}.first{width: 20%;font-size: 14px;}</style><table align=\"left\"> <tr> <td class=\"first\">(I)</td><td>3Ni<sub>(<i>s</i>)</sub> + 2Cr<sup>3+</sup> -> 3Ni<sup>2+</sup> + 2Cr<sub>(<i>s</i>)</sub></td><td>E<sup>o</sup><sub>cell</sub>=-0.49 V</td></tr><tr> <td class=\"first\">(II)</td><td>3Ni<sup>2+</sup> + 2Al<sub>(<i>s</i>)</sub> -> 3Ni<sub>(<i>s</i>)</sub> + 2Al<sup>3+</sup></td><td>E<sup>o</sup><sub>cell</sub>=1.41 V</td></tr></table><hr/><table align=\"left\"> <tr> <td class=\"first\">(III)<sup>I</sup></td><td>2Cr<sup>3+</sup> + 2Al<sub>(<i>s</i>)</sub> -> 2Cr<sub>(<i>s</i>)</sub> + 2Al<sup>3+</sup></td><td>E<sup>o</sup><sub>cell</sub>=0.92 V</td></tr></table><hr/><table align=\"left\"> <tr> <td class=\"first\">(III)<sup>II</sup></td><td>Cr<sup>3+</sup> + Al<sub>(<i>s</i>)</sub> -> Cr<sub>(<i>s</i>)</sub> + Al<sup>3+</sup></td><td>E<sup>o</sup><sub>cell</sub>=0.92 V</td></tr></table><br/><br/>This works the same way as Hess’ Law problems. In this case, you needed to flip equation (I) in order to get Ni<sub>(<i>s</i>)</sub> and Ni<sup>2+</sup><sub>(<i>aq</i>)</sub> to cancel when combining the first two chemical equations to get the desired third chemical equation. When you flip the equation, you must change the sign for E<sub>cell</sub>, so 0.49 V became negative. You then add the E<sub>cell</sub> values to get E<sub>cell</sub> for equation (III)<sup>I</sup>.<br/><br/>E<sub>cell</sub>=0.92 V for chemical equation (III)<sup>I</sup> and (III)<sup>II</sup>. You do not divide the value for E<sub>cell</sub> by two when you divide the coefficients by two like you did when solving Hess’ Law problems. This is because Voltage (V) is actually Joules/Coulomb (J/C). Potential Energy (<sub>PE</sub>) is doubled when the charge is doubled, because twice as much energy is released when a second electron drops to a lower <i>PE</i>. Because voltage is measured in J/C, we end up with 2<i>PE</i>/2charge after sending a second electron down the wire. The 2’s obviously cancel to give you PE/charge.<br/><br/>See Electrochemistry I for more information."
    },
    {
        "question"      :   "<center>Zn<sub>(<i>s</i>)</sub> + Pb<sup>2+</sup><sub>(<i>aq</i>)</sub> -> Zn<sup>2+</sup><sub>(<i>aq</i>)</sub> + Pb<sub>(<i>s</i>)</sub> &nbsp;&nbsp;&nbsp; E<sup>o</sup>=0.63 V</center><br/><br/>A galvanic cell operates under standard conditions in accordance with the above chemical equation. One of the half-cells contains a 1.0 M Pb(NO<sub>3</sub>)<sub>2</sub> solution. If additional Pb(NO<sub>3</sub>)<sub>2</sub> (s) was added to the Pb(NO<sub>3</sub>)<sub>2</sub> solution until its final concentration reached 4.0 M Pb(NO<sub>3</sub>)<sub>2</sub> , what would be the effect on the cell’s voltage?",
        "image"         :   "",
        "choices"       :   [
								"The voltage would increase.",
								"The voltage would decrease to a non-zero number.",
								"The voltage would drop to 0.00V.",
								"The voltage would not change."
                            ],
        "correct"       :   [0],
        "explanation"   :   "<br/><br/>Although equilibrium has not been established when the cell is under standard conditions, Le Chatelier’s principle can still be applied. If the concentration of an aqueous or gaseous reactant increases, the rate of the forward reaction also increases. If the reaction rate increases, the rate at which electrons flow (the current) and the voltage also increase.<br/><br/>When Q=K the cells voltage is 0.00 V. Voltage decreases as <i>Q</i> approaches <i>K</i> and it increases as <i>Q</i> moves further away from <i>K</i><sub>eq</sub>. <i>K</i><sub>eq</sub> is a large number, so the equilibrium position lies to the right. We know this because <i>E</i><sup>o</sup> > 0, so <i>K</i><sub>eq</sub> > 1.<br/><br/><center>Zn<sub>(<i>s</i>)</sub> + Pb<sup>2+</sup><sub>(<i>aq</i>)</sub> -> Zn<sup>2+</sup><sub>(<i>aq</i>)</sub> + Pb<sub>(<i>s</i>)</sub></center><br/><br/><center>Q=<div class=\"frac\"> <span>[Zn<sup>2+</sup>]</span> <span class=\"symbol\">/</span> <span class=\"bottom\">[Pb<sup>2+</sup>]</span></div></center><br/><br/>Increasing [Pb<sup>2+</sup>] causes <i>Q</i> to decrease, which means that <i>Q</i> will be further away from <i>K</i><sub>eq</sub> so the voltage will be greater.<br/><br/>See Electrochemistry II for more information."
    },
    {
        "question"      :   "<style>table{table-layout: fixed;}td{width: 50%; text-align: left; border: 0px solid black; word-wrap:break-word;}.first{width: 10%;}.second{width: 70%;}.third{width: 30%;}</style><table align=\"left\"> <tr > <td class=\"second\">I<sub>2(<i>s</i>)</sub> + 2 e<sup>-</sup> -> 2 I<sup>-</sup><sub>(<i>aq</i>)</sub></td><td class=\"third\">E<sup>o</sup>=0.53 V</td></tr><tr > <td class=\"second\">Cd<sup>2+</sup><sub>(<i>aq</i>)</sub> + 2 e<sup>-</sup> -> Cd<sub>(<i>s</i>)</sub></td><td class=\"third\">E<sup>o</sup>=-0.40 V</td></tr></table> <br/><br/> Use the information above to identify the species that is most easily reduced.",
        "image"         :   "",
        "choices"       :   [
								"I<sub>2(<i>s</i>)</sub>",
								"I<sup>-</sup><sub>(<i>aq</i>)</sub>",
								"Cd<sup>2+</sup><sub>(<i>aq</i>)</sub>",
								"Cd<sub>(<i>s</i>)</sub>"
                            ],
        "correct"       :   [0],
        "explanation"   :   "<br/><br/>The forward reactions are reductions, so the largest E<sup>o</sup> value will be associated with the species that is most easily reduced. In this case it is I<sub>2(<i>s</i>)</sub>. I<sup>-</sup><sub>(<i>aq</i>)</sub> and Cd<sub>(<i>s</i>)</sub> would not be reduced very easily, as they both have full octets and do not want to accept additional electrons.<br/><br/>See Electrochemistry I for more information."
    },
    {
        "question"      :   "How many moles of electrons pass through the cross-sectional area of a wire that is carrying 2.00 A of current for 96.5 seconds?",
        "image"         :   "",
        "choices"       :   [
								"193 mol e<sup>-</sup>",
								"48.3 mol e<sup>-</sup>",
								"2.00 mol e<sup>-</sup>",
								"2.00 x 10<sup>-3</sup> mol e<sup>-</sup>"
                            ],
        "correct"       :   [3],
        "explanation"   :   "<br/><br/>96.5 s x<div class=\"frac\"> <span>2.00 C</span> <span class=\"symbol\">/</span> <span class=\"bottom\">1 s</span></div>x<div class=\"frac\"> <span>1 mol e<sup>-</sup></span> <span class=\"symbol\">/</span> <span class=\"bottom\">96500 C</span></div>=2.00 x 10<sup>-3</sup> mol e<sup>-</sup><br/><br/>See Electrochemistry I for more information"
    },
    {
        "question"      :   "If 4.00 faradays or charge pass through a Cu(NO<sub>3</sub>)<sub>2</sub> solution, what mass of solid copper would be produced?",
        "image"         :   "",
        "choices"       :   [
								"254 g Cu(<i>s</i>)",
								"127 g Cu(<i>s</i>)",
								"63.6 g Cu(<i>s</i>)",
								"31.8 g Cu(<i>s</i>)"
                            ],
        "correct"       :   [1],
        "explanation"   :   "<br/><br/>(4.00)96500 C x<div class=\"frac\"> <span>1 mol e<sup>-</sup></span> <span class=\"symbol\">/</span> <span class=\"bottom\">96500 C</span></div>x<div class=\"frac\"> <span>1 mol Cu<sup>2+</sup></span> <span class=\"symbol\">/</span> <span class=\"bottom\">2 mol e<sup>-</sup></span></div>x<div class=\"frac\"> <span>63.55 g Cu</span> <span class=\"symbol\">/</span> <span class=\"bottom\">1 mol Cu</span></div>=127 g Cu<br/><br/>See Electrochemistry II for more information."
    },
    {
        "question"      :   "A direct current is applied to an aqueous solution of magnesium iodide. Which of the following reactions takes place at the anode?",
        "image"         :   "",
        "choices"       :   [
								"2 I<sup>-</sup> -> I<sub>2(<i>s</i>)</sub> + 2e<sup>-</sup> &nbsp;&nbsp;&nbsp; <i>E</i><sub>red</sub> = 0.53 V",
								"2 H<sub>2</sub>O<sub>(<i>l</i>)</sub> -> O<sub>2(<i>g</i>)</sub> + 4 H<sup>+</sup> + 4e<sup>-</sup> &nbsp;&nbsp;&nbsp; <i>E</i><sub>red</sub> = 1.23 V",
								"2 H<sub>2</sub>O<sub>(<i>l</i>)</sub>  + 2e<sup>-</sup> -> H<sub>2(<i>g</i>)</sub> + 2 OH<sup>-</sup> &nbsp;&nbsp;&nbsp; <i>E</i><sub>red</sub> = -0.83 V",
								"Mg<sup>2+</sup> + 2e<sup>-</sup> -> Mg<sub>(<i>s</i>)</sub> &nbsp;&nbsp;&nbsp; <i>E</i><sub>red</sub> = -2.37 V"
                            ],
        "correct"       :   [0],
        "explanation"   :   "<br/><br/>Oxidation takes place at the anode, and reactions (A) and (B) are oxidation half-reactions. The easiest way to recognize this is to see which side of the equation has the electrons. If they are on the products side, electrons are being lost so it is an oxidation half-reactions. If they are on the reactants side, electrons are being gained so it is a reduction half-reactions. Reactions (C) and (D) are reduction half- reactions. Aside from that, Mg<sup>2+</sup> would be very difficult to oxidized as it already lost two electrons and has a full octet. Furthermore, you are not given an option where Mg<sup>2+</sup> loses additional electrons. From this, it should be clear that the only possible choices are (A) and (B).<br/><br/>The oxidation half reaction with the most positive or least negative half-cell oxidation potential will be oxidized at the anode. The question gives you half-cell reduction potentials, not oxidation potentials, so you must change the signs to make them oxidation potentials.<br/><br/>2 I<sup>-</sup><sub>(<i>aq</i>)</sub> -> I<sub>2(<i>s</i>)</sub> + 2e<sup>-</sup> : <i>E</i><sub>oxidation</sub>=- 0.53 V<br/><br/>2 H<sub>2</sub>O<sub>(<i>l</i>)</sub> -> O<sub>2(<i>g</i>)</sub> + 4 H<sup>+</sup><sub>(<i>aq</i>)</sub> + 4e<sup>-</sup> : <i>E</i><sub>oxidation</sub>=- 1.23 V<br/><br/><i>E</i><sub>oxidation</sub>=- 0.53 V is that least negative half-cell oxidation potential, so (2 I<sup>-</sup><sub>(<i>aq</i>)</sub> -> I<sub>2(<i>s</i>)</sub> + 2e<sup>-</sup>) will take place at the anode.<br/><br/>See Electrochemistry II for more information."
    },
    {
        "question"      :   "A galvanic cell operates with a solid zinc anode immersed in a 1.0 <i>M</i> ZnSO<sub>4</sub> solution and a solid copper cathode immersed in a 1.0 <i>M</i> CuSO<sub>4</sub> solution. Which of the following statements is true?",
        "image"         :   "",
        "choices"       :   [
								"The mass of the anode increases and the mass of the cathode decreases as the cell operates.",
								"The mass of the cathode increases and the mass of the anode decreases as the cell operates.",
								"The mass of the cathode decreases and the mass of the anodede creasesas the cell operates.",
								"The mass of the cathode increases and the mass of the anode increases as the cell operates."
                            ],
        "correct"       :   [1],
        "explanation"   :   "<img src=\"data/images/6_mc2_8.png\" style=\"max-width:100%; max-height=100%\"/><br/><br/>See Electrochemistry I for more information."
    },
    {
        "question"      :   "O<sub>2(<i>g</i>)</sub> + 4 H<sup>+</sup> + 4e<sup>-</sup> -> 2 H<sub>2</sub>O<sub>(<i>l</i>)</sub><br/><br/>E<sub>red</sub>=1.23 V<hr/>2 H<sub>2</sub>O<sub>(<i>l</i>)</sub> + 2e<sup>-</sup> -> H<sub>2(<i>g</i>)</sub> + 2 OH<sup>-</sup><br/><br/>E<sub>red</sub>=-0.83 V<br/><br/>Water is electrolyzed in the presence of a non-reactive electrolyte under standard conditions. Determine which of the following is the balanced net ionic equation for the overall reaction that takes place using the information above.",
        "image"         :   "",
        "choices"       :   [
								"4 H<sub>2</sub>O<sub>(<i>l</i>)</sub> -> O<sub>2(<i>g</i>)</sub> + 2 H<sub>2(<i>g</i>)</sub> + 2 H<sup>+</sup>",
								"2 H<sub>2</sub>O<sub>(<i>l</i>)</sub> -> O<sub>2(<i>g</i>)</sub> + 2 H<sub>2(<i>g</i>)</sub>",
								"O<sub>2(<i>g</i>)</sub> + 4 H<sup>+</sup> -> H<sub>2(<i>g</i>)</sub> + 2 OH<sup>-</sup>",
								"6 H<sub>2</sub>O<sub>(<i>l</i>)</sub> -> O<sub>2(<i>g</i>)</sub> + 2 H<sub>2(<i>g</i>)</sub> + 4 H<sup>+</sup><sub>(<i>aq</i>)</sub> + 4 OH<sup>-</sup><sub>(<i>aq</i>)</sub>"
                            ],
        "correct"       :   [1],
        "explanation"   :   "<br/><br/>2 H<sub>2</sub>O<sub>(<i>l</i>)</sub> -> O<sub>2(<i>g</i>)</sub> + 4 H<sup>+</sup> + 4e<sup>-</sup> (flip to become oxidation of H<sub>2</sub>O)<br/><br/>4 H<sub>2</sub>O<sub>(<i>l</i>)</sub> + 4e<sup>-</sup> -> 2 H<sub>2(<i>g</i>)</sub> + 4 OH<sup>-</sup> (x 2 so the 4e<sup>-</sup> cancel)<hr/>6 H<sub>2</sub>O<sub>(<i>l</i>)</sub> -> O<sub>2(<i>g</i>)</sub> + 2 H<sub>2(<i>g</i>)</sub> + 4 H<sup>+</sup> + 4 OH<sup>-</sup><br/><br/>6 H<sub>2</sub>O<sub>(<i>l</i>)</sub> -> O<sub>2(<i>g</i>)</sub> + 2 H<sub>2(<i>g</i>)</sub> + 4 H<sub>2</sub>O<sub>(<i>l</i>)</sub> (4 H<sup>+</sup> + 4 OH<sup>-</sup> -> 4H<sub>2</sub>O)<hr/>2 H<sub>2</sub>O<sub>(<i>l</i>)</sub> -> O<sub>2(<i>g</i>)</sub> + 2 H<sub>2(<i>g</i>)</sub>"
    }
];
