/** Simple JavaScript Quiz
 * version 0.1.0
 * http://journalism.berkeley.edu
 * created by: Jeremy Rue @jrue
 *
 * Copyright (c) 2013 The Regents of the University of California
 * Released under the GPL Version 2 license
 * http://www.opensource.org/licenses/gpl-2.0.php
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 */

var quiztitle = "Unit 6 MC1";

/**
* Set the information about your questions here. The correct answer string needs to match
* the correct choice exactly, as it does string matching. (case sensitive)
*
*/
var quiz = [
    {
        "question"      :   "<center>2 Ag<sup>+</sup><sub>(<i>aq</i>)</sub> + Zn<sub>(<i>s</i>)</sub> -> 2 Ag<sub>(<i>s</i>)</sub> + Zn<sup>2+</sup><sub>(<i>aq</i>)</sub></center><br/><br/>A galvanic cell operates in accordance with the chemical equation above. The reduction half-reactions are below. What is the cell potential for this galvanic cell?<br/><table> <tr> <td><center>Ag<sup>+</sup><sub>(<i>aq</i>)</sub> + e<sup>-</sup> -> Ag<sub>(<i>s</i>)</sub></center></td><td><center><i>E</i><sup>o</sup>=0.80 V</center></td></tr><tr> <td><center>Zn<sup>2+</sup><sub>(<i>aq</i>)</sub> + 2 e<sup>-</sup> -> Zn<sub>(<i>s</i>)</sub></center></td><td><center><i>E</i><sup>o</sup>=-0.76 V</center></td></tr></table><br/>",
        "image"         :   "",
        "choices"       :   [
								"2.36 V",
								"1.56 V",
								"0.84 V",
								"0.04 V"
                            ],
        "correct"       :   [1],
        "explanation"   :   "<br/><br/><i>E</i><sup>o</sup><sub>cell</sub>=<i>E</i><sup>o</sup><sub>red</sub>(cathode) − <i>E</i><sup>o</sup><sub>red</sub>(anode)<br/><i>E</i><sup>o</sup><sub>cell</sub>=<i>E</i><sup>o</sup><sub>red</sub>(cathode) + <i>E</i><sup>o</sup><sub>oxidation</sub>(anode)<br/><i>E</i><sup>o</sup><sub>cell</sub>=0.80V + 0.76V<br/><i>E</i><sup>o</sup><sub>cell</sub>=1.56V<br/><br/>Since the reduction half-reaction for zinc was flipped to make it an oxidation half-reaction, the sign for <i>E</i><sup>o</sup> was changed and it became positive.<br/><br/>The reduction half-reaction for silver was multiplied by two in order to get the charges to balance. You do not, however, multiply <i>E</i><sup>o</sup> for silver half-reaction by two, as you did in the Hess’ Law problems in the thermochemistry unit. This is because Voltage (V) is actually Joules/Coulomb (J/C). Potential Energy (<i>PE</i>) is doubled when the charge is doubled, because twice as much energy is released when a second electron drops to a lower <i>PE</i>. Because voltage is measured in J/C, we end up with 2<i>PE</i>/2charge after sending a second electron down the wire. The 2’s obviously cancel to give you <i>PE</i>/charge.<br/><br/>See Electrochemistry I for more information."
    },
    {
        "question"      :   "<center>Be<sup>2+</sup><sub>(<i>aq</i>)</sub> + Cu<sub>(<i>s</i>)</sub> -> Be<sub>(<i>s</i>)</sub> + Cu<sup>2+</sup><sub>(<i>aq</i>)</sub> &nbsp;&nbsp;&nbsp; <i>E</i><sup>o</sup>=– 2.04 V</center><br/><br/>Which of the following is true for the chemical reaction represented above?<br/><br/>",
        "image"         :   "",
        "choices"       :   [
                "Δ<i>G</i><sup>o</sup> > 0 and <i>K</i><sub>c</sub> > 1",
                "Δ<i>G</i><sup>o</sup> < 0 and <i>K</i><sub>c</sub> > 1",
                "Δ<i>G</i><sup>o</sup> > 0 and <i>K</i><sub>c</sub> < 1",
                "Δ<i>G</i><sup>o</sup> < 0 and <i>K</i><sub>c</sub> < 1"
                            ],
        "correct"       :   [2],
        "explanation"   :   "<br/><br/>Because the value for <i>E</i><sup>o</sup> is negative, we know that the reaction is NOT thermodynamically favored, and thus, Δ<i>G</i><sup>o</sup> > 0. The equilibrium position always lies to the left (mostly reactants at equilibrium) for all non-thermodynamically favored processes, so <i>K</i><sub>c</sub> < 1.<br/><br/>See Electrochemistry II for more information."
    },
    {
        "question"      :   "<center>4 Fe<sub>(<i>s</i>)</sub> + 3 O<sub>2(<i>g</i>)</sub> -> 2 Fe<sub>2</sub>O<sub>3(<i>s</i>)</sub></center><br/><br/>Which of the following statements pertaining to iron in the above chemical equation is true?",
        "image"         :   "",
        "choices"       :   [
                "Iron is oxidized, because its oxidation number changes from 0 to +2.",
                "Iron is oxidized, because its oxidation number changes from 0 to +3.",
                "Iron is reduced, because its oxidation number changes from 0 to +2.",
                "Iron is reduced, because its oxidation number changes from 0 to +3."
                            ],
        "correct"       :   [1],
        "explanation"   :   "The oxidation number is zero for any element that is on its own or bonded to itself, so the oxidation number on iron in Fe(<i>s</i>) is 0. The oxidation number on oxygen in Fe<sub>2</sub>O<sub>3</sub> is -2, and there are three oxygen atoms (sums to -6), so the oxidation number on iron must be +3 in order for the compound to be neutral. Oxidation occurs when an element’s oxidation number increases, which means that it lost electrons.<br/><br/>See Aqueous Solutions and Chemical Reactions I for more information."
    },
    {
        "question"      :   "<center>Zn<sub>(<i>s</i>)</sub> + Pb<sup><sup>2+</sup></sup><sub>(<i>aq</i>)</sub> -> Zn<sup><sup>2+</sup></sup><sub>(<i>aq</i>)</sub> + Pb<sub>(<i>s</i>)</sub> &nbsp;&nbsp;&nbsp; <i>E</i><sup>o</sup>=0.63 V</center><br/><br/>A galvanic cell operates under standard conditions in accordance with the above chemical equation. One of the half cells contains a 1.0 <i>M</i>Pb(NO<sub>3</sub>)<sub>2</sub> solution and the other contains a 1.0 <i>M</i>Zn(NO<sub>3</sub>)<sub>2</sub> solution. If the concentrations of both solutions were increased to 2.0 M, what would be the effect on the cell’s voltage?<br/><br/>",
        "image"         :   "",
        "choices"       :   [
                "The voltage would increase.",
                "The voltage would decrease to a non-zero number.",
                "The voltage would drop to 0.00V.",
                "The voltage would not change."
                            ],
        "correct"       :   [3],
        "explanation"   :   "<br/><br/>Although equilibrium has not been established when the cell is under standard conditions, Le Chatelier’s principle can still be applied. If the concentration of an aqueous or gaseous reactant increases, the rate of the forward reaction also increases. If the reaction rate increases, the rate at which electrons flow (the current) and the voltage also increase. If the concentration of an aqueous product increases, the rate of the forward reaction decreases. In other words, increasing [Pb<sup>2+</sup>] would increase the voltage and increasing [Zn<sup>2+</sup>] would decrease the voltage. Since the concentrations at the anode and cathode were increased by the same amount, and there are no exponents in the equilibrium expression, the voltage will not change.<br/><br/>When Q=<i>K</i><sub>eq</sub> the cells voltage is 0.00 V. Voltage decreases as Q approaches <i>K</i><sub>eq</sub> and it increases as Q moves further away from <i>K</i><sub>eq</sub>. <i>K</i><sub>eq</sub> is a large number, so the equilibrium position lies to the right. We know this because E<sup>o</sup> > 0, so <i>K</i><sub>eq</sub> > 1.<br/><br/><center>Zn<sub>(<i>s</i>)</sub> + Pb<sup><sup>2+</sup></sup><sub>(<i>aq</i>)</sub> -> Zn<sup><sup>2+</sup></sup><sub>(<i>aq</i>)</sub> + Pb<sub>(<i>s</i>)</sub></center><br/><br/><center>Q=<div class=\"frac\"> <span>[Zn<sup>2+</sup>]</span> <span class=\"symbol\">/</span> <span class=\"bottom\">[Pb<sup>2+</sup>]</span></div>=<div class=\"frac\"> <span>1.0<i>M</i></span> <span class=\"symbol\">/</span> <span class=\"bottom\">1.0<i>M</i></span></div>=<div class=\"frac\"> <span>2.0<i>M</i></span> <span class=\"symbol\">/</span> <span class=\"bottom\">2.0<i>M</i></span></div></center>If a cell has equal concentrations at the anode and the cathode and there are no exponents in the equilibrium expression, the voltage will remain the same because the value for <i>Q</i> does not change.<br/><br/>See Electrochemistry II for more information."
    },
    {
        "question"      :   "<table> <tr> <td>I<sub>2(<i>s</i>)</sub> + 2 e<sup>-</sup> -> 2 I<sup>-</sup><sub>(<i>aq</i>)</sub> </td><td>E<sup>o</sup>=0.53 V</td></tr><tr> <td>Cd<sup>2+</sup><sub>(<i>aq</i>)</sub> + 2 e<sup>-</sup> -> Cd<sub>(<i>s</i>)</sub></td><td>E<sup>o</sup>=-0.40 V</td></tr></table><br/><br/>Use the information above to identify the species that is most easily oxidized.",
        "image"         :   "",
        "choices"       :   [
                "I<sub>2</sub><i>(s)</i>",
                "I<sup>-</sup><i>(aq)</i>",
                "Cd<sup>2+</sup><i>(aq)</i>",
                "Cd<i>(s)</i>"
                            ],
        "correct"       :   [3],
        "explanation"   :   "<br/><br/>The forward reactions are reductions. The following equations, which are flipped, represent the oxidation half-reactions. When a reduction half-reaction is flipped to make an oxidation half reaction, the sign for E<sup>o</sup> must be changed.<br/><br/><table> <tr> <td>2 I<sup>-</sup><sub>(<i>aq</i>)</sub> -> I<sub>2(<i>s</i>)</sub> + 2e<sup>-</sup></td><td><i>E</i><sub>oxidation</sub>=- 0.53 V</td></tr><tr> <td>Cd<sub>(<i>s</i>)</sub> -> Cd<sup>2+</sup><sub>(<i>aq</i>)</sub> + 2e<sup>-</sup></td><td><i>E</i><sub>oxidation</sub>=+ 0.40 V</td></tr></table><br/><br/>The species with the most positive, least negative <i>E</i><sub>oxidation</sub>, value is the most easily oxidized. In this case it is Cd<sub>(<i>s</i>)</sub>. Cd<sup>2+</sup><sub>(<i>aq</i>)</sub> and I<sub>2(<i>s</i>)</sub> would not be oxidized very easily, as they both have full octets and do not want to lose additional electrons.<br/><br/>See Electrochemistry I for more information.<br/><br/>"
    },
    {
        "question"      :   "How long would it take to plate out 5.2 g of Cr(s) from a CrCl3 solution using 9.65 A of current?",
        "image"         :   "",
        "choices"       :   [
                "3.0 x 10<sup>3</sup> s",
                "1.0 x 10<sup>3</sup> s",
                "2.9 s",
                "0.97 s"
                            ],
        "correct"       :   [0],
        "explanation"   :   "5.2 g Cr x<div class=\"frac\"> <span>1mol Cr</span> <span class=\"symbol\">/</span> <span class=\"bottom\">52.00 g Cr</span></div>x<div class=\"frac\"> <span>3 mol e<sup>-</sup></span> <span class=\"symbol\">/</span> <span class=\"bottom\">1mol Cr<sup>3+</sup></span></div>x<div class=\"frac\"> <span>96500 C</span> <span class=\"symbol\">/</span> <span class=\"bottom\">1mol e<sup>-</sup></span></div>x<div class=\"frac\"> <span>1s</span> <span class=\"symbol\">/</span> <span class=\"bottom\">9.65 C</span></div>=3.0×10<sup>3</sup>s<br/><br/>See Electrochemistry II for more information."
    },
    {
        "question"      :   "<table> <tr> <td>O<sub>2(<i>g</i>)</sub> + 4 H<sup>+</sup><sub>(<i>aq</i>)</sub> + 4 e<sup>-</sup> -> 2 H<sub>2</sub>O<sub>(<i>l</i>)</sub></td><td>E<sup>o</sup>=1.23 V</td></tr><tr> <td>Br<sub>2(<i>l</i>)</sub> + 2 e<sup>-</sup> -> 2 Br<sup>-</sup><sub>(<i>aq</i>)</sub></td><td>E<sup>o</sup>=1.07 V</td></tr><tr> <td>2 H<sub>2</sub>O<sub>(<i>l</i>)</sub> + 2 e<sup>-</sup> -> 2 H<sub>2(<i>g</i>)</sub> + 2 OH<sup>-</sup><sub>(<i>aq</i>)</sub></td><td>E<sup>o</sup>=-0.83V</td></tr><tr> <td>Sr<sup>2+</sup><sub>(<i>aq</i>)</sub> + 2 e<sup>-</sup> -> Sr<sub>(<i>s</i>)</sub></td><td>E<sup>o</sup>=-2.89 V</td></tr></table><br /><br />A direct current is applies to an aqueous SrBr<sub>2</sub> solution. Use the information above to determine which of the following statements is correct.",
        "image"         :   "",
        "choices"       :   [
                "H<sub>2</sub>(<i>g</i>) will be produced at the cathode.",
                "H<sub>2</sub>(<i>g</i>) will be produced at the anode.",
                "O<sub>2</sub>(<i>g</i>) will be produced at the cathode.",
                "O<sub>2</sub>(<i>g</i>) will be produced at the anode."
                            ],
        "correct"       :   [0],
        "explanation"   :   "E<sup>o</sup><sub>red</sub>(Sr<sup>2+</sup>)=-2.89 V and E<sup>o</sup><sub>red</sub> (H<sub>2</sub>O)=-0.83 V.The only possible reductions are Sr<sup>2+</sup> and H<sub>2</sub>O, as Br<sup>-</sup> cannot accept any more electrons. Water will be reduced at the cathode, as it has the most positive/least negative reduction potential. As you can see from the reduction half-reaction for H<sub>2</sub>O, H<sub>2</sub>(g) is a product.<br/><br/>E<sup>o</sup><sub>oxidation</sub>(Br<sup>-</sup>)=-1.07 V and E<sup>o</sup><sub>oxidation</sub>(H<sub>2</sub>O)=-1.23 V.<br/><br/>The only possible oxidations are Br<sup>-</sup> and H<sub>2</sub>O, as Sr<sup>2+</sup> cannot lose any more electrons. No gas will evolve from the anode, as H<sub>2</sub>O will not be oxidized at that electrode. Br<sup>-</sup> will be oxidized to Br<sub>2</sub>(l) at the anode, as it has the most positive/least negative oxidation potential. When reduction half-reactions are flipped to turn them into oxidation half-reactions, the sign for E<sup>o</sup> must be changed.<br/><br/>See Electrochemistry II for more information."
    },
    {
        "question"      :   "Which of the following statements about the salt bridge in a galvanic cell is true?",
        "image"         :   "",
        "choices"       :   [
                "Cations migrate from the cathode to the anode.",
                "Anions migrate from the anode to the cathode.",
                "Cations and anions migrate from the cathode to the anode.",
                "Anions migrate from the cathode to the anode and cations migrate from the anode to the cathode."
                            ],
        "correct"       :   [3],
        "explanation"   :   "<img src=\"data/images/6_mc1_8_1.png\" style=\"max-width:100%; max-height=100%\"/><br/><br/>In the above diagram, the sulfate ions are moving into and out of the salt bridge to maintain neutrality. As a Zn<sup>2+</sup> enters the oxidation half-cell, a SO<sub>4</sub><sup>2-</sup> also enters the oxidation half-cell to maintain neutrality.<br/><br/>At the same time, a Cu<sup>2+</sup>(<i>aq</i>) gained two electrons and transforms into a Cu(<i>s</i>). To keep the reduction half-cell neutral, a SO<sub>4</sub><sup>2-</sup> ion leaves the solution and enters the salt bridge. If the sulfate did not leave the solution, there would be excess negative charge at the cathode, and incoming electrons would be repelled. This would cause the reaction to stop.<br/><br/>The salt bridge itself also remains neutral because a SO<sub>4</sub><sup>2-</sup> enters the oxidation half-cell at the same time as the other SO<sub>4</sub><sup>2-</sup> enters the salt bridge from the reduction half-cell.<br/><br/><img src=\"data/images/6_mc1_8_2.png\" style=\"max-width:100%; max-height=100%\"/><br/><br/>In the diagram above, a zinc ion is moving into the salt bridge and two sodium ions are moving out of the salt bridge to maintain neutrality. As a Zn<sup>2+</sup> enters the oxidation half-cell a Zn<sup>2+</sup> ion enters the salt bridge to maintain neutrality.<br/><br/>At the same time, a Cu<sup>2+</sup>(<i>aq</i>) gained two electrons and transforms into Cu(<i>s</i>). This would have given the reduction half-cell an overall negative charge, except for the fact that two sodium ions left the salt bridge and entered the reduction half-cell.<br/><br/>The salt bridge itself also remains neutral. It would have gained an overall charge of +2 when the Zn<sup>2+</sup> ion entered from the anode, but two Na<sup>+</sup> ions left the salt bridge to enter the cathode at the same moment.<br/><br/>If everything does not remain neutral the electrons will stop flowing. If the Na<sup>+</sup> ions did not enter the solution, the cathode would have an overall negative charge. This would repel incoming electrons.<br/><br/>See Electrochemistry I for more information."
    },
    {
        "question"      :   "<table> <tr> <td>Cl<sub>2(<i>g</i>)</sub> + 2e<sup>-</sup> -> 2 Cl<sup>-</sup></td><td><i>E</i><sub>red</sub>=1.36 V</td></tr><tr> <td>O<sub>2(<i>g</i>)</sub> + 4 H<sup>+</sup> + 4e<sup>-</sup> -> 2 H<sub>2</sub>O<sub>(<i>l</i>)</sub></td><td><i>E</i><sub>red</sub>=1.23 V</td></tr><tr> <td>2 H<sub>2</sub>O<sub>(<i>l</i>)</sub> + 2e<sup>-</sup> -> H<sub>2(<i>g</i>)</sub> + 2 OH<sup>-</sup></td><td><i>E</i><sub>red</sub>=-0.83 V</td></tr><tr> <td>Mg<sup>2+</sup> + 2e<sup>-</sup> -> Mg<sub>(<i>s</i>)</sub></td><td><i>E</i><sub>red</sub>=-2.37 V</td></tr></table>",
        "image"         :   "",
        "choices"       :   [
                "Mg<sup>2+</sup> + 2 Cl<sup>-</sup> -> Mg<sub>(<i>s</i>)</sub> + Cl<sub>2(<i>g</i>)</sub>",
                "2 Cl<sup>-</sup> + 2 H<sub>2</sub>O<sub>(<i>l</i>)</sub> -> H<sub>2(<i>g</i>)</sub> + 2 OH<sup>-</sup> + Cl<sub>2(<i>g</i>)</sub>",
                "2 H<sub>2</sub>O<sub>(<i>l</i>)</sub> -> O<sub>2(<i>g</i>)</sub> + 2 H<sub>2(<i>g</i>)</sub>",
                "2 Mg<sup>2+</sup> + 2 H<sub>2</sub>O<sub>(<i>l</i>)</sub> -> 2 Mg<sub>(<i>s</i>)</sub> + O<sub>2(<i>g</i>)</sub> + 4H<sup>+</sup>"
                            ],
        "correct"       :   [2],
        "explanation"   :   "<br/><br/>The only species that can be oxidized are Cl<sup>-</sup> and H<sub>2</sub>O, which are the products of the first two half- reactions. When we flip those half reactions, we must chance the signs for the <i>E</i><sub>red</sub> values to make them <i>E</i><sub>oxidation</sub> values.<br/><br/><table> <tr> <td>2 Cl<sup>-</sup> -> Cl<sub>2(<i>g</i>)</sub> + 2e<sup>-</sup></td><td><i>E</i><sub>oxidation</sub>=-1.36 V</td></tr><tr> <td>2 H2O<sub>(<i>l</i>)</sub> -> O<sub>2(<i>g</i>)</sub> + 4 H<sup>+</sup> + 4e<sup>-</sup></td><td><i>E</i><sub>oxidation</sub>=-1.23 V</td></tr></table><br/><br/>H<sub>2</sub>O will be oxidized as it has the most positive, least negative, <i>E</i><sub>oxidation</sub> value.<br/><br/>The only species that can be reduced are Mg<sup>2+</sup> and H<sub>2</sub>O, which are the second two half reactions.<br/><br/><table> <tr> <td>2 H2O<sub>(<i>l</i>)</sub> + 2e<sup>-</sup> -> H<sub>2(<i>g</i>)</sub> + 2 OH<sup>-</sup></td><td><i>E</i><sub>red</sub>=-0.83 V</td></tr><tr> <td>Mg<sup>2+</sup> + 2e<sup>-</sup> -> Mg<sub>(<i>s</i>)</sub></td><td><i>E</i><sub>red</sub>=-2.37 V</td></tr></table><br/><br/>Since H<sub>2</sub>O has the most positive, least negative, <i>E</i><sub>red</sub> value, it will be reduced.<br/><br/>2 H<sub>2</sub>O<sub>(<i>l</i>)</sub> -> O<sub>2(<i>g</i>)</sub> + 4 H<sup>+</sup> + 4 e<sup>-</sup><br/><br/>4 H<sub>2</sub>O<sub>(<i>l</i>)</sub> + 4 e<sup>-</sup> -> 2 H<sub>2(<i>g</i>)</sub> + 4 OH<sup>-</sup> (x 2 so the 4e<sup>-</sup> cancel)<hr>6H<sub>2</sub>O<sub>(<i>l</i>)</sub> -> O<sub>2(<i>g</i>)</sub> + 2 H<sub>2(<i>g</i>)</sub> + 4 H<sup>+</sup> + 4 OH<sup>-</sup><br/><br/>6 H<sub>2</sub>O<sub>(<i>l</i>)</sub> -> O<sub>2(<i>g</i>)</sub> + 2 H<sub>2(<i>g</i>)</sub> + 4 H<sub>2</sub>O<sub>(<i>l</i>)</sub> (4H<sup>+</sup> + 4OH<sup>-</sup> -> 4H<sub>2</sub>O)<hr>2 H<sub>2</sub>O<sub>(<i>l</i>)</sub> -> O<sub>2(<i>g</i>)</sub> + 2 H<sub>2(<i>g</i>)</sub><br/><br/>See Electrochemistry II for more infromation."
    },
    {
        "question"      :   "If a current of 9.65 amperes flows though a solution of aqueous Ni<sup>2+</sup> for 1.0 x 10<sup>4</sup> seconds, what mass of solid nickel will be produced?",
        "image"         :   "",
        "choices"       :   [
                "117.18 g",
                "58.69 g",
                "29.35 g",
                "19.56 g"
                            ],
        "correct"       :   [2],
        "explanation"   :   "<br/><br/>1.0 × 10<sup>4</sup>s x<div class=\"frac\"> <span>9.65 C</span> <span class=\"symbol\">/</span> <span class=\"bottom\">1 s</span></div>x<div class=\"frac\"> <span>1 mol e<sup>-</sup></span> <span class=\"symbol\">/</span> <span class=\"bottom\">96500</span></div>x<div class=\"frac\"> <span>1 mol Ni<sup>2+</sup></span> <span class=\"symbol\">/</span> <span class=\"bottom\">2 mol e<sup>-</sup></span></div>x<div class=\"frac\"> <span>58.69 g Ni</span> <span class=\"symbol\">/</span> <span class=\"bottom\">1mol Ni</span></div>=29.35 g Ni<br/><br/>See Electrochemistry II for more information."
  }
];
