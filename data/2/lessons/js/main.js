/******* No need to edit below this line *********/
var currentquestion = 0, score = 0, submt=true, picked;

var selectedChoices = [];


function showLoading(isShown) {
  if(!isShown) {
    document.getElementById("loader").style.display = "none";
    document.getElementById("frame").style.display = "block";
  } else {
    document.getElementById("loader").style.display = "block";
    document.getElementById("frame").style.display = "none";
  }
}

function shuffle(array) {
  var currentIndex = array.length, temporaryValue, randomIndex;

  // While there remain elements to shuffle...
  while (0 !== currentIndex) {

    // Pick a remaining element...
    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex -= 1;

    // And swap it with the current element.
    temporaryValue = array[currentIndex];
    array[currentIndex] = array[randomIndex];
    array[randomIndex] = temporaryValue;
  }

  return array;
}


jQuery(document).ready(function($){

    /**
     * HTML Encoding function for alt tags and attributes to prevent messy
     * data appearing inside tag attributes.
     */
    function htmlEncode(value){
      return $(document.createElement('div')).text(value).html();
    }

    /**
     * This will add the individual choices for each question to the ul#choice-block
     *
     * @param {choices} array The choices from each question
     */
    function addChoices(choices){
        if(typeof choices !== "undefined" && $.type(choices) == "array"){
            $('#choice-block').empty();
            for(var i=0;i<choices.length; i++){
                $(document.createElement('li')).addClass('choice choice-box').attr('data-index', i).html(choices[i]).appendTo('#choice-block');
            }
        }
    }

    /**
     * Resets all of the fields to prepare for next question
     */
    function nextQuestion(){
        submt = true;
        selectedChoices = [];
        $('#explanation').empty();
        $('#question').html(quiz[currentquestion]['question']);
        $('#pager').text('Question ' + Number(currentquestion + 1) + ' of ' + quiz.length);
        if(quiz[currentquestion].hasOwnProperty('image') && quiz[currentquestion]['image'] != ""){
            if($('#question-image').length == 0){
                $(document.createElement('img')).addClass('question-image').attr('id', 'question-image').attr('src', quiz[currentquestion]['image']).attr('alt', quiz[currentquestion]['question']).insertAfter('#question');
            } else {
                $('#question-image').attr('src', quiz[currentquestion]['image']).attr('alt', quiz[currentquestion]['question']);
            }
        } else {
            $('#question-image').remove();
        }
        addChoices(quiz[currentquestion]['choices']);
        setupButtons();
    }

    /**
     * After a selection is submitted, checks if its the right answer
     *
     * @param {choice} number The li zero-based index of the choice picked
     */
    function processQuestion(choice){
        if(validateAnswers(choice, quiz[currentquestion]['correct'])){
            for(i = 0; i < choice.length; i++) {
              $('.choice').eq(choice[i]).css({'background-color':'#50D943'});
            }
            $('#explanation').html('<strong>Correct!</strong> ' + quiz[currentquestion]['explanation']);
            score++;
        } else {
            // $('.choice').eq(choice).css({'background-color':'#D92623'});
            $('#explanation').html('<strong>Incorrect.</strong> ' + quiz[currentquestion]['explanation']);
        }
        currentquestion++;
        $('#submitbutton').html('NEXT QUESTION &raquo;').on('click', function(){
            showLoading(true);
            if(currentquestion == quiz.length){
                endQuiz();
            } else {
                $(this).text('Check Answer').css({'color':'#222'}).off('click');
                nextQuestion();
            }
        });
    }

    /**
     * Sets up the event listeners for each button.
     */
    function setupButtons(){
        $('.choice').on('mouseover', function(){
            // $(this).css({'background-color':'#e1e1e1'});
        });
        $('.choice').on('mouseout', function(){
            // $(this).css({'background-color':'#fff'});
        })
        $('.choice').on('click', function(){
            picked = parseInt($(this).attr('data-index'));
            selectedIndex = selectedChoices.indexOf(picked);
            if(selectedIndex != -1) {
              $(this).removeAttr('style').off('mouseout mouseover');
              selectedChoices.splice(selectedIndex, 1);
            } else {
              $(this).css({'border-color':'#222','font-weight':700,'background-color':'#c1c1c1'});
              selectedChoices.push(picked);
            }
            if(submt){
                submt=false;
                $('#submitbutton').css({'color':'#000'}).on('click', function(){
                    $('.choice').off('click');
                    $(this).off('click');
                    processQuestion(selectedChoices);
                });
            }
        });


        showLoading(false);
    }

    /**
     * Quiz ends, display a message.
     */
    function endQuiz(){
        $('#explanation').empty();
        $('#question').empty();
        $('#choice-block').empty();
        $('#question-image').remove();
        $('#submitbutton').remove();
        $('#question').html("You got " + score + " out of " + quiz.length + " correct.");
        $(document.createElement('h2')).css({'text-align':'center', 'font-size':'4em'}).attr('id', 'result').text(Math.round(score/quiz.length * 100) + '%').insertAfter('#question');

        //add retry button
        $(document.createElement('div')).addClass('choice-box').attr('id', 'retrybutton').text('Try again').css({'font-weight':700,'color':'#222','padding':'30px 0'}).appendTo('#frame');

        $('#retrybutton').on('click', function(){
          init();
        });

        showLoading(false);
    }

    /**
     * Runs the first time and creates all of the elements for the quiz
     */
    function init(){
        //Shuffle questions
        quiz = shuffle(quiz);

        $('#frame').empty();
        currentquestion = 0;
        score = 0;
        submt = true;
        picked = -1;
        selectedChoices = [];

        //add title
        if(typeof quiztitle !== "undefined" && $.type(quiztitle) === "string"){
            $(document.createElement('h1')).text(quiztitle).appendTo('#frame');
        } else {
            $(document.createElement('h1')).text("Quiz").appendTo('#frame');
        }

        //add pager and questions
        if(typeof quiz !== "undefined" && $.type(quiz) === "array"){
            //add pager
            $(document.createElement('p')).addClass('pager').attr('id','pager').text('Question 1 of ' + quiz.length).appendTo('#frame');
            //add first question
            $(document.createElement('h2')).addClass('question').attr('id', 'question').html(quiz[0]['question']).appendTo('#frame');
            //add image if present
            if(quiz[0].hasOwnProperty('image') && quiz[0]['image'] != ""){
                $(document.createElement('img')).addClass('question-image').attr('id', 'question-image').attr('src', quiz[0]['image']).attr('alt', quiz[0]['question']).appendTo('#frame');
            }

            //questions holder
            $(document.createElement('ul')).attr('id', 'choice-block').appendTo('#frame');

            //add choices
            addChoices(quiz[0]['choices']);

            //add submit button
            $(document.createElement('div')).addClass('choice-box').attr('id', 'submitbutton').text('Check Answer').css({'font-weight':700,'color':'#222','padding':'30px 0'}).on('click', function(){
              $('html, body').animate({
                scrollTop: $('#explanation').offset().bottom
              }, 2000);
            }).appendTo('#frame');

            $(document.createElement('p')).addClass('explanation').attr('id','explanation').html('&nbsp;').appendTo('#frame');

            setupButtons();
        }
    }

    /**
    *  Validate answer
    **/
    function validateAnswers(needle, haystack){
      if(needle.length != haystack.length) return false;
      for(var i = 0; i < needle.length; i++){
        if(haystack.indexOf(needle[i]) == -1)
           return false;
      }
      return true;
    }

    init();
});
