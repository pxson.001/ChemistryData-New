/** Simple JavaScript Quiz
 * version 0.1.0
 * http://journalism.berkeley.edu
 * created by: Jeremy Rue @jrue
 *
 * Copyright (c) 2013 The Regents of the University of California
 * Released under the GPL Version 2 license
 * http://www.opensource.org/licenses/gpl-2.0.php
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 */

var quiztitle = "Unit 2 MC2";

/**
* Set the information about your questions here. The correct answer string needs to match
* the correct choice exactly, as it does string matching. (case sensitive)
*
*/
var quiz = [
    {
        "question"      :   "Which of the following statements provides the correct prediction and justification about the melting point of KBr when compared with CaS or KCl? ",
        "image"         :   "",
        "choices"       :   [
                                "KBr will have a higher melting point than CaS because the distances between ionic centers in CaS are shorter. ",
								"KBr will have a higher melting point than KCl because the distances between ionic centers in KCl are shorter. ",
								"KBr will have a lower melting point than CaS because the distances between ionic centers in CaS are shorter and the charges carried by the ions on CaS are larger. ",
								"KBr will have a higher melting point than KCl because the coulombic forces of attraction are stronger in KBr due to the arrangement of ions in three dimensional space. "
                            ],
        "correct"       :   [2],
        "explanation"   :   "<i>F</i>=<i>k</i><div class=\"frac\"> <span><i>Q</i><sub>1</sub><i>Q</i><sub>2</sub></span> <span class=\"symbol\">/</span> <span class=\"bottom\"><i>d</i><sup>2</sup></span></div><br/><br/>Melting points and boiling points increase as the forces of attraction between ions increase. Coulomb’s Law can be used to explain the forces that exist between charged particles. According to Coulomb’s Law, melting points increase as the charges of the ions increase (numerator) and as the distances between the ionic centers decrease (denominator). If you are given a choice, greater charges are normally more significant than a shorter distance. In this problem, KBr would have a lower melting point than the other two compounds. KBr has a lower melting point than CaS, as CaS has a shorter distance between ionic centers and larger charges on both ions. KBr has a lower melting point than KCl, as KCl has a shorter distance between ionic centers. If you were required to solve a similar problem about relative boiling points, you would also make predictions based on charges, distances, and Coulomb’s Law. <br/><br/>See Chemical Bonding I for more information. ",
    },
	{
		"question"		:	"A soft solid compound with a relatively low melting point does not conduct electricity at room temperature, when it is melted or when dissolved in water.  Identify the type of interactions that are most likely to occur between the particles in this substance. ",
		"image"			:	"",
		"choices"		:	[
								"Covalent bonds",
								"Ionic bonds ",
								"Metallic bonds",
								"Ion-dipole attractions"

							],
		"correct"		:	[0],
		"explanation"	:	"Covalent compounds, that are solid are room temperature, are often soft and have relatively low melting points.  The only covalent compounds that can ionize in water in order to conduct electricity are acids.  Most covalent compounds are not acids, so most do not conduct electricity when dissolved in water.  When an ionic compound in melted, the individual ions are free to move which allows the liquid to conduct electricity.  When an ionic compound is dissolved in water, the individual ions are free to move around, which also allows the aqueous solution to conduct electricity. ",
	},
	{
		"question"		:	"Which of the following compounds has the largest bond angle? ",
		"image"			:	"",
		"choices"		:	[
								"<img src=\"data/images/2_mc2_3A.png\"  style=\"max-width:100%; max-height:100%\"/>",
								"<img src=\"data/images/2_mc2_3B.png\"  style=\"max-width:100%; max-height:100%\"/>",
								"<img src=\"data/images/2_mc2_3C.png\"  style=\"max-width:100%; max-height:100%\"/>",
								"<img src=\"data/images/2_mc2_3D.png\"  style=\"max-width:100%; max-height:100%\"/>"

							],
		"correct"		:	[0],
		"explanation"	:	"O<sub>3</sub> is bent and has an ideal bond angle of 120<sup>o</sup>. CH<sub>4</sub> is tetrahedral and has a bond angle of 109.5<sup>o</sup>. NF<sub>4</sub> is trigonal pyramidal and has a bond angle of 107.3<sup>o</sup>. H<sub>2</sub>O is bent and has a bond angle of 104.5<sup>o</sup>. <br/><br/> See Chemical Bonding III for more information.",
	},
	{
		"question"		:	"Which of the following compounds has the highest boiling point? ",
		"image"			:	"",
		"choices"		:	[
								"Na<sup>+</sup> and Cl<sup>-</sup> ",
								"Rb<sup>+</sup> and I<sup>-</sup> ",
								"Be<sup>2+</sup> and O<sup>2-</sup> ",
								"Ba<sup>2+</sup> and S<sup>2-</sup> "

							],
		"correct"		:	[2],
		"explanation"	:	"<i>F</i>=<i>k</i><div class=\"frac\"> <span><i>Q</i><sub>1</sub><i>Q</i><sub>2</sub></span> <span class=\"symbol\">/</span> <span class=\"bottom\"><i>d</i><sup>2</sup></span></div><br/><br/>Melting points and boiling points increase as the forces of attraction between ions increase. Coulomb’s Law can be used to explain the forces that exist between charged particles. According to Coulomb’s Law, boiling points increase as the charges on the ions increase (numerator) and as the distances between ionic centers decrease (denominator). If you are given a choice, greater charges are normally more significant than shorter distances. In this problem, BeO would have a higher boiling point than the other compounds, as it has the largest charges (2+ and 2-) and the smallest ionic radii. If you were required to solve a similar problem about relative melting points, you would also make predictions based on charges, distances, and Coulomb’s Law.<br/><br/> See Chemical Bonding I for more information.",
	},
	{
		"question"		:	"Electromagnetic radiation in the infrared spectrum is associated with which of the following? ",
		"image"			:	"",
		"choices"		:	[
								"Electron transitions ",
								"Vibrations of bonds ",
								"Molecular rotation ",
								"All of the above "
							],
		"correct"		:	[1],
		"explanation"	:	"All covalent bonds in molecules vibrate and the frequency of those vibrations falls in the infrared region of the electromagnetic spectrum.  Infrared radiation that has the exact same vibrational frequency as a bond will be absorbed by the bond.<br/><br/>See Chemical Bonding V for more information on IR spectroscopy and infrared radiation.",

	},
	{
		"question"		:	"<center><img src=\"data/images/2_mc2_6.png\" style=\"max-width:100%;max-height:100%;\"/> </center> <br/><br/> Which of the following statements about the above function is true? ",
		"image"			:	"",
		"choices"		:	[
								"If the internuclear distance between the two atoms is at position X, they are too close together and their positive nuclei will repel each other causing them to move toward position Y, which is the average bond length. ",
								"If the internuclear distance between the two atoms is at position Y, a covalent bond is starting to form, and they will continue to move toward position X where the bond becomes more stable in the region beyond the hump",
								"If the internuclear distance between the two atoms is at position Y, a covalent bond is starting to form, and they will continue to move toward position Z where the bond becomes more stable in the region beyond the hump. ",
								"The average bond length lies in the region around position Z, as the potential energy in that region is close to zero. "
							],
		"correct"		:	[0],
		"explanation"	:	"This graph shows the formation of a covalent bond between two individual atoms that start out further to the right of position Z.  The potential energy results from proton-electron attractions, and electronelectron and proton-proton repulsions.  As the atoms move closer together, the potential energy decreases.  This is similar to rock falling off a cliff – its gravitational potential energy decreases as it approaches the ground.  The potential energy decreases as attractions between nuclei and valance electrons pull the nuclei closer together.  The potential energy that is lost is released as heat during the formation of the bond.  The energy that is lost during the formation of the bond is equal to the energy required to break the bond.  At position Y, which is the average bond length between the two atoms, the potential energy is at its lowest point.  Bonds can be thought of as oscillating springs, so Y is an average distance and the central position of the oscillation.  Moving further to the left, the bond (spring) is compressed, and the potential energy rises. <br/><br/>See Chemical Bonding II for more information.",
	},
	{
		"question"		:	"<center><img src=\"data/images/2_mc2_7_ques.png\" style=\"max-width:100%;max-height:100%\"/> </center> <br/><br/> Plots of potential energy as a function of intermolecular distance for Cl<sub>2</sub> and Br<sub>2</sub> are shown above. Select the  answer that identifies which curve is associated with which diatomic halogen and provides a correct justification.",
		"image"			:	"",
		"choices"		:	[
								"X<sub>2</sub> is Cl<sub>2</sub> and Y<sub>2</sub> is Br<sub>2</sub>.  Bromine has a larger atomic radius than chlorine, so Br<sub>2</sub> will have a longer average bond length and less bond energy. ",
								"X<sub>2</sub> is Br<sub>2</sub> and Y<sub>2</sub> is Cl<sub>2</sub>.  Chlorine has a larger atomic radius than bromine, so Cl<sub>2</sub> will have a longer average bond length and less bond energy. ",
								"X<sub>2</sub> is Br<sub>2</sub> and Y<sub>2</sub> is Cl<sub>2</sub>.  Although chlorine has a smaller atomic radius than bromine, the strong repulsive force between the lone pairs of adjacent atoms causes Cl<sub>2</sub> to have a longer bond length and less bond energy. ",
								"X<sub>2</sub> is Cl<sub>2</sub> and Y<sub>2</sub> is Br<sub>2</sub>.  Chlorine has a double bond and bromine has a single bond.  Double bonds are shorter and contain more bond energy. "
							],
		"correct"		:	[0],
		"explanation"	:	"These plots show the formation of covalent bonds between two chloride atoms and two bromide atoms. The potential energy results from proton-electron attractions, and electron-electron and proton-proton repulsions. As the atoms move closer together, the potential energy decreases. The potential energy decreases as attractions between nuclei and valance electrons pull the nuclei closer together. The energy that is lost is released as heat during the formation of the bond. The energy that is lost during the formation of the bond is equal to the energy required to break the bond. The average bond lengths for the molecules are the minimum points on the y-axis. Answer choice (C) is partially correct for F<sub>2</sub>. Fluorine has a shorter bond length than Cl<sub>2</sub> but it also has less bond energy than Cl<sub>2</sub> or Br<sub>2</sub>. This is due to the strong forces of repulsion between the lone pairs of adjacent fluorine atoms, which only occurs because the bond length is so short in F<sub>2</sub>. Due to these forces of repulsion, it requires less energy to break the F – F bond.<br/><br/>See Chemical Bonding II for more information.<br/><br/><center><img src=\"data/images/2_mc2_7_expl.png\" style=\"max-width:100%,max-height:100%\"/> </center>",
	},
	{
		"question"		:	"The central atom in a molecule displays <i>sp</i> hybridization.  What shape do the charge clouds make? ",
		"image"			:	"",
		"choices"		:	[
								"Linear",
								"Trigonal planar",
								"Tetrahedral",
								"Trigonal bipyramidal"
							],
		"correct"		:	[0],
		"explanation"	:	"<i>sp</i> hybridization occurs when there are two charge clouds around the central atom. <i>sp</i> hybridization always results in a linear geometry. <br/><br/>See Chemical Bonding III for more information. ",
	},
	{
		"question"		:	"A single bond between which two atoms would have the highest bond energy? ",
		"image"			:	"",
		"choices"		:	[
								 "H – F",
								 "H – Cl",
								 "H – Br",
								 "H – I"
							],
		"correct"		:	[0],
		"explanation"	:	"Since fluorine has the smallest radius, the H – F bond will be the shortest.  As bond length decreases, bond energy increases.  Potential energy decreases as the bonding atoms move closer together.  The potential energy that is lost is released as heat.  The energy released during the formation of a bond is equal to the energy required to break the same bond.  When the bond length is shorter, more energy is released, and more energy is required to break the bond.  Bond energy is the amount of energy that is required to break a bond. <br/><br/>See Chemical Bonding II for more information. ",
	},
	{
		"question"		:	"Which of the following statements about the polarity of the BF<sub>3</sub> and NF<sub>3</sub> molecules is true? ",
		"image"			:	"",
		"choices"		:	[
								"The polarity of the two molecules is similar because their molecular formulas share the same ratios of one element to another.",
								"BF<sub>3</sub> is more polar than NF<sub>3</sub> because the electronegativity difference between B and F is greater than that between N and F.",
								"BF<sub>3</sub> is non-polar, as it has a trigonal planar geometry and NF<sub>3</sub> is polar as it has trigonal pyramidal geometry.",
								"Both structures are non-polar, as they both exhibit  <i>sp<sup>2</sup></i> hybridization around the central atom. "
							],
		"correct"		:	[2],
		"explanation"	:	"The Lewis structures for the two compounds are shown above. BF<sub>3</sub> has three charge clouds (three bonds) around the central atom, which gives it a symmetrical trigonal planar geometry. It has <i>sp<sup>2</sup></i> hybrid orbitals and is non-polar. NF<sub>3</sub> has four charge clouds (three bonds and one lone pair) around the central atom, which gives it a trigonal pyramidal geometry. It has <i>sp<sup>3</sup><i> hybrid orbitals. NF<sub>3</sub> is polar, as it is not symmetrical and F is more electronegative than N.<br/><br/> See Chemical Bonding III and IV for more information. ",
	}
];
