/** Simple JavaScript Quiz
 * version 0.1.0
 * http://journalism.berkeley.edu
 * created by: Jeremy Rue @jrue
 *
 * Copyright (c) 2013 The Regents of the University of California
 * Released under the GPL Version 2 license
 * http://www.opensource.org/licenses/gpl-2.0.php
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 */

var quiztitle = "Unit 2 MC1";

/**
* Set the information about your questions here. The correct answer string needs to match
* the correct choice exactly, as it does string matching. (case sensitive)
*
*/
var quiz = [
    {
        "question"      :   "A chemist is given a pure sample of an unknown compound and asked to identify it.  Which of the following indicates the correct type of spectrometer that she would use to identify the compound with the correct justification?  ",
        "image"         :   "",
        "choices"       :   [
                                "An infrared (IR) spectrometer would be used because the compound would absorb the frequencies of infrared radiation that match the vibrational frequencies of bonds between specific atoms. ",
								"An infrared (IR) spectrometer would be used because the compound would reflect, refract, and diffract infrared light in order to produce a visual image of the compound’s geometric shape and structure. ",
								"An ultraviolet/visual (UV/Vis) spectrometer would be used because the compound would absorb the frequencies of ultraviolet and visual radiation that match the vibrational frequencies of bonds between specific atoms. ",
								"An ultraviolet/visual (UV/Vis) spectrometer would be used because the compound would reflect, refract, and diffract ultraviolet and visual light in order to produce an image of the compound’s geometric shape and structure. "
                            ],
        "correct"       :   [0],
        "explanation"   :   "One would use an IR spectrometer. In IR spectroscopy, different frequencies of electromagnetic radiation in the IR spectrum are absorbed by different types of bonds in the molecule that vibrate at the same frequencies as the IR radiation. The spectrum that is obtained from these experiments can be used to identify the different types of bonds in the structure and the elements that are involved in those bonds. The spectrum produced for a given compound also provides a ‘fingerprint’ that can be used to identify it. <br /> <br /> See Chemical Bonding V for more information.",
    },
    {
        "question"      :   "A solid and brittle compound with a very high melting point does not conduct electricity at room temperature but does conduct electricity very well when it is melted or dissolved in water.  Identify the type of interactions that are most likely to occur between the particles in this substance.",
        "image"         :   "",
        "choices"       :   [
                                "Covalent bonds ",
                                "Ionic bonds ",
                                "Metallic bonds ",
                                " Hydrogen bonds "
                            ],
        "correct"       :   [1],
        "explanation"   :   "Since ionic bonds are extremely strong, it takes a lot of energy to melt ionic crystal lattices, so you must add a lot of heat (energy) to get them to melt. In order to conduct electricity, there must be charged particles and those particles must be free to more. When an ionic compound in melted, the individual ions are free to move which allows it to conduct electricity. When an ionic compound is dissolved in water, the individual ions are free to move around, which also allows the aqueous solution to conduct electricity. Ionic solids are brittle. <br/><br/> See Chemical Bonding I for more information.",
    },
    {
        "question"      :   "Which of the following lists compounds in order of decreasing bond polarity? ",
        "image"         :   "",
        "choices"       :   [
                                "HF > CCl<sub>4</sub> > CF<sub>4</sub> ",
                                "CF<sub>4</sub> > CO > CH<sub>4</sub> ",
                                "NH<sub>3</sub> > NCl<sub>3</sub> > NF<sub>3</sub> ",
                                "NCl<sub>3</sub> > CCl<sub>4</sub> > NF<sub>3</sub> "
                            ],
        "correct"       :   [1],
        "explanation"   :   "Electronegativity is defined as an element’s ability to attract electrons towards itself in a chemical bond. Electronegativity increases when moving from left to right across a period and from bottom to top in a group. Bond polarity increases as the electronegativity difference between the bonding atoms increases. This is because the shared electrons will spend more time around the more electronegative element. This gives the more electronegative element a partially negative charge and the less electronegative element a partially positive charge. Bond polarity decreases (the bond becomes less polar) as the electronegativity difference between the atoms decreases. The electronegativity difference between F and C in CF<sub>4</sub> is 4.0 – 2.6=1.4; the electronegativity difference between C and O in CO is 3.4 – 2.6=0.8; and the electronegativity difference between C and H in CH<sub>4</sub> is 2.6 – 2.1=0.5. This is the only choice that shows decreasing bond polarity in the three consecutive compounds. <br/><br/>This question is not asking about the polarity of molecules. Many of the molecules in this question are non-polar even though the bonds within the molecules are polar, due to dipoles canceling. <br/><br/>See Chemical Bonding I and IV for more information of polar bonds and polar molecules. ",
    },
	{
		"question"		:	"The graph below shows a plot of absorbance vs. wavelength for a solution containing an unknown colored compound, X. <br/> <center><img src=\"data/images/2_mc1_4.png\" style=\"max-width:100%;max-height:100%\"/></center><br/>A student is giving the task of using a spectrophotometer to measure the absorbance of a solution containing the colored compound, X, in order to determine its concentration. Which of the following statements indicates the optimum wavelength to use in this experiment with the most appropriate explanation? ",
		"image"			:	"",
		"choices"		:	[
								"400 nm, as the compound will absorb light in two separate regions at this wavelength. ",
								"450 nm, as this wavelength of light is best for causing electron transitions between pi-bonding and pi-antibonding orbitals. ",
								"580 nm, as the molecular orbitals in this compound absorb the maximum amount of light at this wavelength. ",
								"700 nm, as the colorless components of this compound will allow for maximum transmittance at that wavelength. "
								
							],
		"correct"		:	[2],
		"explanation"	:	"When choosing the wavelength of light to use in a UV/Vis spectroscopy experiment, it is best to choose the wavelength that correlates with maximum absorbance.  This will increase precision in order to give the experimenter the most accurate results. <br/>See Chemical Bonding V for more information. ",
	},
	{
		"question"		:	"What can be said about the hybridization around the carbon atom in CH<sub>4</sub> and CO<sub>2</sub>?",
		"image"			:	"",
		"choices"		:	[
								"Yes, the hybrid orbitals were <i>sp<sup>3</sup></i> in CH<sub>4</sub> and changed to <i>sp</i> in CO<sub>2</sub>.",
								"Yes, the hybrid orbitals were <i>sp<sup>4</sup></i> in CH<sub>4</sub> and changed to <i>sp<sup>2</sup></i> in CO<sub>2</sub>.",
								"Yes, the hybrid orbitals were <i>sp<sup>2</sup></i> in CH<sub>4</sub> and changed to <i>sp<sup>3</sup></i> in CO<sub>2</sub>." ,
								"No, the hybridization does not change."

							],
		"correct"		:	[0],
		"explanation"	:	"The Lewis structures for the compounds in question are below.<br/> <br/> <img src=\"data/images/2_mc1_5.png\" style=\"max-width:100%;max-height:100%\"/> <p>To figure out the type of hybrid orbitals around the central atom, you must count the charge clouds around the central atom. One charge cloud is considered to be a single bond, a double bond, a triple bond, or a lone pair. CH<sub>4</sub> has 4 charge clouds. The hybrid orbitals for a central atom with 4 charge clouds must be constructed from 4 atomic orbitals (s + p + p + p=sp<sup>3</sup> hybrid orbitals). CO<sub>2</sub> has 2 charge clouds. The hybrid orbitals for a central atom with 2 charge clouds must be constructed from 2 atomic orbitals (s + p=sp hybrid orbitals). To figure out the type of hybrid orbitals that are around a central atom, count the charge clouds around the central atom, and use one s-orbital and up to three p-orbitals to make the correct hybrid orbitals. The total number of atomic orbitals used always equals the number of charge clouds.<br/> See Chemical Bonding IV for more information. </p>",
	},
	{
		"question"		:	"Electromagnetic radiation in the ultraviolet and visual spectrum is associated with which of the following?",
		"image"			:	"",
		"choices"		:	[
								" Electron transitions",
								"Vibration of bonds",
								"Molecular rotation",
								"All of the above "
							],
		"correct"		:	[0],
		"explanation"	:	"Ultraviolet and visual light causes electronic transitions between molecular orbitals.<br/><br/>See Chemical Bonding V for more information on UV/Vis spectroscopy.",
	},
	{
		"question"		:	"A white substance does not conduct electricity as a solid, but it does when it is dissolved in water. Its melting point is 884<sup>o</sup>C. Which of the following could be the identity of this compound? ",
		"image"			:	"",
		"choices"		:	[
								 "Na<sub>2</sub>SO<sub>4</sub>(<i>s</i>)",
								 "C<sub>6</sub>H<sub>12</sub>O<sub>6</sub>(<i>s</i>)",
								 "Al(<i>s</i>)",
								 "I<sub>2</sub>(<i>s</i>)"
							],
		"correct"		:	[0],
		"explanation"	:	"Since ionic bonds are extremely strong, it takes a lot of energy to melt ionic compounds, so you must add a lot of heat (energy) to get them to melt. In order to conduct electricity, there must be charged particles and those particles must be free to more. When an ionic compound in melted, the individual ions are free to move which allows it to conduct electricity. When an ionic compound is dissolved in water, the individual ions are free to move around, which also allows the aqueous solution to conduct electricity. Ionic solids are brittle. The only covalent compounds that can ionize in water in order to conduct electricity are acids. C<sub>6</sub>H<sub>12</sub>O<sub>6</sub>(s) and I<sub>2</sub>(<i>s</i>) are not acids. Al(<i>s</i>) conducts electricity as a solid. <br/><br/>See Chemical Bonding I for more information.",
	},
	{
		"question"		:	"Which selection below correctly identifies the three molecules as being polar or non-polar? ",
		"image"			:	"",
		"choices"		:	[
								"O<sub>3</sub> polar, CH<sub>3</sub>F polar, BF<sub>3</sub> non-polar",
								"O<sub>3</sub> non-polar, CH<sub>3</sub>F polar, BF<sub>3</sub> non-polar",
								"O<sub>3</sub> polar, CH<sub>3</sub>F non-polar, BF<sub>3</sub> non-polar",
								"O<sub>3</sub> non-polar, CH<sub>3</sub>F non-polar, BF<sub>3</sub> non-polar "
							],
		"correct"		:	[1],
		"explanation"	:	"<p></p><img src=\"data/images/2_mc1_8.png\" style=\"max-width:100%;max-height:100%\"/> <br/><br/>See Chemical Bonding I and IV for more information. ",
	},
	{
		"question"		:	"How many sigma and pi bonds are there in C<sub>2</sub>H<sub>2</sub>.",
		"image"			:	"",
		"choices"		:	[
								 "2 sigma-bonds and 2 pi-bonds",
								 "3 sigma-bonds and 2 pi-bonds",
								 "2 sigma-bonds and 3 pi-bonds",
								 "4 sigma-bonds and 1 pi-bond"
							],
		"correct"		:	[1],
		"explanation"	:	"The Lewis structure for C2H2 is below. <br/><br/><img src=\"data/images/2_mc1_9.png\"/> <br/><br/>Sigma-bonds result from the direct overlap of orbitals. All single bonds are sigma-bonds, so the bonds between the carbon atoms and the hydrogen atoms are sigma-bonds. Triple bonds consist of one sigma-bond and two pi-bonds. Therefore, this structure has three sigma-bonds and two pi-bonds. Double bonds contain one sigma-bond and one pi-bond. <br/><br/>See Chemical Bonding IV for more information. ",
	},
	{
		"question"		:	"Which compound has the highest melting point? ",
		"image"			:	"",
		"choices"		:	[
								 "NaBr",
								 "KBr",
								 "MgO",
								 "CaO"
							],
		"correct"		:	[2],
		"explanation"	:	"<br/><br/><i>F</i>=<i>k</i><div class=\"frac\"> <span><i>Q</i><sub>1</sub><i>Q</i><sub>2</sub></span> <span class=\"symbol\">/</span> <span class=\"bottom\"><i>d</i><sup>2</sup></span></div><br/><br/>Melting points and boiling points increase as the forces of attraction between ions increase. Coulomb’s Law can be used to explain the forces that exist between charged particles. According to Coulomb’s Law, melting points increase as the charges on the ions increase (numerator) and as the distances between the ions decrease (denominator). If you are given a choice, greater charges are more significant than a shorter distance. In this problem, MgO would have the highest melting point. Mg<sup>2+</sup> and O<sup>2-</sup> carry the highest charges and have the smallest radii. If you were required to solve a similar problem about relative boiling points, you would also make predictions based on charges, distances, and Coulomb’s Law. <br/><br/>See Chemical Bonding I for more information. ",
	}
];
